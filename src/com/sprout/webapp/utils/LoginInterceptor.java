package com.sprout.webapp.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.UserInfo;

public class LoginInterceptor implements HandlerInterceptor {

    public String[] allowUrls;// 还没发现可以直接配置不拦截的资源，所以在代码里面来排除

    public void setAllowUrls(String[] allowUrls) {
        this.allowUrls = allowUrls;
    }

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2,
            Exception arg3) throws Exception {
        System.out.println("this is afterCompletion");
        // TODO Auto-generated method stub

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2,
            ModelAndView arg3) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("this is postHandle");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object)
            throws Exception {
        System.out.println("path-->" + request.getContextPath());
        String requestUrl = request.getRequestURI().replace(request.getContextPath(), "");
        System.out.println("requestUrl-->" + requestUrl);
        if (null != allowUrls && allowUrls.length >= 1) for (String url : allowUrls) {
            if (requestUrl.contains(url)) {
                return true;
            }
        }
        UserInfo user = (UserInfo) request.getSession().getAttribute("currentUser");
        if (user != null) {
            System.out.println(user.getUsercode());
            return true; // 返回true，则这个方面调用后会接着调用postHandle(), afterCompletion()
        } else {
            // 未登录 跳转到登录页面
            response.sendRedirect(request.getContextPath() + "/pages/login-new.jsp");
            // response.sendRedirect(request.getContextPath() );
        }
        return false;
    }

}
