package com.sprout.webapp.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author zhou
 *
 */
public class JsonUtil {
	/**
	 * transfer java object to json text
	 * @param obj
	 * @return
	 */
	public static String toJsonText(Object object){
		return JSON.toJSONString(object);
	}
	
	public static Object toOjbect(String jsonText){
		return JSON.parse(jsonText);
	}
	
	public static JSONObject parseObject(String jsonText){
		return JSON.parseObject(jsonText);
	}
}
	