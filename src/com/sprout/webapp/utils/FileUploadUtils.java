package com.sprout.webapp.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 文件上传组件工具类
 * 
 * @author 
 * @version 1.0
 * @date 2009-8-21
 * @copyright Gever 2008-2010
 * @param <E>
 */
public class FileUploadUtils {

	private static FileUploadUtils utils = new FileUploadUtils();

	private FileUploadUtils() {
	}

	/** 默认单例 */
	public static FileUploadUtils getInstance() {
		return (utils);
	}

	/**
	 * 将文件源拷贝到目标文件
	 * @param src
	 * @param dist
	 */
	public void copy(File src, File dist) {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			int BUFFER_SIZE = 16 * 1024;
			bis = new BufferedInputStream(new FileInputStream(src), BUFFER_SIZE);
			bos = new BufferedOutputStream(new FileOutputStream(dist));
			byte[] buf = new byte[BUFFER_SIZE];
			int len = 0;
			while ((len = bis.read(buf)) != -1) {
				bos.write(buf, 0, len);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	/**
	 * 用于上传文件
	 * @param src 要上传的文件
	 * @param savePath 要上传的目标路径
	 */
	public void upload(File src, String savePath) {
		// 获取一个上传的输入流
		BufferedInputStream bis = null;
		// 获取一个文件的输出流
		BufferedOutputStream bos = null;
		FileInputStream fis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(src));
			bos = new BufferedOutputStream(new FileOutputStream(savePath));
			byte[] buf = new byte[(int)src.length()];
			int len = 0;
			while((len = bis.read(buf)) != -1) {
				bos.write(buf, 0, len);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
