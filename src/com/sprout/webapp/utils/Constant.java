package com.sprout.webapp.utils;

public class Constant {

    public static final int REGISTER = 0;
    public static final int UNREGISTER = -1;
    public static final int RUNNING = 1;
    public static final int KILL = 2;
    public static final String HDFS_PATH = "hdfs://125.216.243.183:8020";
    public static final String MAPREDUCE_RESULT =
            "D:\\Workspaces\\MyEclipse 9\\sprouts\\WebRoot\\result";

    public static final String AMQ_BROKER_URL = "tcp://125.216.243.185:61616";
    public static final String WEB_QUEUE_NAME = "Queue.Web.Tasks";
    public static final String ALARMER_QUEUE_NAME = "Queue.Alarm.Tasks";
    public static final String APPSER_QUEUE_NAME = "Queue.AppServer.Tasks";

}
