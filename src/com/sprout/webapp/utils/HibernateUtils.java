﻿package com.sprout.webapp.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projection;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.impl.CriteriaImpl.OrderEntry;
import org.springframework.util.Assert;

/**
 * 用于DAO查询的辅助工具类
 * @author 
 * @version 1.0
 * @date 2009-7-31
 * @copyright Gever 2008-2010
 * @param <E>
 */
public class HibernateUtils {

	@SuppressWarnings("unchecked")
	public static List getResultPage(Query query, int startIndex, int pageSize) {
		query.setFirstResult(startIndex);
		query.setMaxResults(pageSize);
		return query.list();
	}
	
	/**
	 * 用于拼接多个字段的参数值。精确查找
	 * @param model ：表名
	 * @param fields ：字段名和值
	 * @param andOror ：拼接类型
	 * @param tmps ：字段值
	 * @return HQL语句
	 */
	@SuppressWarnings({ "unchecked", "unchecked", "unchecked" })
	public static String buildFieldsHql(String model, Map<String, Object> fields,String andOror,List tmps) {
		Iterator iter = fields.entrySet().iterator(); 
		Map.Entry<String, Object> entry = null;
		StringBuffer sb = new StringBuffer();
		sb.append("from ").append(model).append(" as m where");
		while (iter.hasNext()) { // 遍历存储字段的Map,进行hql语句拼接
			entry = (Map.Entry<String, Object>) iter.next();
			sb.append(" m.").append(entry.getKey()).append(" = ?" + " " + andOror);
			tmps.add(entry.getValue());
		}
		//以 AND或 OR结束
		if(sb.charAt(sb.length()-1) == 'd') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 4));
		}
		if(sb.charAt(sb.length()-1) == 'r') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 3));
		}
		return sb.toString();
	}
	
	@SuppressWarnings({ "unchecked", "unchecked", "unchecked" })
	public static String buildFieldsHqlJoin(String model, Map<String, Object> fields,String andOror,List tmps) {
		Iterator iter = fields.entrySet().iterator(); 
		Map.Entry<String, Object> entry = null;
		StringBuffer sb = new StringBuffer();
		sb.append("from ").append(model).append(" as p inner join p.shop where");
		while (iter.hasNext()) { // 遍历存储字段的Map,进行hql语句拼接
			entry = (Map.Entry<String, Object>) iter.next();
			sb.append(" ").append(entry.getKey()).append(" = ?" + " " + andOror);
			tmps.add(entry.getValue());
		}
		//以 AND或 OR结束
		if(sb.charAt(sb.length()-1) == 'd') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 4));
		}
		if(sb.charAt(sb.length()-1) == 'r') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 3));
		}
		return sb.toString();
	}
	
	/**
	 * 用于拼接多个字段的参数值。模糊查找
	 * @param model ：表名
	 * @param fields ：字段名和值
	 * @param andOror ：拼接类型
	 * @param tmps ：字段值
	 * @return HQL语句
	 */
	@SuppressWarnings({ "unchecked", "unchecked", "unchecked" })
	public static String buildFieldsHqlLike(String model, Map<String, Object> fields,String andOror,List tmps) {
		Iterator iter = fields.entrySet().iterator(); 
		Map.Entry<String, Object> entry = null;
		StringBuffer sb = new StringBuffer();
		sb.append("from ").append(model).append(" as m where ");
		while (iter.hasNext()) { // 遍历存储字段的Map,进行hql语句拼接
			entry = (Map.Entry<String, Object>) iter.next();
			sb.append(" m.").append(entry.getKey()).append(" like ?" + " " + andOror);
			tmps.add(entry.getValue());
		}
		//以 AND或 OR结束
		if(sb.charAt(sb.length()-1) == 'd'||sb.charAt(sb.length()-1) == 'D') {
			sb = new StringBuffer(sb
					.substring(0, sb.length() - 4));
		}
		if(sb.charAt(sb.length()-1) == 'r'||sb.charAt(sb.length()-1) == 'R') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 3));
		}
		return sb.toString();
	}
	
	public static String buildFieldsHqlLike2(String model, Map<String, Object> fields,String andOror,List tmps) {
		Iterator iter = fields.entrySet().iterator(); 
		Map.Entry<String, Object> entry = null;
		StringBuffer sb = new StringBuffer();
		sb.append("from ").append(model).append(" as m where (");
		while (iter.hasNext()) { // 遍历存储字段的Map,进行hql语句拼接
			entry = (Map.Entry<String, Object>) iter.next();
			sb.append(" m.").append(entry.getKey()).append(" like ?" + " " + andOror);
			tmps.add(entry.getValue());
		}
		//以 AND或 OR结束
		if(sb.charAt(sb.length()-1) == 'd'||sb.charAt(sb.length()-1) == 'D') {
			sb = new StringBuffer(sb
					.substring(0, sb.length() - 4));
		}
		if(sb.charAt(sb.length()-1) == 'r'||sb.charAt(sb.length()-1) == 'R') {
			sb = new StringBuffer(sb.substring(0, sb.length() - 3));
		}
		return sb.toString();
	}
	
	
	public static Projection getProjection(Criteria criteria) {
		CriteriaImpl impl = (CriteriaImpl) criteria;
		return impl.getProjection();
	}

	@SuppressWarnings("unchecked")
	public static OrderEntry[] getOrders(Criteria criteria) {
		CriteriaImpl impl = (CriteriaImpl) criteria;
		Field field = getOrderEntriesField(criteria);
		try {
			return (OrderEntry[]) ((List) field.get(impl)).toArray(new OrderEntry[0]);
		} catch (Exception e) {
			throw new InternalError(" Runtime Exception impossibility can't throw ");
		}
	}

	public static Criteria removeOrders(Criteria criteria) {
		CriteriaImpl impl = (CriteriaImpl) criteria;
		try {
			Field field = getOrderEntriesField(criteria);
			field.set(impl, new ArrayList());
			return impl;
		} catch (Exception e) {
			throw new InternalError(" Runtime Exception impossibility can't throw ");
		}
	}

	@SuppressWarnings("unchecked")
	public static Criteria addOrders(Criteria criteria, OrderEntry[] orderEntries) {
		CriteriaImpl impl = (CriteriaImpl) criteria;
		try {
			Field field = getOrderEntriesField(criteria);
			for (int i = 0; i < orderEntries.length; i++) {
				List innerOrderEntries = (List) field.get(criteria);
				innerOrderEntries.add(orderEntries[i]);
			}
			return impl;
		} catch (Exception e) {
			throw new InternalError(" Runtime Exception impossibility can't throw ");
		}
	}

	private static Field getOrderEntriesField(Criteria criteria) {
		Assert.notNull(criteria, " criteria is requried. ");
		try {
			Field field = CriteriaImpl.class.getDeclaredField("orderEntries");
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			throw new InternalError();
		}
	}
}
