package com.sprout.webapp.utils;

import java.io.File;
import java.io.IOException;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;

/**
 * This class is for the remote openssh
 * @author Howson
 * 
 */
public class SSHTools{
	
	private Connection connection = null;
	
	private Session session = null;
	
	private String publicKey = "C:\\ssh\\openssh_private";
	
	/**
	 * This method is for connection without password
	 * @param user
	 * @param host
	 * @return boolean
	 */
	public boolean openConnection(String user,String host){
		connection = new Connection(host);
		File file = new File(publicKey);
		boolean result = false;
		try {
			connection.connect();
			result = connection.authenticateWithPublicKey( user, file, null);
			session = connection.openSession();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * This method is for connection that needs password
	 * @param user
	 * @param password
	 * @param host
	 * @return boolean
	 */
	public boolean openConnection(String user, String password, String host){
		connection = new Connection(host);
		boolean result = false;
		try {
			connection.connect();
			result = connection.authenticateWithPassword(user, password);
			session = connection.openSession();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * This method simply executes a shell command
	 * @param String command
	 */
	public void execShell(String command){
		try {
			if(session==null){
				session = connection.openSession();
			}
			session.execCommand(command);
		} catch (IOException e) {
			System.out.println("SSHTools execShell error!");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		System.out.println(new SSHTools().openConnection("zhou", "125.216.243.183"));
	}
	
}