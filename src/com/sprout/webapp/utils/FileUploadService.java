package com.sprout.webapp.utils;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class FileUploadService {
	private static FileUploadService fileUpload= new FileUploadService();
	
	public FileUploadService(){
	}
	/** 默认单例 */
	public static FileUploadService getInstance() {
		return fileUpload;
	}
	
	public void fileUpload(String localfilePath, String filePath) throws Exception{
		//  获取一个上传文件的输入流
		BufferedInputStream bis = null;
		//FileInputStream fis = null;
		BufferedOutputStream bos = null;
		System.out.println(localfilePath+":localfilePath");
		File file = new File(localfilePath);
		// 获取一个保存文件的输出流
		try {
			// 文件输入流
			bis=new BufferedInputStream(new FileInputStream(file));
			// 文件输出流
			bos=new BufferedOutputStream(new FileOutputStream(filePath));
			// 建立一个跟文件大小相同的数组，存放文件数据
			byte[] buffer=new byte[(int)file.length()];
			int len=0;
			while((len=bis.read(buffer))!=-1){
				bos.write(buffer,0,len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(bis!=null)
					bis.close();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
			
			try {
				if(bos!=null)
					bos.close();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}	
	}
	
	public void fileUpload(File file, String filePath) throws Exception{
		//  获取一个上传文件的输入流
			BufferedInputStream bis = null;
			FileInputStream fis = null;
			BufferedOutputStream bos = null;
			// 获取一个保存文件的输出流
			try {
				// 文件输入流
				bis=new BufferedInputStream(new FileInputStream(file));
				// 文件输出流
				bos=new BufferedOutputStream(new FileOutputStream(filePath));
				// 建立一个跟文件大小相同的数组，存放文件数据
				byte[] buffer=new byte[(int)file.length()];
				int len=0;
				while((len=bis.read(buffer))!=-1){
					bos.write(buffer,0,len);
				}
				bos.flush();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				try {
					if(bis!=null)
						bis.close();
				} catch (RuntimeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					if(bos!=null)
						bos.close();
				} catch (RuntimeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}

}
