package com.sprout.webapp.pojo;

//1. 平均界面响应时间
//2. 域名数
//3. 请求数
//4. 界面大小
//
//5. 正常数
//6. 错误数
//
//7. 界面请求/后台请求
//8. css /js /image / cookie 平均大小
//9. css /js /image / cookie  缓存/请求比例
//
//
//1.每个界面详细信息列表
public class SiteInfo {

	private Integer id;
	
	private Integer category_id;
	
	private Double average_time;//平均响应时间
	
	private Integer domain_num;//域名数
	
	private Integer request_num;//请求数
	
	private Double page_size;//界面大小
	
	private Double content_resp_time;
	
	private Integer cookie_num;
	
	private Integer css_num;
	
	private Integer js_num;
	
	private Integer picture_num;
	
	private Integer record_num;
	
	private Integer dym_num;
	
	private Integer static_num;
	
	private Integer css_size;
	
	private Integer js_size;
	
	private Integer picture_size;
	
	private String status_string;
	
	private String date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getAverage_time() {
		return average_time;
	}

	public void setAverage_time(Double average_time) {
		this.average_time = average_time;
	}

	public Integer getDomain_num() {
		return domain_num;
	}

	public void setDomain_num(Integer domain_num) {
		this.domain_num = domain_num;
	}

	public Integer getRequest_num() {
		return request_num;
	}

	public void setRequest_num(Integer request_num) {
		this.request_num = request_num;
	}

	public Double getPage_size() {
		return page_size;
	}

	public void setPage_size(Double page_size) {
		this.page_size = page_size;
	}

	public Double getContent_resp_time() {
		return content_resp_time;
	}

	public void setContent_resp_time(Double content_resp_time) {
		this.content_resp_time = content_resp_time;
	}

	public Integer getCookie_num() {
		return cookie_num;
	}

	public void setCookie_num(Integer cookie_num) {
		this.cookie_num = cookie_num;
	}

	public Integer getCss_num() {
		return css_num;
	}

	public void setCss_num(Integer css_num) {
		this.css_num = css_num;
	}

	public Integer getJs_num() {
		return js_num;
	}

	public void setJs_num(Integer js_num) {
		this.js_num = js_num;
	}

	public Integer getPicture_num() {
		return picture_num;
	}

	public void setPicture_num(Integer picture_num) {
		this.picture_num = picture_num;
	}

	public Integer getRecord_num() {
		return record_num;
	}

	public void setRecord_num(Integer record_num) {
		this.record_num = record_num;
	}

	public Integer getDym_num() {
		return dym_num;
	}

	public void setDym_num(Integer dym_num) {
		this.dym_num = dym_num;
	}

	public Integer getStatic_num() {
		return static_num;
	}

	public void setStatic_num(Integer static_num) {
		this.static_num = static_num;
	}

	public Integer getCss_size() {
		return css_size;
	}

	public void setCss_size(Integer css_size) {
		this.css_size = css_size;
	}

	public Integer getJs_size() {
		return js_size;
	}

	public void setJs_size(Integer js_size) {
		this.js_size = js_size;
	}

	public Integer getPicture_size() {
		return picture_size;
	}

	public void setPicture_size(Integer picture_size) {
		this.picture_size = picture_size;
	}

	public String getStatus_string() {
		return status_string;
	}

	public void setStatus_string(String status_string) {
		this.status_string = status_string;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
	}
	
	
}
