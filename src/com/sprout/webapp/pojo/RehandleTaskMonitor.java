package com.sprout.webapp.pojo;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.alibaba.fastjson.JSON;

public class RehandleTaskMonitor {
	
	//List <LogTaskState> rehandleList;
	
	//List <LogTask> disabledList;
	
	//创建写锁
	private final Lock writeLock;
	
	public RehandleTaskMonitor(){
		
		//rehandleList = new ArrayList<LogTaskState>();
		//disabledList = new ArrayList<LogTask>();
		ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
		this.writeLock = readWriteLock.writeLock();
	}
	
	/*public List<LogTaskState> getRehandleList()
	{
		return this.rehandleList;
	}*/
	
	/*public List<LogTask> getDisabledList()
	{
		return this.disabledList;
	}*/
	
	/*把未处理的LogTask存入disabledList中，每次只能一个线程访问
	public void addToList(List<LogTask>rehandleList)
	{
		writeLock.lock();
		this.disabledList.addAll(rehandleList);
		writeLock.unlock();
	}*/
	
	/*发送disabledList到DirectoryController*/
	public void postList(List <LogTask>disabledList)
	{
		String url = "http://125.216.243.183:8080";
		HttpClient httpClient = new DefaultHttpClient();
		String json = JSON.toJSONString(disabledList);
		
		//HttpEntity entity = new UrlEncodedFormEntity(list,"UTF-8"); 
		try {
			StringEntity entity = new StringEntity(json);
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			HttpPost request = new HttpPost(url.toString());
			request.setEntity(entity);
			
			HttpResponse response = httpClient.execute(request);
			
			/*System.out.println("Response Code: " +  
			        response.getStatusLine().getStatusCode()); 
			BufferedReader rd = new BufferedReader(  
		            new InputStreamReader(response.getEntity().getContent()));  
		    String line = "";  
		   while((line = rd.readLine()) != null)
		   {  
		       System.out.println(line);
		   }*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
}
