package com.sprout.webapp.pojo;

public class ErrorInfo {

	private String error;
	
	private String describle;

	private String created_date;
	
	private Integer monitor_id;
	
	private String host;
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getDescrible() {
		return describle;
	}

	public void setDescrible(String describle) {
		this.describle = describle;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public Integer getMonitor_id() {
		return monitor_id;
	}

	public void setMonitor_id(Integer monitor_id) {
		this.monitor_id = monitor_id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	
}
