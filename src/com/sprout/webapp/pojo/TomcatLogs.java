package com.sprout.webapp.pojo;

public class TomcatLogs {

	private Integer id;
	
	private String filename;
	
	private String filePath;
	
	private String collectedDay;
	
	private Integer tomcatId;
	
	private Integer resultId;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getCollectedDay() {
		return collectedDay;
	}

	public void setCollectedDay(String collectedDay) {
		this.collectedDay = collectedDay;
	}

	public Integer getTomcatId() {
		return tomcatId;
	}

	public void setTomcatId(Integer tomcatId) {
		this.tomcatId = tomcatId;
	}

	public Integer getResultId() {
		return resultId;
	}

	public void setResultId(Integer resultId) {
		this.resultId = resultId;
	}
	
	
}
