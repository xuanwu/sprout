package com.sprout.webapp.pojo;

public class LogTask {

	private Integer type;// 监控类型：0：访问日志accesslog，1：应用日志applog，2：系统日志syslog 3：hadoop日志hadooplog
	
	private String logpath;  //日记路径，需要进行验证
	
	private Integer frequent;  //监控频率  一天一次，或者一小时一次
	
	private String ip;  	// 监控主机的IP
	
	private String machine;  //监控的主机
	
	private int categoryId;//任务ID
	
	private int method;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getLogpath() {
		return logpath;
	}

	public void setLogpath(String logpath) {
		this.logpath = logpath;
	}

	public Integer getFrequent() {
		return frequent;
	}

	public void setFrequent(Integer frequent) {
		this.frequent = frequent;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getMethod() {
		return method;
	}

	public void setMethod(int method) {
		this.method = method;
	}
	
}
