package com.sprout.webapp.pojo;

public class LogRequest {

	private String request;
	
	private Integer status;
	
	private String size;
	
	private String resp_times;
	
	 private String method;//请求方法 get post

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getResp_times() {
		return resp_times;
	}

	public void setResp_times(String resp_times) {
		this.resp_times = resp_times;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
}
