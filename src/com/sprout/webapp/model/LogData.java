package com.sprout.webapp.model;

public class LogData {
	private Integer id;
	private String job_id;
	private int map_task_num;
	private int reduce_task_num;
	private int datalocal_map_num;
	private int cpu_time;
	private int map_time,reduce_time;
	private int phy_memory;
	private int vir_memory;
	private int input_file;
	
	public boolean finish;
	private int taskId;
	
	public boolean isFinish() {
		return finish;
	}

	public LogData(){
		this.finish = false;
	}
	
	public LogData(String job_id, int map_task_num, int reduce_task_num,
			int datalocal_map_num, int cpu_time, int map_time, int reduce_time,
			int phy_memory, int vir_memory, int input_file,
			int taskId) {
		super();
		this.job_id = job_id;
		this.map_task_num = map_task_num;
		this.reduce_task_num = reduce_task_num;
		this.datalocal_map_num = datalocal_map_num;
		this.cpu_time = cpu_time;
		this.map_time = map_time;
		this.reduce_time = reduce_time;
		this.phy_memory = phy_memory;
		this.vir_memory = vir_memory;
		this.input_file = input_file;
		this.taskId = taskId;
	}

	public LogData(Integer id, String job_id, int map_task_num,
			int reduce_task_num, int datalocal_map_num, int cpu_time,
			int map_time, int reduce_time, int phy_memory, int vir_memory,
			int input_file, boolean finish, int taskId) {
		super();
		this.id = id;
		this.job_id = job_id;
		this.map_task_num = map_task_num;
		this.reduce_task_num = reduce_task_num;
		this.datalocal_map_num = datalocal_map_num;
		this.cpu_time = cpu_time;
		this.map_time = map_time;
		this.reduce_time = reduce_time;
		this.phy_memory = phy_memory;
		this.vir_memory = vir_memory;
		this.input_file = input_file;
		this.taskId = taskId;
	}



	public void setJob_id(String jobid)
	{
		this.job_id = jobid;
	}
	
	public String getJob_id()
	{
		return job_id;
	}
	
	public void setMap_task_num(int mtn)
	{
		this.map_task_num = mtn;
	}
	
	public int getMap_task_num()
	{
		return map_task_num;
	}
	
	public void setReduce_task_num(int rtn)
	{
		this.reduce_task_num = rtn;
	}
	
	public int getReduce_task_num()
	{
		return reduce_task_num;
	}
	
	public void setDatalocal_map_num(int dmn)
	{
		this.datalocal_map_num = dmn;
	}
	
	public int getDatalocal_map_num()
	{
		return datalocal_map_num;
	}
	
	public void setCpu_time(int ct)
	{
		this.cpu_time = ct;
	}
	
	public int getCpu_time()
	{
		return cpu_time;
	}
	
	public void setMap_time(int mt)
	{
		this.map_time = mt;
	}
	
	public int getMap_time()
	{
		return map_time;
	}
	
	public void setReduce_time(int rt)
	{
		this.reduce_time = rt;
	}
	
	public int getReduce_time()
	{
		return reduce_time;
	}
	
	public void setPhy_memory(int pm)
	{
		this.phy_memory = pm;
	}
	
	public int getPhy_memory()
	{
		return phy_memory;
	}
	
	public void setVir_memory(int vm)
	{
		this.vir_memory = vm;
	}
	
	public int getVir_memory()
	{
		return vir_memory;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getInput_file() {
		return input_file;
	}

	public void setInput_file(int input_file) {
		this.input_file = input_file;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	
}
