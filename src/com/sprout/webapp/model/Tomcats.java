package com.sprout.webapp.model;

/**
 * 应用服务器监控的model类，不只是Tomcat的，还有Jetty或其他
 * 
 * @author howson
 * 
 */
public class Tomcats {

    private Integer id; // id

    private String destination; // 目标监控ip

    private String created_time; // 创建时间

    private Integer status; // 是否启动的flag

    private Integer port; // jmx监控端口，自动配置的，jetty是8998，tomcat是8999

    private Integer realport; // 连接端口，自动配置，jetty是8081，tomcat是8080（jetty需要改应用服务器端口配置）

    private String center_machine; // 任务分发的中心节点

    private String user; // 该实例所属用户

    private String type; // 监控类型，jetty，tomcat或其他

    private String monitorname; // 实例名称

    private Integer frequency; // 监控频率

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getCenter_machine() {
        return center_machine;
    }

    public void setCenter_machine(String center_machine) {
        this.center_machine = center_machine;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMonitorname() {
        return monitorname;
    }

    public void setMonitorname(String monitorname) {
        this.monitorname = monitorname;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getRealport() {
        return realport;
    }

    public void setRealport(Integer realport) {
        this.realport = realport;
    }

}
