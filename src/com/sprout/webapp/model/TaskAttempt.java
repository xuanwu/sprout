package com.sprout.webapp.model;

import java.util.List;

//应用实例
public class TaskAttempt {

	private String id; //任务Id
	
	private String jobId; //任务当前运行Id
	
	private String taskName;//任务名称
	
//	private List<String> services; // 使用服务名称
	
	private List<Integer> services; // 使用服务名称/id
	
	private String jarPath; // jar文件在本地的全路径
	
	private String memory; // 运行任务所需要的内存，单位MB
	
	private String cpu; // 运行任务的CPU百分比
	
	private String distUsed; //硬盘空间
	
	private String architecture; // 运行的架构，分为i386和x86_64两个类型
	
	private String network;//网络
	
	private Integer assumeTime;  //所需时间
	
	private Integer usedTime;  //真实时间
	
	private String createdDate;  //运行时间
	
	private String url; //实例的URL 
	
	private String logsPath; //日记文件
	
	private Integer appId;  //关联的applicationID
	
	private Integer userId; //关联的用户ID
	
	
	private String envVariables;// 环境变量
	
	private String username; //执行的用户名称
	
	private String taskname;//实例程序名称
	
	private String applicationType; //实例程序类型
	
	private String queue; //队列
	
	private String startTime; //开始时间
	
	private String endTime; //结束时间
	
	private Integer status; //实例状态     0: 未启动  1：pending 启动中  2：运行中running  3:finished 已完成
	
	private String finalStatus; //最终状态

	private float mapProgress; //当前map进度
	
	private float reduceProgress; // 当前reduce进度
	
	public TaskAttempt() {
		super();
	}

	public TaskAttempt(String id, String jobId, String taskName,
			List<Integer> services, String jarPath, String memory, String cpu,
			String distUsed, String architecture, String network,
			Integer assumeTime, Integer usedTime, String createdDate,
			String url, String logsPath, Integer appId, Integer userId,
			String envVariables, String username, String taskname2,
			String applicationType, String queue, String startTime,
			String endTime, Integer status, String finalStatus,
			float mapProgress, float reduceProgress) {
		super();
		this.id = id;
		this.jobId = jobId;
		this.taskName = taskName;
		this.services = services;
		this.jarPath = jarPath;
		this.memory = memory;
		this.cpu = cpu;
		this.distUsed = distUsed;
		this.architecture = architecture;
		this.network = network;
		this.assumeTime = assumeTime;
		this.usedTime = usedTime;
		this.createdDate = createdDate;
		this.url = url;
		this.logsPath = logsPath;
		this.appId = appId;
		this.userId = userId;
		this.envVariables = envVariables;
		this.username = username;
		taskname = taskname2;
		this.applicationType = applicationType;
		this.queue = queue;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
		this.finalStatus = finalStatus;
		this.mapProgress = mapProgress;
		this.reduceProgress = reduceProgress;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getJarPath() {
		return jarPath;
	}

	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getArchitecture() {
		return architecture;
	}

	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public Integer getAssumeTime() {
		return assumeTime;
	}

	public void setAssumeTime(Integer assumeTime) {
		this.assumeTime = assumeTime;
	}

	public Integer getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(Integer usedTime) {
		this.usedTime = usedTime;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<Integer> getServices() {
		return services;
	}

	public void setServices(List<Integer> services) {
		this.services = services;
	}

	public String getDistUsed() {
		return distUsed;
	}

	public void setDistUsed(String distUsed) {
		this.distUsed = distUsed;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLogsPath() {
		return logsPath;
	}

	public void setLogsPath(String logsPath) {
		this.logsPath = logsPath;
	}

	public String getEnvVariables() {
		return envVariables;
	}

	public void setEnvVariables(String envVariables) {
		this.envVariables = envVariables;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTaskname() {
		return taskname;
	}

	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}

	public float getMapProgress() {
		return mapProgress;
	}

	public void setMapProgress(float mapProgress) {
		this.mapProgress = mapProgress;
	}

	public float getReduceProgress() {
		return reduceProgress;
	}

	public void setReduceProgress(float reduceProgress) {
		this.reduceProgress = reduceProgress;
	}
	
	
	

}
