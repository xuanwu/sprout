package com.sprout.webapp.model;

public enum AppState {
	  NEW,
	  INIT,
	  RUNNING,
	  SUCCESS,
	  FAILED,
	  KILLED
}
