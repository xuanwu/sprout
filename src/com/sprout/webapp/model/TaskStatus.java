package com.sprout.webapp.model;


public class TaskStatus {

	  private String jobid;
	  private float mapProgress;
	  private float reduceProgress;

	  private int runState;
	  private int finishedState;
	  private long startTime;
	  
	  private String jobName;
	  private String jobFile;
	  private long finishTime;
	  
	public TaskStatus() {
		super();
	}

	public TaskStatus(String jobid, float mapProgress, float reduceProgress,
			int runState, int finishedState, long startTime, String jobName,
			String jobFile, long finishTime) {
		super();
		this.jobid = jobid;
		this.mapProgress = mapProgress;
		this.reduceProgress = reduceProgress;
		this.runState = runState;
		this.finishedState = finishedState;
		this.startTime = startTime;
		this.jobName = jobName;
		this.jobFile = jobFile;
		this.finishTime = finishTime;
	}

	public String getJobid() {
		return jobid;
	}

	public void setJobid(String jobid) {
		this.jobid = jobid;
	}

	public float getMapProgress() {
		return mapProgress;
	}

	public void setMapProgress(float mapProgress) {
		this.mapProgress = mapProgress;
	}

	public float getReduceProgress() {
		return reduceProgress;
	}

	public void setReduceProgress(float reduceProgress) {
		this.reduceProgress = reduceProgress;
	}

	public int getRunState() {
		return runState;
	}

	public void setRunState(int runState) {
		this.runState = runState;
	}

	public int getFinishedState() {
		return finishedState;
	}

	public void setFinishedState(int finishedState) {
		this.finishedState = finishedState;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobFile() {
		return jobFile;
	}

	public void setJobFile(String jobFile) {
		this.jobFile = jobFile;
	}

	public long getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(long finishTime) {
		this.finishTime = finishTime;
	}

	  
}
