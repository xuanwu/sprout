package com.sprout.webapp.model;

import java.util.List;
import java.util.Set;

public class Category {

	private Integer id; 
	
	private String name;  // 监控日记类型名称
	
	private Integer status; //当前监控状态    0:为启动   1：启动  2：停止
	
	private Integer type;// 监控类型：0：访问日志accesslog，1：应用日志applog，2：系统日志syslog 3：hadoop日志hadooplog
	
	private String logpath;  //日记路径，需要进行验证
	
	private Boolean validate;  //是否验证成功
	
	private Integer frequent;  //监控频率  一天一次，或者一小时一次
	
	private String samplelogs; //样例
	
	private String ip;  // 监控主机的IP
	
	private String machine;  //监控的主机
	
	private String regex;  //解析的正则表达式
	
	private Integer logType;//日志模式
	
	private Set<String>  machines;  //监控的主机集群
	
	private Integer appId;  //管理的APPID
	
	private Integer sampleKPI;//sampleKPI id
	
	private String configpath;  //配置文件
	
	private List<Integer> monitors;//监控指标

	private Long createDate;
	
	private Integer userId; //关联的用户ID
	
	public Category() {
		super();
	}



	public Category(Integer id, String name, Integer status, Integer type,
			String logpath, Boolean validate, Integer frequent,
			String samplelogs, String ip, String machine, String regex,
			Set<String> machines, Integer appId, Integer sampleKPI,
			String configpath, List<Integer> monitors, Long createDate,Integer userId) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.type = type;
		this.logpath = logpath;
		this.validate = validate;
		this.frequent = frequent;
		this.samplelogs = samplelogs;
		this.ip = ip;
		this.machine = machine;
		this.regex = regex;
		this.machines = machines;
		this.appId = appId;
		this.sampleKPI = sampleKPI;
		this.configpath = configpath;
		this.monitors = monitors;
		this.createDate = createDate;
		this.userId = userId;
	}



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public String getLogpath() {
		return logpath;
	}


	public void setLogpath(String logpath) {
		this.logpath = logpath;
	}


	public Boolean getValidate() {
		return validate;
	}


	public void setValidate(Boolean validate) {
		this.validate = validate;
	}


	public Integer getFrequent() {
		return frequent;
	}


	public void setFrequent(Integer frequent) {
		this.frequent = frequent;
	}


	public String getSamplelogs() {
		return samplelogs;
	}


	public void setSamplelogs(String samplelogs) {
		this.samplelogs = samplelogs;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public String getMachine() {
		return machine;
	}


	public void setMachine(String machine) {
		this.machine = machine;
	}


	public String getRegex() {
		return regex;
	}


	public void setRegex(String regex) {
		this.regex = regex;
	}


	public Set<String> getMachines() {
		return machines;
	}


	public void setMachines(Set<String> machines) {
		this.machines = machines;
	}


	public Integer getAppId() {
		return appId;
	}


	public void setAppId(Integer appId) {
		this.appId = appId;
	}


	public Integer getSampleKPI() {
		return sampleKPI;
	}


	public void setSampleKPI(Integer sampleKPI) {
		this.sampleKPI = sampleKPI;
	}


	public String getConfigpath() {
		return configpath;
	}


	public void setConfigpath(String configpath) {
		this.configpath = configpath;
	}


	public List<Integer> getMonitors() {
		return monitors;
	}


	public void setMonitors(List<Integer> monitors) {
		this.monitors = monitors;
	}



	public Long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}



	public Integer getUserId() {
		return userId;
	}



	public void setUserId(Integer userId) {
		this.userId = userId;
	}



	public Integer getLogType() {
		return logType;
	}



	public void setLogType(Integer logType) {
		this.logType = logType;
	}


	
}
