package com.sprout.webapp.model;

public class MonitorStatus {
    private Integer id;
    private String hostname; // 监控主机名 或 IP地址
    private String created_time; // 创建的时间
    private Integer status; // 当前状态
    private String destination; // 监控IP地址
    private String nodesystem; // 监控的操作系统
    private String type; // 监控类型 ：1 : ping 2: tcp 3：udp 4:http
    private String monitorMethod; // http的参数 监控方式 get post head
    private Integer frequency; // 监控频率秒数
    private int userId; // 监控用户，可为空
    private String monitorname; // 监控名称，即实例名称
    private Integer port; // udp 监控端口

    public MonitorStatus() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }


    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getNodesystem() {
        return nodesystem;
    }

    public void setNodesystem(String nodesystem) {
        this.nodesystem = nodesystem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMonitorMethod() {
        return monitorMethod;
    }

    public void setMonitorMethod(String monitorMethod) {
        this.monitorMethod = monitorMethod;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMonitorname() {
        return monitorname;
    }

    public void setMonitorname(String monitorname) {
        this.monitorname = monitorname;
    }


    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }


}
