package com.sprout.webapp.model;

public class NodeStatus {
	
	
	private Integer id;
	private Double cpu_idle;
	private Double cpu_used;
	private Double disk_free;
	private Double disk_total;
	private Double mem_free;
	private Double mem_total;
	private String hostname;
	private String created_time;
	
	private Long created_seconds;
	
	public NodeStatus() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getCpu_idle() {
		return cpu_idle;
	}

	public void setCpu_idle(Double cpu_idle) {
		this.cpu_idle = cpu_idle;
	}

	public Double getCpu_used() {
		return cpu_used;
	}

	public void setCpu_used(Double cpu_used) {
		this.cpu_used = cpu_used;
	}

	public Double getDisk_free() {
		return disk_free;
	}

	public void setDisk_free(Double disk_free) {
		this.disk_free = disk_free;
	}

	public Double getDisk_total() {
		return disk_total;
	}

	public void setDisk_total(Double disk_total) {
		this.disk_total = disk_total;
	}

	public Double getMem_free() {
		return mem_free;
	}

	public void setMem_free(Double mem_free) {
		this.mem_free = mem_free;
	}

	public Double getMem_total() {
		return mem_total;
	}

	public void setMem_total(Double mem_total) {
		this.mem_total = mem_total;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public Long getCreated_seconds() {
		return created_seconds;
	}

	public void setCreated_seconds(Long created_seconds) {
		this.created_seconds = created_seconds;
	}



}
