package com.sprout.webapp.model;

public class MonitorWarning {

    private Integer id;

    private String email;

    private int times_today;

    private String type; // 监控类型 ：1:ping 2: tcp 3：udp 4:http 5:tomcat

    private Integer userId; // 监控用户，可为空

    private Integer monitorId;// 与监控对象进行关联

    private String description;

    private Integer threshold; // 阈值（超出报警）

    private Integer frequency; // 监控周期

    private Integer retry_time; // 重试次数

    private Integer is_start;

    private String created_time; // 创建的时间

    public MonitorWarning() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTimes_today() {
        return times_today;
    }

    public void setTimes_today(int times_today) {
        this.times_today = times_today;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getRetry_time() {
        return retry_time;
    }

    public void setRetry_time(Integer retry_time) {
        this.retry_time = retry_time;
    }

    public Integer getIs_start() {
        return is_start;
    }

    public void setIs_start(Integer is_start) {
        this.is_start = is_start;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

}
