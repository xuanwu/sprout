package com.sprout.webapp.model;

/**
 * AbstractServiceinfo entity provides the base persistence definition of the
 * Serviceinfo entity. @author MyEclipse Persistence Tools
 */

public class ServiceInfo {

	// Fields

	private Integer id;
	private String servicename;
	private String servicetype;
	private String serviceenv;
	private Integer status;
	private String createdDate;
	private Integer appId;
	private Integer userId;

	// Constructors

	/** default constructor */
	public ServiceInfo() {
	}

	/** full constructor */
	public ServiceInfo(String servicename, String servicetype,
			String serviceenv, Integer status, String createdDate,
			Integer appId, Integer userId) {
		this.servicename = servicename;
		this.servicetype = servicetype;
		this.serviceenv = serviceenv;
		this.status = status;
		this.createdDate = createdDate;
		this.appId = appId;
		this.userId = userId;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServicename() {
		return this.servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getServicetype() {
		return this.servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getServiceenv() {
		return this.serviceenv;
	}

	public void setServiceenv(String serviceenv) {
		this.serviceenv = serviceenv;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getAppId() {
		return this.appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}