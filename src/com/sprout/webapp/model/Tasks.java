package com.sprout.webapp.model;

/**
 * Taskattempt entity. @author MyEclipse Persistence Tools
 */

public class Tasks implements java.io.Serializable {

	// Fields

	private Integer id;  //任务Id
	private String jobId;  //任务当前运行Id
	private String jarPath;  //jar 路径
	
	private String memory;
	private String cpu;
	private String distUsed;
	
	private int map_task_num;
	private int reduce_task_num;
	
	private String usedTime; //  starttime-finishtime
	private String assumeTime;
	private String createdDate;
	private String logsPath;  //log 路径
	private Integer userId;
	private Integer appId;
	private Integer appType;  // app类型
	
	private String configFile; //配置文件
	
	//job status 
	private Integer status;  // 实例状态     0: 未启动  1：pending 启动中  2：运行中running  3:finished 已完成
	private Integer finalStatus;// RUNNING(1), SUCCEEDED(2), FAILED(3), PREP(4)   KILLED(5);
	private String envVariables;
	private String username;
	private String taskname;
	private String applicationType;
	private String queue;
	private String startTime;
	private Float mapProgress;
	private Float reduceProgress;
	private String finishTime;
	  
	private String jobName;
	private String jobFile;
	private int numUsedSlots;
	private int usedMem;
	private int neededMem;
	
	private String resultPath;
	// Constructors
	
	/** default constructor */
	public Tasks() {
	}


	public Tasks(Integer id, String jobId, String jarPath, String memory,
			String cpu, String distUsed, int map_task_num, int reduce_task_num,
			String usedTime, String assumeTime, String createdDate,
			String logsPath, Integer userId, Integer appId, Integer appType,
			String configFile, Integer status, Integer finalStatus,
			String envVariables, String username, String taskname,
			String applicationType, String queue, String startTime,
			Float mapProgress, Float reduceProgress, String finishTime,
			String jobName, String jobFile, int numUsedSlots, int usedMem,
			int neededMem, String resultPath) {
		super();
		this.id = id;
		this.jobId = jobId;
		this.jarPath = jarPath;
		this.memory = memory;
		this.cpu = cpu;
		this.distUsed = distUsed;
		this.map_task_num = map_task_num;
		this.reduce_task_num = reduce_task_num;
		this.usedTime = usedTime;
		this.assumeTime = assumeTime;
		this.createdDate = createdDate;
		this.logsPath = logsPath;
		this.userId = userId;
		this.appId = appId;
		this.appType = appType;
		this.configFile = configFile;
		this.status = status;
		this.finalStatus = finalStatus;
		this.envVariables = envVariables;
		this.username = username;
		this.taskname = taskname;
		this.applicationType = applicationType;
		this.queue = queue;
		this.startTime = startTime;
		this.mapProgress = mapProgress;
		this.reduceProgress = reduceProgress;
		this.finishTime = finishTime;
		this.jobName = jobName;
		this.jobFile = jobFile;
		this.numUsedSlots = numUsedSlots;
		this.usedMem = usedMem;
		this.neededMem = neededMem;
		this.resultPath = resultPath;
	}


	/** full constructor */

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJobId() {
		return this.jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJarPath() {
		return this.jarPath;
	}

	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}

	public String getMemory() {
		return this.memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getCpu() {
		return this.cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getDistUsed() {
		return this.distUsed;
	}

	public void setDistUsed(String distUsed) {
		this.distUsed = distUsed;
	}

	public String getUsedTime() {
		return this.usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public String getAssumeTime() {
		return this.assumeTime;
	}

	public void setAssumeTime(String assumeTime) {
		this.assumeTime = assumeTime;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLogsPath() {
		return this.logsPath;
	}

	public void setLogsPath(String logsPath) {
		this.logsPath = logsPath;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getAppId() {
		return this.appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getFinalStatus() {
		return this.finalStatus;
	}

	public void setFinalStatus(Integer finalStatus) {
		this.finalStatus = finalStatus;
	}

	public String getEnvVariables() {
		return this.envVariables;
	}

	public void setEnvVariables(String envVariables) {
		this.envVariables = envVariables;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTaskname() {
		return this.taskname;
	}

	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}

	public String getApplicationType() {
		return this.applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getQueue() {
		return this.queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Float getMapProgress() {
		return this.mapProgress;
	}

	public void setMapProgress(Float mapProgress) {
		this.mapProgress = mapProgress;
	}

	public Float getReduceProgress() {
		return this.reduceProgress;
	}

	public void setReduceProgress(Float reduceProgress) {
		this.reduceProgress = reduceProgress;
	}

	public Integer getAppType() {
		return appType;
	}

	public void setAppType(Integer appType) {
		this.appType = appType;
	}

	public String getConfigFile() {
		return configFile;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}



	public String getFinishTime() {
		return finishTime;
	}



	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}



	public String getJobName() {
		return jobName;
	}



	public void setJobName(String jobName) {
		this.jobName = jobName;
	}



	public String getJobFile() {
		return jobFile;
	}



	public void setJobFile(String jobFile) {
		this.jobFile = jobFile;
	}

	public int getNumUsedSlots() {
		return numUsedSlots;
	}



	public void setNumUsedSlots(int numUsedSlots) {
		this.numUsedSlots = numUsedSlots;
	}

	public void setUsedMem(int usedMem) {
		this.usedMem = usedMem;
	}

	public int getNeededMem() {
		return neededMem;
	}


	public void setNeededMem(int neededMem) {
		this.neededMem = neededMem;
	}

	public int getMap_task_num() {
		return map_task_num;
	}

	public void setMap_task_num(int map_task_num) {
		this.map_task_num = map_task_num;
	}

	public int getReduce_task_num() {
		return reduce_task_num;
	}

	public void setReduce_task_num(int reduce_task_num) {
		this.reduce_task_num = reduce_task_num;
	}


	public String getResultPath() {
		return resultPath;
	}


	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}


	public int getUsedMem() {
		return usedMem;
	}

}