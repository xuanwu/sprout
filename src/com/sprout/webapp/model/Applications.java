// package com.sprout.webapp.model;
//
// import org.apache.hadoop.yarn.event.Dispatcher;
// import org.apache.hadoop.yarn.event.EventHandler;
//
// //应用
// public class Applications {
//
// private Integer id; //任务Id
//
// private String appId; //任务当前运行Id
//
// private String appName; //任务名称
//
// private String hostIp; //执行的IP地址
//
// private String dnsServer;
//
// private String jarPath; // jar文件在本地的全路径
//
// private String memory; // 运行任务所需要的内存，单位MB
//
// private String cpu; // 运行任务的CPU百分比
//
// private String distUsed; //硬盘空间
//
// private String architecture; // 运行的架构，分为i386和x86_64两个类型
//
// private String network;//网络
//
// private String usedTime; //运行时间
//
// private String createdDate; //运行时间
//
// private Integer status;
//
// private Integer userId; //关联的用户ID
//
// private Integer appType; //应用类型 0：mapreduce 1:spark 2:storm 3:mathout 4:java_web 5:weblog
//
// private String configFile;
//
//
// private Dispatcher dispatcher;
//
// private Integer tasknum;
//
// public Applications() {
// super();
// }
//
// public Applications(Integer id, String appId, String appName,
// String hostIp, String dnsServer, String jarPath, String memory,
// String cpu, String distUsed, String architecture, String network,
// String usedTime, String createdDate, Integer status,
// Integer userId, Integer appType, String configFile) {
// super();
// this.id = id;
// this.appId = appId;
// this.appName = appName;
// this.hostIp = hostIp;
// this.dnsServer = dnsServer;
// this.jarPath = jarPath;
// this.memory = memory;
// this.cpu = cpu;
// this.distUsed = distUsed;
// this.architecture = architecture;
// this.network = network;
// this.usedTime = usedTime;
// this.createdDate = createdDate;
// this.status = status;
// this.userId = userId;
// this.appType = appType;
// this.configFile = configFile;
// }
//
//
// public Integer getId() {
// return id;
// }
//
// public void setId(Integer id) {
// this.id = id;
// }
//
// public String getAppName() {
// return appName;
// }
//
// public void setAppName(String appName) {
// this.appName = appName;
// }
//
// public String getHostIp() {
// return hostIp;
// }
//
// public void setHostIp(String hostIp) {
// this.hostIp = hostIp;
// }
//
// public String getDnsServer() {
// return dnsServer;
// }
//
// public void setDnsServer(String dnsServer) {
// this.dnsServer = dnsServer;
// }
//
// public String getJarPath() {
// return jarPath;
// }
//
// public void setJarPath(String jarPath) {
// this.jarPath = jarPath;
// }
//
// public String getMemory() {
// return memory;
// }
//
// public void setMemory(String memory) {
// this.memory = memory;
// }
//
// public String getCpu() {
// return cpu;
// }
//
// public void setCpu(String cpu) {
// this.cpu = cpu;
// }
//
// public String getDistUsed() {
// return distUsed;
// }
//
// public void setDistUsed(String distUsed) {
// this.distUsed = distUsed;
// }
//
// public String getArchitecture() {
// return architecture;
// }
//
// public void setArchitecture(String architecture) {
// this.architecture = architecture;
// }
//
// public String getNetwork() {
// return network;
// }
//
// public void setNetwork(String network) {
// this.network = network;
// }
//
// public Integer getUserId() {
// return userId;
// }
//
// public void setUserId(Integer userId) {
// this.userId = userId;
// }
//
//
// public String getUsedTime() {
// return usedTime;
// }
//
//
// public void setUsedTime(String usedTime) {
// this.usedTime = usedTime;
// }
//
//
// public String getCreatedDate() {
// return createdDate;
// }
//
//
// public void setCreatedDate(String createdDate) {
// this.createdDate = createdDate;
// }
//
//
// public Integer getStatus() {
// return status;
// }
//
//
// public void setStatus(Integer status) {
// this.status = status;
// }
//
//
// public String getAppId() {
// return appId;
// }
//
//
// public void setAppId(String appId) {
// this.appId = appId;
// }
//
//
// public Integer getAppType() {
// return appType;
// }
//
//
// public void setAppType(Integer appType) {
// this.appType = appType;
// }
//
// public String getConfigFile() {
// return configFile;
// }
//
// public void setConfigFile(String configFile) {
// this.configFile = configFile;
// }
//
//
// public Integer getTasknum() {
// return tasknum;
// }
//
// public void setTasknum(Integer tasknum) {
// this.tasknum = tasknum;
// }
//
// // do something
// public Dispatcher getDispatcher() {
// return dispatcher;
// }
//
// public void setDispatcher(Dispatcher dispatcher) {
// this.dispatcher = dispatcher;
// }
//
// public static class AppEventDispatcher implements EventHandler<ApplicationEvent> {
//
// @Override
// public void handle(ApplicationEvent event) {
// if (event.getType() == AppState.KILLED) {
// System.out.println("receive job kill, kill all job");
//
// } else if (event.getType() == AppState.NEW) {
// System.out.println("receive app init ,NEW");
//
//
// }
// }
// }
// }
