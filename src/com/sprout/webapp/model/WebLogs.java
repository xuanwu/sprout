package com.sprout.webapp.model;

import java.util.ArrayList;

public class WebLogs {

	private Integer max;
	private ArrayList name;
	private ArrayList data;
	private ArrayList ip;
	
	public WebLogs(Integer max,ArrayList name,ArrayList data,ArrayList ip){
		this.max=max;
		this.name=name;
		this.data=data;
		this.ip = ip;
	}
	
	public Integer getMax(){
		return max;
	}
	
	public void setMax(Integer max){
		this.max=max;
	}
	
	public ArrayList getName(){
		return name;
	}
	
	public void setName(String names){
		name.add(names);
	}
	
	public ArrayList getData(){
		return data;
	}
	
	public void setData(Integer datas){
		data.add(datas);
	}
	
	public ArrayList getIp() {
		return ip;
	}

	public void setIp(Integer ips) {
		ip.add(ips);
	}

	public void setNameAndData(String names,Integer datas){
		if(name.contains(names)){
			int index = name.indexOf(names);
			int origin = (Integer)data.get(index);
			data.set(index, origin+datas);
		}else{
			name.add(names);
			data.add(datas);
		}			
	}
}
