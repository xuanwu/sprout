package com.sprout.webapp.model;

//监控对象
public class MonitorNode {
	private Integer id;
	private String hostname;
	private Double memtotal;
	private String created_time;
	private Integer status;
	private String nodeIP;
	private String nodesystem;
	
	private Double usedMem;
	private Double usedCpu;
	
	private String tomcatPath;
	
	
	
	public MonitorNode() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public Double getMemtotal() {
		return memtotal;
	}

	public void setMemtotal(Double memtotal) {
		this.memtotal = memtotal;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNodeIP() {
		return nodeIP;
	}

	public void setNodeIP(String nodeIP) {
		this.nodeIP = nodeIP;
	}

	public String getNodesystem() {
		return nodesystem;
	}

	public void setNodesystem(String nodesystem) {
		this.nodesystem = nodesystem;
	}


	public Double getUsedMem() {
		return usedMem;
	}

	public void setUsedMem(Double usedMem) {
		this.usedMem = usedMem;
	}

	public Double getUsedCpu() {
		return usedCpu;
	}

	public void setUsedCpu(Double usedCpu) {
		this.usedCpu = usedCpu;
	}

	public String getTomcatPath() {
		return tomcatPath;
	}

	public void setTomcatPath(String tomcatPath) {
		this.tomcatPath = tomcatPath;
	}

}
