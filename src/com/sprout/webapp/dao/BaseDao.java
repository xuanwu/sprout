package com.sprout.webapp.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;


public interface BaseDao<E> {
	public void insert(E entity);

	public Serializable save(E entity);

	public void delete(E entity);

	public void delete(Integer id);

	public void delete(Serializable... ids);

	public void batchDel(List<E> entities);

	public void batchDel(int[] ids);

	public void update(E entity);

	public E get(Integer id);

	List<E> findAll() ;
	
	public List<E> find(String field, Object value);

	// 通用prepareStatement方法
	public List queryByField(String hql, ArrayList param);

	// 提供外部的接口，使用hql语句进行查询
	public List query(String query);
	
	List<E> findBySQLList(String sql, Object... values) ;
	
	 List findByHQL(String sql, Object...values);
	 
	List doSqlQuery(String sql,Object...values);

	List<E> findByAttribute(final String field, final Object value);
	
	 int executeUpdate(String sql);
	
	/*// 查询所有实体
	public PaginationSupport<E> findAll(int currentPage, int pageSize);

	// 按实体的某个字段属性进行查询
	public PaginationSupport<E> find(String field, Object value,
			int currentPage, int pageSize);

	// 传入一个包含字段及字段参数值的Map，按多字段进行查询。精确查询
	public PaginationSupport<E> find(Map<String, Object> fields,
			String andOror, int currentPage, int pageSize);

	// 传入一个包含字段及字段参数值的Map，按多字段进行查询。模糊查询
	public PaginationSupport<E> findLike(Map<String, Object> fields,
			String andOror, int currentPage, int pageSize);

	// 自定制各字段的连接词，并且可以控制not/and/or的优先级，较为全面的搜索方法。
	public PaginationSupport<E> findMix(Map<String, Object> fields,
			final String andOror, final int currentPage, int pageSize);

	// 使用detachedCriteria取得分页记录集
	public PaginationSupport<E> findByCriteria(DetachedCriteria criteria,
			int currentPage, int pageSize);

	public PaginationSupport<E> findByHQLPagination(final String sql,
			final int currentPage, final int pageSize, final Object... values);*/

}
