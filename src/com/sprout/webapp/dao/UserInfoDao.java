package com.sprout.webapp.dao;

import java.util.List;

import com.sprout.webapp.model.UserInfo;

public interface UserInfoDao extends BaseDao<UserInfo>{
	
	public void insert(UserInfo userInfo);
	
	public void delete(UserInfo userInfo);
	
	public void update(UserInfo userInfo);
	
	public List<UserInfo>queryAll();
	
	public UserInfo queryUserInfo(String usercode);
	
	UserInfo isUserLogin(final UserInfo userInfo) ;
	
	UserInfo checkUserName(final String userName) ;
}
