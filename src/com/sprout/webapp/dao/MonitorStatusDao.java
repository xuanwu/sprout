package com.sprout.webapp.dao;

import com.sprout.webapp.model.MonitorStatus;

public interface MonitorStatusDao extends BaseDao<MonitorStatus>{
	
}
