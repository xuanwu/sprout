package com.sprout.webapp.dao;

import com.sprout.webapp.model.Category;


public interface CategoryDao extends BaseDao<Category>{
	
}
