package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.ServiceInfoDao;
import com.sprout.webapp.model.ServiceInfo;

/**
 * 
 * @author xiaocai
 * 
 */

@Repository
public class ServiceInfoDaoImp extends BaseDaoImp<ServiceInfo> implements ServiceInfoDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.ServiceInfo";
        super.setSessionFactory(sessionFactory);
    }
}
