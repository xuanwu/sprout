﻿package com.sprout.webapp.dao.imp;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.BaseDao;
public class BaseDaoImp<E> extends HibernateDaoSupport implements BaseDao<E> {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	
	protected String model;
	
	// 保存实体
	public Serializable save(E entity) {
		Serializable seri = this.getHibernateTemplate().save(entity);
		return seri;
	}
	

	// 删除实体
	public void delete(E entity) {
		this.getHibernateTemplate().delete(entity);
	}

	// 根据id删除
	public void delete(Integer id) {
		this.getHibernateTemplate().delete(get(id));
	}

	// 序列化删除
	public void delete(Serializable... ids) {
		for (Serializable id : ids) {
			this.getHibernateTemplate().delete(get((Integer) id));
		}
	}

	// 批量删除
	public void batchDel(List<E> entities) {
		for (E entity : entities) {
			delete(entity);
		}
	}

	// 根据id批量删除
	public void batchDel(int[] ids) {
		for (int id : ids) {
			delete(id);
		}
	}

	// 更新实体
	public void update(E entity) {
		this.getHibernateTemplate().update(entity);
		this.getHibernateTemplate().flush();
	}

	// 添加实体
	public void insert(E entity) {
		this.getHibernateTemplate().save(entity);
	}

	// 根据id取得实体
	@SuppressWarnings("unchecked")
	public E get(Integer id) {
		return (E) this.getHibernateTemplate().get(model, id);
	}

	// 查询所有实体
	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		return (List<E>) this.getHibernateTemplate().executeFind(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Criteria criteria = session.createCriteria(model);
						return criteria.list();
					}
				});
	}
	
	
	
	/*// 查询所有实体，并分页
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> findAll(final int currentPage,
			final int pageSize) {

		return (PaginationSupport) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {

						Criteria criteria = session.createCriteria(model)
								.setFirstResult((currentPage - 1) * pageSize)
								.setMaxResults(pageSize);
						List items = criteria.list();

						Criteria cri2 = session.createCriteria(model)
								.setProjection(Projections.rowCount());
						int totalCount = ((Integer) cri2.uniqueResult())
								.intValue();
						return new PaginationSupport(items, totalCount,
								currentPage, pageSize);
					}
				});
	}*/

	/**
	 * 按实体的某个字段属性进行查询
	 */
	@SuppressWarnings("unchecked")
	public List<E> find(String field, Object value) {

		StringBuffer sb = new StringBuffer();
		sb.append("from ").append(model).append(" where ").append(field)
				.append(" = :field");
		return this.getHibernateTemplate().findByNamedParam(sb.toString(),
				"field", value);
	}

	@SuppressWarnings("unchecked")
	public List<E> findBySQLList(String sql, Object... values) {
		return (List<E>) getHibernateTemplate().find(sql, values);
	}
	
	@Transactional
	public List findByHQL(String sql, Object...values){
		return getHibernateTemplate().find(sql, values);
	}
	
	@Transactional
	public int executeUpdate(String sql){
		return getHibernateTemplate().bulkUpdate(sql);
	}
	
	
	public List doSqlQuery(String sql,Object...values){
	    SQLQuery query = this.getSession().createSQLQuery(sql);  
		for (int i = 0; i < values.length; i++) {
			query.setParameter(i, values[i]);
		}
	    return query.list();  
	}
	
	@SuppressWarnings("unchecked")
	public List<E> findByAttribute(final String field, final Object value) {
		return (List<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						String hql = "from " + model + " where " + field
								+ " = :field";
						Query q = session.createQuery(hql);
						q.setParameter("field", value);
						return q.list();
					}
				});
	}

	
	/**
	 * 按实体的某个字段属性进行查询
	 * 
	 * @param field
	 *            //数据表的字段 culume
	 * @param value
	 * @param startIndex
	 * @param pageSize
	 * @return PaginationSupport(返回结果集,并存入分页Bean)
	 *//*
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> find(final String field, final Object value,
			final int currentPage, final int pageSize) {

		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						String hql = "from " + model + " where " + field
								+ " = :field";
						Query q = session.createQuery(hql);
						q.setParameter("field", value);
						int totalCount = q.list().size();
						List<E> items = HibernateUtils.getResultPage(q,
								(currentPage - 1) * pageSize, pageSize);
						return new PaginationSupport(items, totalCount,
								currentPage, pageSize);
					}
				});
	}

	*//**
	 * 传入一个包含字段及字段参数值的Map，按多字段进行查询。精确查询
	 * 
	 * @param fields
	 *            ：字段名和值
	 * @param andOror
	 *            : 查找类型
	 * @param startIndex
	 *            ：起始数据位置
	 * @param pageSize
	 *            　：每页大小
	 * @return {@link PaginationSupport}(返回结果集,并存入分页Bean)
	 */
/*	@SuppressWarnings("unchecked")
	public PaginationSupport<E> find(final Map<String, Object> fields,
			final String andOror, final int currentPage, final int pageSize) {
		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						List tmps = new ArrayList(); // 用于临时存储字段参数值
						String hql = HibernateUtils.buildFieldsHql(model,
								fields, andOror, tmps);
						Query q = session.createQuery(hql);
						for (int i = 0; i < tmps.size(); i++) {
							q.setParameter(i, tmps.get(i));
						}
						int totalCount = q.list().size();
						List items = HibernateUtils.getResultPage(q,
								(currentPage - 1) * pageSize, pageSize);
						return new PaginationSupport(items, totalCount,
								currentPage, pageSize);
					}
				});
	}
*/
	
/*
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> findByHQLPagination(final String sql,
			final int currentPage, final int pageSize, final Object... values) {
		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Query query = session.createQuery(sql);

						if (values != null) {
							for (int i = 0; i < values.length; i++) {
								query.setParameter(i, values[i]);
							}
						}
						int totalCount = query.list().size();
						List result = query.setFirstResult(
								(currentPage - 1) * pageSize).setMaxResults(
								pageSize).list();

						return new PaginationSupport(result, totalCount,
								currentPage, pageSize);
					}
				});
	}*/

	/**
	 * 传入一个包含字段及字段参数值的Map，按多字段进行查询。模糊查询
	 * 
	 * @param fields
	 *            ：字段名和值
	 * @param andOror
	 *            : 查找类型
	 * @param startIndex
	 *            ：起始数据位置
	 * @param pageSize
	 *            　：每页大小
	 * @return {@link PaginationSupport}(返回结果集,并存入分页Bean)
	 *//*
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> findLike(final Map<String, Object> fields,
			final String andOror, final int currentPage, final int pageSize) {
		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						List tmps = new ArrayList(); // 用于临时存储字段参数值
						String hql = HibernateUtils.buildFieldsHqlLike(model,
								fields, andOror, tmps);
						Query q = session.createQuery(hql);
						for (int i = 0; i < tmps.size(); i++) {
							q.setParameter(i, tmps.get(i));
						}
						int totalCount = q.list().size();
						List items = HibernateUtils.getResultPage(q,
								(currentPage - 1) * pageSize, pageSize);
						PaginationSupport ps = new PaginationSupport(items,
								totalCount, currentPage, pageSize);
						return ps;
					}
				});
	}

	*//**
	 * 自定制各字段的连接词，并且可以控制not/and/or的优先级，较为全面的搜索方法。（该方法建议二期开发）
	 * 
	 * @param fields
	 *            字段名和值
	 * @param andOror
	 *            查找类型
	 * @param startIndex
	 *            起始数据位置
	 * @param pageSize
	 *            每页大小
	 * @return {@link PaginationSupport}(返回结果集,并存入分页Bean)
	 *//*
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> findMix(final Map<String, Object> fields,
			final String andOror, final int currentPage, final int pageSize) {
		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						List tmps = new ArrayList(); // 用于临时存储字段参数值
						String hql = HibernateUtils.buildFieldsHqlLike(model,
								fields, andOror, tmps);
						Query q = session.createQuery(hql);
						for (int i = 0; i < tmps.size(); i++) {
							q.setParameter(i, tmps.get(i));
						}
						int totalCount = q.list().size();
						List items = HibernateUtils.getResultPage(q,
								(currentPage - 1) * pageSize, pageSize);
						PaginationSupport ps = new PaginationSupport(items,
								totalCount, currentPage, pageSize);
						return ps;
					}
				});
	}

	*//**
	 * 通用prepareStatement方法
	 * 
	 * @param hql
	 * @param param
	 * @return 返回数据结果集
	 */
	@SuppressWarnings("unchecked")
	public List queryByField(final String hql, final ArrayList param) {
		return (List) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Query q = session.createQuery(hql);
						for (int i = 0; i < param.size(); i++) {
							q.setParameter(i, param.get(i));
						}
						return q.list();
					}
				});
	}

	/**
	 * 使用detachedCriteria取得分页记录集
	 * 
	 * @param criteria
	 * @param currentPage
	 * @param pageSize
	 * @return {@link PaginationSupport}(返回结果集,并存入分页Bean)
	 *//*
	@SuppressWarnings("unchecked")
	public PaginationSupport<E> findByCriteria(final DetachedCriteria criteria,
			final int currentPage, final int pageSize) {

		return (PaginationSupport<E>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						Criteria executableCriteria = criteria
								.getExecutableCriteria(session);
						// 先将可排序列存储
						OrderEntry[] orderEntries = HibernateUtils
								.getOrders(executableCriteria);
						// 移除所有的排序
						executableCriteria = HibernateUtils
								.removeOrders(executableCriteria);
						// 先将投影列存储
						Projection projection = HibernateUtils
								.getProjection(executableCriteria);

						int totalCount = ((Integer) executableCriteria
								.setProjection(Projections.rowCount())
								.uniqueResult()).intValue();
						executableCriteria.setProjection(projection);
						if (projection == null) {
							executableCriteria
									.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
						}
						executableCriteria = HibernateUtils.addOrders(
								executableCriteria, orderEntries);
						List items = executableCriteria.list();

						return new PaginationSupport(items, totalCount,
								currentPage, pageSize);
					}
				});

	}
*/
	/**
	 * 提供外部的接口，使用hql语句进行查询
	 * 
	 * @param query
	 * @return 查询的结果集
	 */
	@SuppressWarnings("unchecked")
	public List query(String query) {
		return this.getHibernateTemplate().find(query);
	}


	

}
