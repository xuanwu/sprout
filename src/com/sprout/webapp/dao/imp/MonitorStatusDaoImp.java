package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.MonitorWarningDao;
import com.sprout.webapp.model.MonitorWarning;


@Repository
public class MonitorStatusDaoImp extends BaseDaoImp<MonitorWarning> implements MonitorWarningDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.MonitorWarning";
        super.setSessionFactory(sessionFactory);
    }


}
