package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.NodeStatusDao;
import com.sprout.webapp.model.NodeStatus;

@Repository
public class NodeStatusDaoImp extends BaseDaoImp<NodeStatus> implements NodeStatusDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.NodeStatus";
        super.setSessionFactory(sessionFactory);
    }


}
