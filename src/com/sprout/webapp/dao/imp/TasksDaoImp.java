package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.TasksDao;
import com.sprout.webapp.model.Tasks;

@Repository
public class TasksDaoImp extends BaseDaoImp<Tasks> implements TasksDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.Tasks";
        super.setSessionFactory(sessionFactory);
    }


}
