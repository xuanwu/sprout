package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.LogDao;
import com.sprout.webapp.model.LogData;

@Repository
public class LogDaoImp extends BaseDaoImp<LogData> implements LogDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.LogData";
        super.setSessionFactory(sessionFactory);
    }


}
