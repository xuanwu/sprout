package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.MonitorStatusDao;
import com.sprout.webapp.model.MonitorStatus;

@Repository
public class MonitorWarningDaoImp extends BaseDaoImp<MonitorStatus> implements MonitorStatusDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.MonitorStatus";
        super.setSessionFactory(sessionFactory);
    }


}
