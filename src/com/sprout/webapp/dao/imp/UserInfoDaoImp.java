package com.sprout.webapp.dao.imp;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.UserInfoDao;
import com.sprout.webapp.model.UserInfo;

@Repository
public class UserInfoDaoImp extends BaseDaoImp<UserInfo> implements UserInfoDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.UserInfo";
        super.setSessionFactory(sessionFactory);
    }

    // 添加用户实体
    public void insert(UserInfo userInfo) {
        this.getHibernateTemplate().save(userInfo);
    }

    // 删除用户
    public void delete(UserInfo userInfo) {
        this.getHibernateTemplate().delete(userInfo);
    }

    // 更新实用户信息
    public void update(UserInfo userInfo) {
        this.getHibernateTemplate().update(userInfo);
    }

    // 查询用户信息(列表)
    public List<UserInfo> queryAll() {
        // List<UserInfo> ui = this.getHibernateTemplate().find("from List_spm_userinfo");
        List<UserInfo> ui = this.getHibernateTemplate().find("from UserInfo");
        if (ui.size() == 0 || ui == null)
            return null;
        else
            return ui;
    }

    // 查询用户信息(个人)
    public UserInfo queryUserInfo(String usercode) {
        // List<UserInfo> ui =
        // this.getHibernateTemplate().find("from List_spm_userinfo u where u.usercode=?",
        // usercode);
        List<UserInfo> ui =
                this.getHibernateTemplate().find("from UserInfo u where u.usercode=?", usercode);
        if (ui.size() == 0 || ui == null)
            return null;
        else
            return ui.get(0);
    }

    // 判断用户是否登录
    @SuppressWarnings("unchecked")
    public UserInfo isUserLogin(final UserInfo userInfo) {
        // String hql =
        // "FROM List_spm_userinfo AS userInfo WHERE userInfo.usercode = ? AND userInfo.userpass = ?";
        String hql =
                "FROM UserInfo AS userInfo WHERE userInfo.usercode = ? AND userInfo.userpass = ?";
        List<UserInfo> list =
                (List<UserInfo>) this.findBySQLList(hql, userInfo.getUsercode(),
                        userInfo.getUserpass());
        if (list.size() == 0) {
            return null;
        } else {
            UserInfo user = (UserInfo) list.get(0);
            return user;
        }
    }

    // 判断用户名是否存在
    @SuppressWarnings("unchecked")
    public UserInfo checkUserName(final String userName) {
        // String hql = "FROM List_spm_userinfo AS userInfo WHERE userInfo.usercode = ?";
        String hql = "FROM UserInfo AS userInfo WHERE userInfo.usercode = ?";
        List<UserInfo> list = (List<UserInfo>) this.findBySQLList(hql, userName);
        if (list.size() == 0) {
            return null;
        } else {
            UserInfo user = (UserInfo) list.get(0);
            return user;
        }
    }


}
