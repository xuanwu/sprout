package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.AppServerDao;
import com.sprout.webapp.model.Tomcats;

;
@Repository
public class TomcatsDaoImp extends BaseDaoImp<Tomcats> implements AppServerDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.Tomcats";
        super.setSessionFactory(sessionFactory);
    }


}
