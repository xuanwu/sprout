package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.CategoryDao;
import com.sprout.webapp.model.Category;

@Repository
public class CategoryDaoImp extends BaseDaoImp<Category> implements CategoryDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.Category";
        super.setSessionFactory(sessionFactory);
    }


}
