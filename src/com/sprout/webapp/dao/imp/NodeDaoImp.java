package com.sprout.webapp.dao.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprout.webapp.dao.NodeDao;
import com.sprout.webapp.model.Node;

@Repository
public class NodeDaoImp extends BaseDaoImp<Node> implements NodeDao {

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory) {
        super.model = "com.sprout.webapp.model.Node";
        super.setSessionFactory(sessionFactory);
    }


}
