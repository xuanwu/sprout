package com.sprout.webapp.dao;

import com.sprout.webapp.model.NodeStatus;

public interface NodeStatusDao extends BaseDao<NodeStatus>{
	
}
