package com.sprout.webapp.dao;

import com.sprout.webapp.model.ServiceInfo;

/**
 * 
 * @author xiaocai
 *
 */

public interface ServiceInfoDao extends BaseDao<ServiceInfo> {

}
