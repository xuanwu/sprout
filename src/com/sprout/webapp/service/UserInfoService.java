package com.sprout.webapp.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.utils.ServiceResult;

public interface UserInfoService {

	ServiceResult addUserInfo(UserInfo userInfo);
	
	ServiceResult deleteUserInfo(UserInfo userInfo);
	
	ServiceResult updateUserInfo(UserInfo userInfo);
	
	UserInfo getUserInfo(String usercode);//UserInfo userInfo);
	
	List<UserInfo> getUserList();
	
	boolean loginCheck(UserInfo userInfo, HttpSession session);
	
	List<Integer> getUserFriend(int userId);
	
	 UserInfo isLogin(UserInfo userInfo );
}
