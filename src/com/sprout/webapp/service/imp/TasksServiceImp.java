// package com.sprout.webapp.service.imp;
//
// import java.text.SimpleDateFormat;
// import java.util.Date;
// import java.util.List;
// import java.util.Map;
// import java.util.concurrent.ArrayBlockingQueue;
// import java.util.concurrent.BlockingQueue;
//
// import javax.servlet.http.HttpSession;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Propagation;
// import org.springframework.transaction.annotation.Transactional;
//
// import com.sprout.webapp.dao.ApplicationsDao;
// import com.sprout.webapp.dao.TasksDao;
// import com.sprout.webapp.model.Applications;
// import com.sprout.webapp.model.Tasks;
// import com.sprout.webapp.service.TasksService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
//
// @Service
// @Transactional(propagation = Propagation.REQUIRED)
// public class TasksServiceImp extends BaseService implements TasksService {
//
// @Autowired
// private TasksDao tasksDao;
//
// @Autowired
// private ApplicationsDao applicationsDao;
//
// // @Autowired
// // private DispatchService dispatcher;
//
// // 数据库事务
// @Transactional(propagation = Propagation.REQUIRED)
// public ServiceResult addTasks(Applications app, Tasks tasks) {
// tasks.setAppId(app.getId());
// tasks.setAppType(app.getAppType());
// tasks.setJarPath(app.getJarPath());
// tasks.setConfigFile(app.getConfigFile());
// tasks.setCpu(app.getCpu());
// tasks.setMemory(app.getMemory());
// tasks.setMapProgress(0f);
// tasks.setReduceProgress(0f);
// tasks.setStatus(1);
// tasks.setFinalStatus(0);
// try {
// SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// Date date = new Date();
// String dateCreated = formatter.format(date);
// tasks.setCreatedDate(dateCreated);
//
// tasksDao.insert(tasks);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
//
// return successStringResult(JsonUtil.toJsonText(app));
// }
//
// public Applications getApplication(Integer appId) {
// Applications app = applicationsDao.get(appId);
// return app;
// }
//
// /** 默认配置文件 */
// private static String filename =
// "D://Workspaces//MyEclipse 9//sproutserver//src//com//sprout//server//util//student.xml";
//
// final static BlockingQueue<String> queue = new ArrayBlockingQueue<String>(8);
//
// // // 数据库事务
// // @Transactional(propagation = Propagation.REQUIRED)
// // public ServiceResult submitTask(Tasks tasks, HttpSession session) {
// //
// // String configPath = tasks.getConfigFile();
// // configPath = "//upload//student.xml";
// // String realPath = session.getServletContext().getRealPath(configPath);
// //
// // Map<String, Object> mapCache = TaskConfiguration.initConfig(realPath);
// //
// // tasks.setLogsPath(mapCache.get("log_file") + "");
// // // mapCache.put("jarPath", "");
// // // String myshell =
// // //
// //
// "hadoop jar /home/zhou/workplace/share/hadoop-mapreduce-examples-2.2.0.jar wordcount /test/test.txt /test/outputtest26 > /home/zhou/workplace/share/hadoop.log 2>&1";
// //
// // try {
// // String stringShell = buildShell(mapCache);
// //
// // System.out.println("stringShell---->" + stringShell);
// //
// // queue.put(stringShell);
// // MapReduceService.start(stringShell);
// // //
// // // FindJobUtil job =new FindJobUtil(tasks.getId(), tasks.getLogsPath());
// // // Thread thread = new Thread(job);
// // // thread.start();
// //
// // SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// // Date date = new Date();
// // String dateCreated = formatter.format(date);
// // tasks.setStartTime(dateCreated);
// // // tasks.setStatus(2);//running
// // tasksDao.update(tasks);
// // } catch (Exception e) {
// // e.printStackTrace();
// // return failResult();
// // } catch (Throwable e) {
// // e.printStackTrace();
// // }
// // return successStringResult("启动成功!");
// // }
// //
// // public String getProgressTask(String appId) {
// // TaskStatus status = YarnTasksTracker.monitorTaskTracker(appId);
// //
// // return JsonUtil.toJsonText(status);
// // }
//
// public String buildShell(Map<String, Object> mapCache) throws Throwable {
//
// if (mapCache.get("job_name") == null) {
// throw new Exception("job_name is null!");
// }
// if (mapCache.get("jarPath") == null) {
// throw new Exception("jarPath is null!");
// }
// if (mapCache.get("input_file") == null) {
// throw new Exception("input_file is null!");
// }
// if (mapCache.get("output_file") == null) {
// throw new Exception("output_file is null!");
// }
//
// StringBuilder builder = new StringBuilder();
// builder.append("/home/zhou/workplace/hadoop-2.2.0/bin/hadoop jar ");
// builder.append(mapCache.get("jarPath") + " ");
// builder.append(mapCache.get("job_name") + " ");
// builder.append(mapCache.get("input_file") + " ");
// builder.append(mapCache.get("output_file"));
//
// if (mapCache.get("log_file") != null) {
// builder.append(" >" + mapCache.get("log_file") + " 2>&1");
// }
//
// return builder.toString();
// }
//
//
// @Transactional(propagation = Propagation.REQUIRED)
// // 数据库事务
// public ServiceResult deleteTasks(Integer tasksId) {
// try {
// tasksDao.delete(tasksId);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
// return successStringResult("删除成功");
// }
//
// @Transactional(propagation = Propagation.REQUIRED)
// // 数据库事务
// public ServiceResult updateTasks(Tasks tasks) {
// try {
// tasksDao.update(tasks);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
// return successStringResult("success");
// }
//
// @Override
// public List<Tasks> getTasksList() {
// return tasksDao.findAll();
// }
//
// public ServiceResult getUserAppList(Integer userId) {
// List<Tasks> list = tasksDao.find("userId", userId);
// return successObjectResult(list);
// }
//
// // public ServiceResult getTaskResult(Integer appId) {
// // Tasks task = tasksDao.get(appId);
// //
// // if (task == null) {
// // return failStringResult("task is null");
// // }
// //
// // String outPut = task.getResultPath();
// //
// // outPut = outPut + "/part-r-00000";
// // String result = null;
// // try {
// // result = HDFSUtils.readFileString(outPut);
// // } catch (IOException e) {
// // // TODO Auto-generated catch block
// // e.printStackTrace();
// // return failStringResult("result is error");
// // }
// // return successObjectResult(result);
// // }
// //
// // public ServiceResult getLogFile(int taskId) {
// // Tasks task = tasksDao.get(taskId);
// // if (task == null) {
// // return failStringResult("task is null");
// // }
// //
// // String log_path = task.getLogsPath();
// //
// // try {
// // String result_file = TasksServiceImp.readFileString(log_path);
// // if (result_file != null) {
// // return successObjectResult(result_file);
// // }
// // } catch (IOException e) {
// // // TODO Auto-generated catch block
// // e.printStackTrace();
// // }
// // return failStringResult("result is error");
// // }
// //
// // public static String readFileString(String dst) throws IOException {
// //
// // StringBuilder builder = new StringBuilder();
// // // Buffer
// // BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(dst)));
// // String temp = null;
// // while ((temp = reader.readLine()) != null) {
// // builder.append(temp);
// // builder.append("\r\n");
// // }
// // return builder.toString();
// // }
// //
// //
// //
// // // @Transactional(propagation = Propagation.REQUIRED)
// // public ServiceResult getUserRunningApp(Integer userId, Integer appId) {
// // System.out.println("getDispatcherId-getUserRunningApp->"
// // + this.dispatcher.getDispatcherId());
// // String hql =
// // "from Taskattempt task where task.userId=" + userId + " and task.appId=" + appId
// // + " and task.status in (1,2)";
// // List<Tasks> list = tasksDao.query(hql);
// // // List<Tasks> list = tasksDao.find("appId", userId);
// // // String hql =
// // //
// //
// "select * from taskattempt task where task.userId=? and task.appId=? and task.status in (1,2)";
// // // List<Tasks> list = tasksDao.doSqlQuery(hql,userId,appId);
// // System.out.println(hql);
// //
// // Tasks task = null;
// // if (list.size() != 0) {
// // task = list.get(0);
// // if (task.getJobId() != null) {
// // // YarnTasksTracker.monitorTaskTracker(task.getJobId(), task);
// // // updateTasks(task);
// // }
// // } else {
// // return successObjectResult(list);
// // }
// // return successObjectResult(list);
// // }
// //
// //
// // public ServiceResult getHistoryTasks(Integer userId, Integer appId) {
// // System.out.println("getDispatcherId-getHistoryTasks->" + this.dispatcher.getDispatcherId());
// // String hql =
// // "from Taskattempt task where task.userId=? and task.appId=? and task.status in (0,3,5)";
// // System.out.println(hql);
// // List<Tasks> list = tasksDao.findByHQL(hql, userId, appId);
// //
// // // if(list.size()==0){
// // // successObjectResult(list);
// // // }
// // return successObjectResult(list);
// // }
//
// public Tasks checkUserRunningTask(Integer userId, Integer appId) {
// String hql =
// "from Taskattempt task where task.userId=" + userId + " and task.appId=" + appId
// + " and task.status in (1,2)";
// System.out.println(hql);
// List<Tasks> list = tasksDao.query(hql);
//
// Tasks task = null;
// if (list.size() != 0) {
// task = list.get(0);
// } else {
// return null;
// }
// return task;
// }
//
// public ServiceResult getTaskJson(Integer appId) {
//
// Tasks app = tasksDao.get(appId);
//
// return successObjectResult(app);
// }
//
// public Tasks getTasks(Integer taskId) {
//
// Tasks app = tasksDao.get(taskId);
// return app;
// }
//
// @Transactional
// public List<Integer> getUserFriend(int userId) {
// Tasks info = tasksDao.get(userId);
// List<Integer> fiends = null;
// if (fiends.size() == 0) {
// return null;
// }
// return fiends;
//
// }
//
// @Override
// public ServiceResult getUserRunningApp(Integer userId, Integer appId) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public ServiceResult getHistoryTasks(Integer userId, Integer appId) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public ServiceResult submitTask(Tasks tasks, HttpSession session) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public String getProgressTask(String appId) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public ServiceResult getTaskResult(Integer appId) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public ServiceResult getLogFile(int taskId) {
// // TODO Auto-generated method stub
// return null;
// }
//
// }
