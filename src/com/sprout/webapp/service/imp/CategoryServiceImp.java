// package com.sprout.webapp.service.imp;
//
// import java.text.ParseException;
// import java.text.SimpleDateFormat;
// import java.util.Date;
// import java.util.List;
//
// import org.apache.log4j.Logger;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Propagation;
// import org.springframework.transaction.annotation.Transactional;
//
// import com.sprout.webapp.dao.CategoryDao;
// import com.sprout.webapp.model.Category;
// import com.sprout.webapp.pojo.LogTask;
// import com.sprout.webapp.service.ApplicationsService;
// import com.sprout.webapp.service.CategoryService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
//
// @Service
// @Transactional(propagation = Propagation.REQUIRED)
// public class CategoryServiceImp extends BaseService implements CategoryService {
//
// private static Logger logger = Logger.getLogger(CategoryServiceImp.class);
//
// @Autowired
// private CategoryDao categoryDao;
//
// @Autowired
// private ApplicationsService applicationService;
//
// // @Autowired
// // private DispatchService dispatcher;
//
// // 数据库事务
// @Transactional(propagation = Propagation.REQUIRED)
// public ServiceResult addCategory(Category category) {
// try {
// Date date = new Date();
// category.setCreateDate(date.getTime());
//
// category.setStatus(0);
// categoryDao.insert(category);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
// return successStringResult("添加成功!");
// }
//
// // // // 数据库事务
// // @Transactional(propagation = Propagation.REQUIRED)
// // public ServiceResult submitCategory(int cid) {
// //
// // try {
// // Category category = categoryDao.get(cid);
// // if (category != null) {
// // category.setIp("125.216.243.182");// ;125.216.243.183
// // category.setFrequent(1);
// // category.setLogpath("/home/zhou/solfware/logs");
// // category.setType(1);
// //
// // dispatcher.submitApplication(category);
// // }
// //
// // } catch (Exception e) {
// // e.printStackTrace();
// // return failResult();
// // }
// // return successStringResult("添加成功!");
// // }
//
// public String disableCategory(List<LogTask> list) {
// try {
// for (LogTask logTask : list) {
// int id = logTask.getCategoryId();
// String update = "update Category c set c.status = 2 where c.id = " + id;
// categoryDao.executeUpdate(update);
// }
// return "success";
// } catch (Exception e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// return "fail";
// }
//
// @Transactional(propagation = Propagation.REQUIRED)
// // 数据库事务
// public ServiceResult deleteCategory(Category category) {
// try {
// categoryDao.delete(category);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
// return successStringResult("success");
// }
//
// @Transactional(propagation = Propagation.REQUIRED)
// // 数据库事务
// public ServiceResult updateCategory(Category category) {
// try {
// categoryDao.update(category);
// } catch (Exception e) {
// e.printStackTrace();
// return failResult();
// }
// return successStringResult("success");
// }
//
// @Override
// public List<Category> getUserList() {
// return categoryDao.findAll();
// }
//
// public ServiceResult getUserAppList(Integer userId) {
// List<Category> list = categoryDao.find("userId", userId);
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult getCategory(Integer appId) {
//
// Category app = categoryDao.get(appId);
// return successObjectResult(app);
// }
//
// public long getTimeSec() {
// SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");// 设置日期格式
// long timesec = 0L;
// try {
// Date sec = date.parse(date.format(new Date()));
// timesec = sec.getTime() / 1000 - 10;
// } catch (ParseException e) {
// e.printStackTrace();
// }
// return timesec;
// }
//
// @Override
// public ServiceResult getApplicationList(int userId) {
// return applicationService.getCategoryList(userId);
// }
//
// @Override
// public String getAppCategory(Integer appId) {
// String result = null;
// try {
// List<Category> list = categoryDao.find("appId", appId);
// if (list.size() != 0) {
// result = JsonUtil.toJsonText(successObjectResult(list));
// }
// } catch (Exception e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// result = JsonUtil.toJsonText(failResult());
// }
// return result;
// }
//
// public int updateCategory(int id, int status) {
// try {
// Category category = categoryDao.get(id);
// if (category == null) {
// return 0;
// }
// category.setStatus(status);
// categoryDao.update(category);
// } catch (Exception e) {
// e.printStackTrace();
// return 0;
// }
// return 1;
// }
//
// @Override
// public ServiceResult getUserCategoryList(Integer userId) {
// List<Category> list = categoryDao.find("userId", userId);
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult fuzzyQueryCategory(String fuzzyQueryKey, String appId) {
// // TODO Auto-generated method stub
// String sql = "from Category c where c.name like ? and c.appId=" + appId;
//
// List<Category> list = categoryDao.findBySQLList(sql, "%" + fuzzyQueryKey + "%");
// System.out.println("list-->" + list.size());
//
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult getcategoryById(int id) {
// // TODO Auto-generated method stub
//
// String sql = "from Category c where c.id = ?";
// List<Category> list = categoryDao.findBySQLList(sql, id);
// System.out.println("list-->" + list.size());
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult getCategoryByAppId(int appId) {
// // TODO Auto-generated method stub
// String sql = "from Category c where c.appId = ?";
// List<Category> list = categoryDao.findBySQLList(sql, appId);
// System.out.println("list-->" + list.size());
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult getWebStatusById(Integer category_id) {
// // TODO Auto-generated method stub
// String sql = "from Webstatus w where w.category_id=?";
// List<Category> list = categoryDao.findBySQLList(sql, category_id);
// System.out.println("list-->" + list.size());
// return successObjectResult(list);
// }
//
// @Override
// public ServiceResult submitCategory(int cid) {
// // TODO Auto-generated method stub
// return null;
// }
// }
