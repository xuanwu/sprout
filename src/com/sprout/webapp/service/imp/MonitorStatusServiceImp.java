package com.sprout.webapp.service.imp;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.MonitorStatusDao;
import com.sprout.webapp.model.MonitorStatus;
import com.sprout.webapp.service.MonitorStatusService;
import com.sprout.webapp.utils.ServiceResult;
import com.sprouts.spm_framework.enums.MonitorType;
import com.sprouts.spm_framework.model.HttpModel;
import com.sprouts.spm_framework.model.PingModel;
import com.sprouts.spm_framework.model.TcpModel;
import com.sprouts.spm_framework.model.UdpModel;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class MonitorStatusServiceImp extends BaseService implements MonitorStatusService {

    @Autowired
    private MonitorStatusDao monitorDao;

    public ServiceResult createMonitor(MonitorStatus monitor) {
        try {
            monitor.setCreated_time(sqlService.getTimeStamp());
            monitorDao.insert(monitor);

        } catch (Exception e) {
            e.printStackTrace();
            return failResult();
        }
        return successStringMessage("创建成功!");
    }

    public MonitorStatus getMonitor(Integer id) {
        MonitorStatus monitor = null;
        try {
            monitor = monitorDao.get(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return monitor;
    }

    @Override
    public ServiceResult listWebMonitors(Object... objects) {
        List<MonitorStatus> list = null;
        try {
            if (objects.length == 1) {
                String hql = "from MonitorStatus monitor where monitor.userId=?";
                list = monitorDao.findByHQL(hql, (Integer) objects[0]);
            } else if (objects.length == 2) {
                String hql = "from MonitorStatus monitor where monitor.type=? and monitor.userId=?";
                list = monitorDao.findByHQL(hql, (String) objects[0], (Integer) objects[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.successResult(list);
    }

    @Override
    public ServiceResult showUtilityView(String hostname, String name, String type,
            Object... objects) {
        List<Object> list = new ArrayList<Object>();

        sqlService.reInitService();
        String sql = null;
        ResultSet result = null;
        int choosetime = (Integer) objects[0];
        long threshold = System.currentTimeMillis() - choosetime * 24 * 60 * 60 * 1000L; // 选择choosetime时间前的数据做起始点
        threshold = threshold / 1000;
        MonitorType mType = MonitorType.valueOf(type.toUpperCase());
        try {
            switch (mType) {
                case PING:
                    sql =
                            "select * from collect_ping ping where ping.name ='" + name
                                    + "' and ping.ip='" + hostname + "' and timesec>" + threshold;
                    result = sqlService.attachList(sql);
                    while (result.next()) {
                        PingModel ping = new PingModel();
                        ping.setId(result.getInt("id"));
                        ping.setMonitorName(result.getString("name"));
                        ping.setIp(result.getString("ip"));
                        ping.setPack_receive(result.getInt("pack_receive"));
                        ping.setPack_loss(result.getInt("pack_loss"));
                        ping.setPack_send(result.getInt("pack_send"));
                        ping.setAverage_res_time(result.getInt("average_res_time"));
                        ping.setLoss_rate(result.getInt("loss_rate"));
                        ping.setSuccess_rate(result.getInt("success_rate"));
                        ping.setStatus(result.getInt("status"));
                        ping.setTimestamp(result.getString("timestamp"));
                        list.add(ping);
                    }
                    break;
                case HTTP:
                    sql =
                            "select * from collect_http http where http.name ='" + name
                                    + "' and http.site='" + hostname + "' and timesec>" + threshold;
                    result = sqlService.attachList(sql);
                    while (result.next()) {
                        HttpModel http = new HttpModel();
                        http.setId(result.getInt("id"));
                        http.setMonitorName(result.getString("name"));
                        http.setSite(result.getString("site"));
                        http.setStatus(result.getInt("status"));
                        http.setStatus_code(result.getInt("status_code"));
                        http.setAverage_res_time(result.getFloat("average_res_time"));
                        http.setTimestamp(result.getString("timestamp"));
                        list.add(http);
                    }
                    break;
                case TCP:
                    sql =
                            "select * from collect_tcp tcp where tcp.name ='" + name
                                    + "' and tcp.ip='" + hostname + "' and tcp.port=" + objects[1]
                                    + " and timesec>" + threshold;
                    result = sqlService.attachList(sql);
                    while (result.next()) {
                        TcpModel tcp = new TcpModel();
                        tcp.setId(result.getInt("id"));
                        tcp.setMonitorName(result.getString("name"));
                        tcp.setIp(result.getString("ip"));
                        tcp.setPort(result.getInt("port"));
                        tcp.setStatus(result.getInt("status"));
                        tcp.setAverage_res_time(result.getInt("average_res_time"));
                        tcp.setTimestamp(result.getString("timestamp"));
                        list.add(tcp);
                    }
                    break;
                case UDP:
                    sql =
                            "select * from collect_udp udp where udp.name ='" + name
                                    + "' and udp.ip='" + hostname + "' and udp.port=" + objects[1]
                                    + " and timesec>" + threshold;
                    result = sqlService.attachList(sql);
                    while (result.next()) {
                        UdpModel udp = new UdpModel();
                        udp.setId(result.getInt("id"));
                        udp.setMonitorName(result.getString("name"));
                        udp.setIp(result.getString("ip"));
                        udp.setPort(result.getInt("port"));
                        udp.setStatus(result.getInt("status"));
                        udp.setAverage_res_time(result.getInt("average_res_time"));
                        udp.setTimestamp(result.getString("timestamp"));
                        list.add(udp);
                    }
                    break;
                default:
                    return ServiceResult.failResult();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sqlService.close();

        if (list != null && list.size() > 0) {
            return ServiceResult.successResult(list);
        }
        return ServiceResult.failResult();
    }
}
