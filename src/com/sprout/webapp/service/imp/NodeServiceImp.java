package com.sprout.webapp.service.imp;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.NodeDao;
import com.sprout.webapp.dao.NodeStatusDao;
import com.sprout.webapp.model.Node;
import com.sprout.webapp.model.NodeStatus;
import com.sprout.webapp.service.NodeService;
import com.sprout.webapp.utils.DateUtil;
import com.sprout.webapp.utils.ServiceResult;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class NodeServiceImp extends BaseService implements NodeService {

    @Autowired
    private NodeDao nodeDao;

    @Autowired
    private NodeStatusDao statusDao;

    @Override
    public ServiceResult updateNode(Node node) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Node> getNodeList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ServiceResult findNodes() {
        List<Node> nodes = nodeDao.findAll();

        if (nodes != null) {
            return ServiceResult.successResult(nodes);
        }
        return ServiceResult.failResult("null");
    }

    @Override
    public ServiceResult findNodeStatus(String hostname) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String now = formatter.format(date);

        String startTime = DateUtil.addHours(date, -2);

        // , startTime, now
        // String sql =
        // "select * from NodeStatus status where status.hostname =? and (status.created_time between ? and ? )";

        String sql =
                "from Collect_node status where status.hostname =? and (status.created_seconds between 1415289960 and 1415302200 ) ";
        List<NodeStatus> nodes = statusDao.findBySQLList(sql, hostname);

        if (nodes != null) {
            return ServiceResult.successResult(nodes);
        }
        return ServiceResult.failResult("null");
    }

}
