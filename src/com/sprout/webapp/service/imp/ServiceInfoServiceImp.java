package com.sprout.webapp.service.imp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.ServiceInfoDao;
import com.sprout.webapp.model.ServiceInfo;
import com.sprout.webapp.service.ServiceInfoService;
import com.sprout.webapp.utils.ServiceResult;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ServiceInfoServiceImp extends BaseService implements ServiceInfoService{

	private static Logger logger = Logger.getLogger(ServiceInfoServiceImp.class);
	
	@Autowired
	private ServiceInfoDao serviceInfoDao;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public ServiceResult createService(ServiceInfo serviceInfo){
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 Date date = new Date();
			 String dateCreated = formatter.format(date);
			 serviceInfo.setCreatedDate(dateCreated);
			 
			 serviceInfo.setStatus(0);
			 
			 serviceInfoDao.insert(serviceInfo);
			 
		}catch(Exception e){
			e.printStackTrace();
			return failResult();
		}
		return successStringMessage("创建成功!");
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public ServiceResult changeServiceStatus(ServiceInfo serviceInfo,Integer Status){
		try{
			serviceInfo.setStatus(Status);
			
			serviceInfoDao.update(serviceInfo);
			
		}catch(Exception e){
			e.printStackTrace();
			return failResult();
		}
		
		return successResult();
	}
	
	public ServiceResult getAppServiceList(Integer appId) {
		 System.out.println("userId-->"+appId);
		 List<ServiceInfo> list = serviceInfoDao.find("appId", appId);
		 System.out.println("list-->"+list.size());
		return successObjectResult(list);
	}
	
	public ServiceInfo checkUserCreatedService(Integer userId, Integer appId,String serviceType){
		String hql = "from ServiceInfo serinfo where serinfo.userId="+userId+" and serinfo.appId="+appId+" and serinfo.servicetype="+"'"+serviceType+"'";
		//System.out.println(hql);
		System.out.println("hqlquery");
		List<ServiceInfo> list = serviceInfoDao.query(hql);
		
		ServiceInfo serviceinfo=null;
		if(list.size()!=0){
			serviceinfo = list.get(0);
		}else{
			return null;
		}
		return serviceinfo;
	}
}
