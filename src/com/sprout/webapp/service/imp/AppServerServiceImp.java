package com.sprout.webapp.service.imp;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.AppServerDao;
import com.sprout.webapp.model.Tomcats;
import com.sprout.webapp.pojo.TomcatStatus;
import com.sprout.webapp.service.AppServerService;
import com.sprout.webapp.utils.ServiceResult;
import com.sprouts.spm_framework.sql.SQLService;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class AppServerServiceImp extends BaseService implements AppServerService {

    @Autowired
    private AppServerDao appServerDao;

    private SQLService sqlService = new SQLService();

    @Override
    public ServiceResult create(Tomcats monitor) {
        try {
            monitor.setCreated_time(sqlService.getTimeStamp());
            appServerDao.insert(monitor);
        } catch (Exception e) {
            e.printStackTrace();
            return failResult();
        }
        return successStringMessage("创建成功!");
    }

    @Override
    public ServiceResult update(Tomcats node) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ServiceResult lists(String username) {
        List<Tomcats> list = null;
        try {
            String sql = "from Tomcats tom where tom.user=?";
            list = appServerDao.findByHQL(sql, username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.successResult(list);
    }

    @Override
    public ServiceResult getAppServerStatus(String host, int port, int choosetime) {

        String sql = null;
        long threshold = System.currentTimeMillis() - choosetime * 24 * 60 * 60 * 1000L;
        threshold = threshold / 1000;
        sql =
                "SELECT * FROM collect_appserver s WHERE ip='" + host + "' and port=" + port
                        + " and timesec>" + threshold + " ORDER BY id ASC";
        System.out.println(choosetime);
        System.out.println("select sql ----->" + sql);
        List<TomcatStatus> list = null;

        sqlService.reInitService();
        ResultSet set = sqlService.attachList(sql);

        try {
            if (set != null) {
                list = this.handleResult(set);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            return ServiceResult.successResult(list);
        }
        return ServiceResult.failResult();
    }

    private List<TomcatStatus> handleResult(ResultSet set) {
        List<TomcatStatus> list = new ArrayList<TomcatStatus>();
        try {
            while (set.next()) {
                TomcatStatus status = new TomcatStatus();
                status.setAlive_thread(set.getInt("aliveThreadCount"));
                status.setCpu_usage((double) set.getFloat("cpuUsage"));
                status.setMem_usage(set.getFloat("memused"));
                status.setTime_current(set.getString("timestamp"));
                status.setId(set.getInt("id"));
                list.add(status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
