package com.sprout.webapp.service.imp;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.MonitorWarningDao;
import com.sprout.webapp.model.MonitorWarning;
import com.sprout.webapp.service.MonitorWarningService;
import com.sprout.webapp.utils.ServiceResult;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class MonitorWarningServiceImp extends BaseService implements MonitorWarningService {

    @Autowired
    private MonitorWarningDao monitorDao;


    @Override
    public ServiceResult createMonitor(MonitorWarning monitor) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String dateCreated = formatter.format(date);
            monitor.setCreated_time(dateCreated);
            monitor.setIs_start(1);
            monitorDao.insert(monitor);

        } catch (Exception e) {
            e.printStackTrace();
            return failResult();
        }
        return successStringMessage("创建成功!");
    }

    @Override
    public ServiceResult updateMonitor(MonitorWarning monitor) {
        MonitorWarning warning = this.getMonitor(monitor.getId());
        try {
            monitorDao.update(monitor);
        } catch (Exception e) {
            e.printStackTrace();
            return failResult();
        }
        return successStringMessage("更新成功!");
    }

    @Override
    public ServiceResult deleteMonitor(Integer id) {
        try {
            monitorDao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return failResult();
        }
        return successStringMessage("删除成功!");
    }

    public MonitorWarning getMonitor(Integer id) {
        MonitorWarning warning = null;
        try {
            warning = monitorDao.get(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return warning;
    }


    @Override
    public ServiceResult updateMonitorWarning(MonitorWarning monitor) {
        // TODO Auto-generated method stub
        return null;
    }

    public ServiceResult findWarnings(String host, String type) {
        String sql = null;
        List<MonitorWarning> list = null;
        if (type == null) {
            sql = "from MonitorWarning w where w.hostname='" + host + "'";
        } else {
            sql = "from MonitorWarning w where w.hostname='" + host + "' and type=" + type;
        }
        try {
            list = monitorDao.findByHQL(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.successResult(list);
    }

    public ServiceResult findWarnings(String type) {
        String sql = null;
        List<MonitorWarning> list = null;
        if (type == null) {
            sql = "from MonitorWarning   ";
        } else {
            sql = "from MonitorWarning w where type=" + type;
        }
        try {
            list = monitorDao.findByHQL(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.successResult(list);
    }

    // public ServiceResult findWarningById(String host, Integer id){
    // List<MonitorWarning> list =null;
    // String sql = "from MonitorWarning w where w.hostname='"+host+"' and monitorId="+id;
    // try {
    // list = monitorDao.findByHQL(sql);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // return ServiceResult.successResult(list);
    // }

    public ServiceResult findWarningById(Integer id) {
        List<MonitorWarning> list = null;
        String sql = "from MonitorWarning w where monitorId=" + id;
        try {
            list = monitorDao.findByHQL(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.successResult(list);
    }


}
