package com.sprout.webapp.service.imp;


import com.sprout.webapp.utils.ServiceResult;
import com.sprouts.spm_framework.sql.SQLService;

/**
 * 各种服务基础类:验证/统一返回结果
 * 
 * @author zhou
 * 
 */
public class BaseService {

    public SQLService sqlService = new SQLService();

    /**
     * 验证用户名
     * 
     * @param nick
     * @return
     */
    protected ServiceResult verifyUserInfo(String nick) {
        // 验证成功,返回验证成功的活动对象
        return successResult();
    }

    protected ServiceResult failResult() {
        return ServiceResult.failResult();
    }

    protected ServiceResult failResult(int errorCode) {
        return ServiceResult.failResult(errorCode);
    }

    /**
     * 返回错误信息
     * 
     * @param errorMessage 错误内容
     * @return
     */
    protected ServiceResult failStringResult(String errorMessage) {
        return ServiceResult.failResult(errorMessage);
    }

    /**
     * 返回错误信息
     * 
     * @param errorMessage 错误内容
     * @param errorObject 错误对象
     * @return
     */
    protected ServiceResult failResult(String errorMessage, Object errorobject) {
        return ServiceResult.failResult(errorMessage, errorobject);
    }

    /**
     * 返回成功结果
     * 
     * @return
     */
    protected ServiceResult successResult() {
        return ServiceResult.successResult();
    }

    /**
     * 返回成功结果
     * 
     * @param object
     * @return
     */
    protected ServiceResult successObjectResult(Object object) {
        return ServiceResult.successResult(object);
    }

    /**
     * 结果对象是字符串的结果
     * 
     * @param strResult
     * @return
     */
    protected ServiceResult successStringResult(String strResult) {
        return ServiceResult.successResult(strResult, "");
    }

    protected ServiceResult successStringMessage(String strResult) {
        return ServiceResult.successResult("", strResult);
    }

}
