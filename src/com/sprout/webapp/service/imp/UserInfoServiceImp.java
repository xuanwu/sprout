package com.sprout.webapp.service.imp;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sprout.webapp.dao.UserInfoDao;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.UserInfoService;
import com.sprout.webapp.utils.MD5code;
import com.sprout.webapp.utils.ServiceResult;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserInfoServiceImp extends BaseService implements UserInfoService {

	@Autowired
	private UserInfoDao userInfoDao;

	@Transactional(propagation = Propagation.REQUIRED)
	// 数据库事务
	public ServiceResult addUserInfo(UserInfo userInfo) {
		try {
			userInfoDao.insert(userInfo);
		} catch (Exception e) {
			e.printStackTrace();
			return failResult();
		}
		return successStringResult("success");
	}

	@Transactional(propagation = Propagation.REQUIRED)
	//数据库事务
	public ServiceResult deleteUserInfo(UserInfo userInfo){
		try {
			userInfoDao.delete(userInfo);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return failResult();
		}
		return successStringResult("success");
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	//数据库事务
	public ServiceResult updateUserInfo(UserInfo userInfo)
	{
		try {
			userInfoDao.update(userInfo);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return failResult();
		}
		return successStringResult("success");
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	// 数据库事务
	public UserInfo getUserInfo(String usercode) {
		return userInfoDao.queryUserInfo(usercode);//userInfo.getId());

	}

	public boolean loginCheck(UserInfo userInfo, HttpSession session) {
		boolean flag = false;
		UserInfo user = null;//userInfoDao.loginCheck(userInfo);
		if (user != null) {
			session.putValue("currentUser", user);
			flag = true;
		}
		return flag;

	}
	
	public UserInfo isLogin(UserInfo userInfo ){
		MD5code md5 = new MD5code();
		userInfo.setUserpass(md5.getMD5ofStr(userInfo.getUserpass()));
		
		userInfo=userInfoDao.isUserLogin(userInfo);
		//System.out.println("asshole is "+userInfo.getUsercode());
		return userInfo;
	}
	

	@Override
	public List<UserInfo> getUserList() {
		return userInfoDao.queryAll();
	}

	@Transactional
	public List<Integer> getUserFriend(int userId) {
		UserInfo info = userInfoDao.get(userId);
		List<Integer> fiends = null;
		if(fiends.size()==0){
			return null;
		}
		return fiends;

	}
}
