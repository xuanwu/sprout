package com.sprout.webapp.service;

import java.util.List;

import com.sprout.webapp.model.Node;
import com.sprout.webapp.utils.ServiceResult;

public interface NodeService {

	ServiceResult updateNode(Node node);
	
	List<Node> getNodeList();
	
	ServiceResult findNodes();

	ServiceResult findNodeStatus(String hostname);
	
}
