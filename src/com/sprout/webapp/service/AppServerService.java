package com.sprout.webapp.service;

import com.sprout.webapp.model.Tomcats;
import com.sprout.webapp.utils.ServiceResult;

public interface AppServerService {

    ServiceResult update(Tomcats node);

    ServiceResult lists(String host);

    ServiceResult create(Tomcats monitor);

    ServiceResult getAppServerStatus(String host, int port, int top);
}
