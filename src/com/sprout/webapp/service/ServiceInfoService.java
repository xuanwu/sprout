package com.sprout.webapp.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sprout.webapp.model.ServiceInfo;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.utils.ServiceResult;

public interface ServiceInfoService {
	
	ServiceResult createService(ServiceInfo serviceInfo);
	
	ServiceResult changeServiceStatus(ServiceInfo serviceInfo,Integer Status);
	
	ServiceInfo checkUserCreatedService(Integer userId, Integer appId,String serviceType);
	
	ServiceResult getAppServiceList(Integer appId);

}
