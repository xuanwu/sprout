// package com.sprout.webapp.service;
//
// import java.util.List;
//
// import javax.servlet.http.HttpSession;
//
// import com.sprout.webapp.model.Applications;
// import com.sprout.webapp.model.Tasks;
// import com.sprout.webapp.model.UserInfo;
// import com.sprout.webapp.utils.ServiceResult;
//
// public interface TasksService {
//
// ServiceResult addTasks(Applications app , Tasks tasks);
//
// ServiceResult updateTasks(Tasks tasks);
//
// ServiceResult deleteTasks(Integer tasksId);
//
// Tasks getTasks(Integer taskId);
//
// ServiceResult getTaskJson(Integer appId);
//
// ServiceResult getUserAppList(Integer userId);
//
// ServiceResult getUserRunningApp(Integer userId, Integer appId);
//
// ServiceResult getHistoryTasks(Integer userId, Integer appId) ;
//
// ServiceResult submitTask(Tasks tasks, HttpSession session) ;
//
// List<Tasks> getTasksList();
//
// Tasks checkUserRunningTask(Integer userId, Integer appId);
//
// Applications getApplication(Integer appId);
//
// String getProgressTask(String appId);
//
// ServiceResult getTaskResult(Integer appId);
//
// ServiceResult getLogFile(int taskId);
// }
