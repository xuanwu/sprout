package com.sprout.webapp.service;

import com.sprout.webapp.model.MonitorWarning;
import com.sprout.webapp.utils.ServiceResult;

public interface MonitorWarningService {

	ServiceResult updateMonitorWarning(MonitorWarning monitor);

	ServiceResult createMonitor(MonitorWarning monitor);

	ServiceResult findWarningById( Integer id);

	ServiceResult findWarnings(String host, String type);

	ServiceResult findWarnings(String type);
	
	ServiceResult updateMonitor(MonitorWarning monitor);

	ServiceResult deleteMonitor(Integer id);
	
	MonitorWarning getMonitor(Integer id);
}
