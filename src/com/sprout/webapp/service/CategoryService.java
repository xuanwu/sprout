package com.sprout.webapp.service;

import java.util.List;

import com.sprout.webapp.model.Category;
import com.sprout.webapp.pojo.LogTask;
import com.sprout.webapp.utils.ServiceResult;

public interface CategoryService {

    ServiceResult addCategory(Category category);

    ServiceResult deleteCategory(Category category);

    ServiceResult updateCategory(Category category);

    List<Category> getUserList();

    ServiceResult getUserAppList(Integer userId);

    ServiceResult getCategory(Integer appId);

    String getAppCategory(Integer appId);

    int updateCategory(int id, int status);

    ServiceResult submitCategory(int cid);

    String disableCategory(List<LogTask> list);

    ServiceResult getApplicationList(int userId);

    ServiceResult getUserCategoryList(Integer id);

    ServiceResult fuzzyQueryCategory(String fuzzyQueryKey, String string);

    ServiceResult getcategoryById(int parseInt);

    ServiceResult getCategoryByAppId(int parseInt);


    ServiceResult getWebStatusById(Integer category_id);

}
