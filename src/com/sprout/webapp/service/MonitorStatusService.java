package com.sprout.webapp.service;

import com.sprout.webapp.model.MonitorStatus;
import com.sprout.webapp.utils.ServiceResult;

public interface MonitorStatusService {

    ServiceResult listWebMonitors(Object... objs);

    ServiceResult showUtilityView(String hostname, String name, String type, Object... objects);

    ServiceResult createMonitor(MonitorStatus monitor);

    MonitorStatus getMonitor(Integer id);
}
