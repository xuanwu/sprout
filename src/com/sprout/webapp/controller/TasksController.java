// package com.sprout.webapp.controller;
//
// import java.util.Map;
//
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.servlet.ModelAndView;
//
// import com.sprout.webapp.model.Applications;
// import com.sprout.webapp.model.Tasks;
// import com.sprout.webapp.model.UserInfo;
// import com.sprout.webapp.service.TasksService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
// import com.sprout.webapp.utils.TaskConfiguration;
//
// @Controller
// public class TasksController {
//
// @Autowired
// private TasksService tasksService;
//
// @RequestMapping(value = "/task/add", method = RequestMethod.POST)
// @ResponseBody
// public String addTasks(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// UserInfo user = getCurrentUser(request);
// String app = request.getParameter("id");
// String taskname = request.getParameter("taskname");
// // String jarfile = request.getParameter("jarfile");
//
// Applications application = tasksService.getApplication(Integer.parseInt(app));
// if(application==null){
// return JsonUtil.toJsonText(ServiceResult.failResult());
// }
// ServiceResult result = null;
// Tasks task = new Tasks();
// task.setTaskname(taskname);
// task.setUserId(user.getId());
//
// result = tasksService.addTasks(application, task);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/task/submit", method = RequestMethod.POST)
// @ResponseBody
// public String submitTask(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// UserInfo user = getCurrentUser(request);
// String appId = request.getParameter("taskId");
// Tasks runtask = tasksService.checkUserRunningTask(user.getId(),
// Integer.parseInt(appId));
// ServiceResult result = null;
// // if (runtask != null) {
// // result = ServiceResult.failResult("task running exsit", runtask);
// // return JsonUtil.toJsonText(result);
// // }
// HttpSession session = request.getSession();
//
// Tasks task = tasksService.getTasks(Integer.parseInt(appId));
// if (task != null) {
// result = tasksService.submitTask(task, session);
// } else {
// result = ServiceResult.failResult("task is null!");
// }
//
// return JsonUtil.toJsonText(result);
// }
//
// public UserInfo getCurrentUser(HttpServletRequest request) {
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
// return user;
// }
//
// // ------------------------------------------------------
// @RequestMapping(value = "/task/delete", method = RequestMethod.POST)
// @ResponseBody
// public String deleteTasks(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String taskId=request.getParameter("taskId");
// System.out.println(taskId);
// ServiceResult result=tasksService.deleteTasks(Integer.parseInt(taskId));
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/task/modify", method = RequestMethod.POST)
// @ResponseBody
// public String modifyTasks(Tasks tasks, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// ServiceResult result = tasksService.updateTasks(tasks);
// return JsonUtil.toJsonText(result);
// }
//
// // 这是查询个人信息的
// @RequestMapping(value = "/task/query", method = RequestMethod.POST)
// @ResponseBody
// public String queryTasks(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String appId = request.getParameter("appId");
// String result = tasksService.getProgressTask(appId);
// return result;
// }
//
// // ------------------------------------------------------
// @RequestMapping(value = "/task/queryone", method = RequestMethod.POST)
// public ModelAndView getOneTask(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView();
// String taskId=request.getParameter("taskId");
// Tasks task= tasksService.getTasks(Integer.parseInt(taskId));
// ServiceResult result=ServiceResult.successResult(task);
// mv.addObject("result", result);
// mv.setViewName("instance/instanceDetail");
//
// return mv;
// }
// // 这是查看所有用户的
// @RequestMapping(value = "/task/history", method = RequestMethod.POST)
// public ModelAndView getTasks(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView();
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
// String appId = request.getParameter("id");
// Integer userId = user.getId();
//
// ServiceResult result = tasksService.getHistoryTasks(userId, Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("instance/instance_history");
//
// return mv;
// }
//
// // 这是查看所有用户的
// @RequestMapping(value = "/task/running", method = RequestMethod.POST)
// public ModelAndView getRunningTask(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView();
// UserInfo user = getCurrentUser(request);
//
// String appId = request.getParameter("id");
// Integer userId = user.getId();
//
// ServiceResult result = tasksService.getUserRunningApp(userId,
// Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("instance/instance_detail");
//
// return mv;
// }
//
// // 这是查看所有用户的
// @RequestMapping(value = "/task/readlogs", method = RequestMethod.POST)
// public ModelAndView readTaskLogs(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView();
//
// UserInfo user = getCurrentUser(request);
//
// String appId = request.getParameter("id");
// Integer userId = user.getId();
//
// ServiceResult result = tasksService.getUserRunningApp(userId,
// Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("instance_home");
//
// return mv;
// }
//
// // 这是查看所有用户的
// @RequestMapping(value = "/task/readresult")
// @ResponseBody
// public String readTaskResult(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// String appId = request.getParameter("taskId");
// ServiceResult result = tasksService.getTaskResult(Integer.parseInt(appId));
// return JsonUtil.toJsonText(result);
// }
//
//
// @RequestMapping(value = "/task/view", method = RequestMethod.POST)
// public ModelAndView viewApplication(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView();
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// String appId = request.getParameter("id");
//
// ServiceResult result = tasksService
// .getTaskJson(Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("home/applicationInfo");
// return mv;
// }
//
//
// @RequestMapping(value="/task/getlog")
// @ResponseBody
// public String getLogFile(HttpServletRequest request,
// HttpServletResponse response){
// String taskId = request.getParameter("taskId");
// ServiceResult result = tasksService.getLogFile(Integer.parseInt(taskId));
// return JsonUtil.toJsonText(result);
//
// }
//
// }
