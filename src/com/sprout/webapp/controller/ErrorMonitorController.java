package com.sprout.webapp.controller;

// import java.sql.ResultSet;
// import java.sql.SQLException;
// import java.util.ArrayList;
// import java.util.List;
//
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
//
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.servlet.ModelAndView;
//
// import com.sprout.webapp.pojo.ErrorInfo;
// import com.sprout.webapp.pojo.ErrorMonitor;
// import com.sprout.webapp.utils.ServiceResult;
// import com.sprout.webapp.utils.TasksDb;
// @Controller
// public class ErrorMonitorController {
//
//
// @RequestMapping(value = "/error/list")
// public ModelAndView submitCategory(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String host = request.getParameter("host");
// ModelAndView mv = new ModelAndView();
// ServiceResult result = ServiceResult.successResult(this.getMonitorList());
// mv.addObject("result", result);
// mv.setViewName("weblogs/error/warninglist_content");
// return mv;
// }
//
// public List<ErrorMonitor> getMonitorList(){
// TasksDb database = new TasksDb();
// database.connectDatabase();
// String sql = "select * from error_monitor ";
// ResultSet set = database.querySql(sql);
// List<ErrorMonitor> list = new ArrayList<ErrorMonitor>();
// try {
// while (set.next()) {
// ErrorMonitor monitor = new ErrorMonitor();
// monitor.setHost(set.getString("host"));
// monitor.setName(set.getString("name"));
// monitor.setPath(set.getString("path"));
// monitor.setStatus(set.getInt("status"));
// list.add(monitor);
// }
// } catch (SQLException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// return list;
// }
//
// @RequestMapping(value = "/error/collect")
// public ModelAndView getErrorList(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String host = request.getParameter("host");
// ModelAndView mv = new ModelAndView();
// ServiceResult result = ServiceResult.successResult(this.getErrorList(host));
// mv.addObject("result", result);
// mv.setViewName("weblogs/error/error_detail");
// return mv;
// }
//
// public List<ErrorInfo> getErrorList(String host){
// TasksDb database = new TasksDb();
// database.connectDatabase();
// String sql = "select * from error_monitor_collect ";
// ResultSet set = database.querySql(sql);
// List<ErrorInfo> list = new ArrayList<ErrorInfo>();
// try {
// while (set.next()) {
// ErrorInfo info = new ErrorInfo();
// info.setCreated_date(set.getString("created_date"));
// info.setDescrible(set.getString("describle"));
// info.setError(set.getString("error"));
// info.setHost(host);
// list.add(info);
// }
// } catch (SQLException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// return list;
// }
//
//
// }
