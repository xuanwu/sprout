package com.sprout.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.MonitorStatus;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.MonitorStatusService;
import com.sprout.webapp.service.NodeService;
import com.sprout.webapp.utils.JsonUtil;
import com.sprout.webapp.utils.ServiceResult;
import com.sprouts.spm_framework.amq.AmqConfig;
import com.sprouts.spm_framework.amq.AmqSender;
import com.sprouts.spm_framework.amq.message.WebTaskMessage;
import com.sprouts.spm_framework.enums.MonitorType;
import com.sprouts.spm_framework.utils.SPMConstants;

@Controller
public class WebMonitorController {

    @Autowired
    private NodeService nodeService;

    @Autowired
    private MonitorStatusService monitorService;

    /**
     * 获取监控列表
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/webstatus")
    public ModelAndView showMonitorList(HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");
        String html = request.getParameter("responehtml");
        HttpSession session = request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("currentUser");
        ServiceResult result = null;
        if (type == null) {
            result = ServiceResult.failResult("parameter error");
        } else {
            result = monitorService.listWebMonitors(type, userInfo.getId());
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("result", result);
        mv.setViewName("monitor/" + type + "/" + html);
        return mv;
    }

    /**
     * Web性能监控的画图
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/graphic")
    @ResponseBody
    public String monitorHttpStatus(HttpServletRequest request, HttpServletResponse response) {
        String destination = request.getParameter("destination");
        String type = request.getParameter("type");
        String name = request.getParameter("name");
        String choosetime = request.getParameter("choosetime");

        ServiceResult result = null;
        if (destination == null || type == null || name == null || choosetime == null) {
            result = ServiceResult.failResult("parameter error");
        } else {
            if (type.equals("http") || type.equals("ping")) {
                result =
                        monitorService.showUtilityView(destination, name, type,
                                Integer.parseInt(choosetime));
            } else {
                String port = request.getParameter("port").replace(",", "");
                result =
                        monitorService.showUtilityView(destination, name, type,
                                Integer.parseInt(choosetime), port);
            }

        }
        System.out.println(result);
        return JsonUtil.toJsonText(result);
    }

    /**
     * 显示所有监控项
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/showall")
    public ModelAndView monitorAll(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("currentUser");
        String html = "monitor/showall_content";
        ServiceResult result = null;
        if (userInfo == null) {
            result = ServiceResult.failResult("userInfo in session is null.");
        } else {
            result = monitorService.listWebMonitors(userInfo.getId());
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("result", result);
        mv.setViewName(html);
        return mv;
    }

    /**
     * 创建监控对象
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/webcreate")
    @ResponseBody
    public String createWebMonitor(HttpServletRequest request, HttpServletResponse response) {

        String frequency = request.getParameter("frequency");
        String monitorname = request.getParameter("name");
        String port = request.getParameter("port");
        String destination = request.getParameter("destination");
        String type = request.getParameter("type");
        HttpSession session = request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("currentUser");

        MonitorStatus monitor = new MonitorStatus();
        monitor.setDestination(destination);
        monitor.setFrequency(Integer.parseInt(frequency));
        monitor.setHostname("125.216.243.185");
        monitor.setMonitorname(monitorname);
        monitor.setType(type);
        if (port != null) {
            monitor.setPort(Integer.parseInt(port));
        }
        monitor.setUserId(userInfo.getId());
        monitor.setStatus(0);

        AmqConfig config =
                AmqConfig.getDefaultConfig(SPMConstants.WEB_MONITOR_QUEUE, WebTaskMessage.class,
                        null);
        AmqSender sender = AmqSender.initAmqSender(config);
        WebTaskMessage webTask = new WebTaskMessage();
        webTask.setDestination(destination);
        webTask.setFrequency(Integer.parseInt(frequency));
        webTask.setMonitorName(monitorname);
        webTask.setPort(Integer.parseInt(port));
        webTask.setType(MonitorType.valueOf(type.toUpperCase()));
        sender.send(webTask);

        ServiceResult result = monitorService.createMonitor(monitor);
        return JsonUtil.toJsonText(result);
    }

}
