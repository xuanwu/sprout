// package com.sprout.webapp.controller;
//
//
// import java.io.File;
// import java.util.List;
//
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.multipart.MultipartFile;
// import org.springframework.web.servlet.ModelAndView;
//
// import com.sprout.webapp.model.Applications;
// import com.sprout.webapp.model.UserInfo;
// import com.sprout.webapp.service.ApplicationsService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
//
// @Controller
// public class ApplicationsController {
//
// @Autowired
// private ApplicationsService applicationsService;
//
// @RequestMapping(value = "/application/add", method = RequestMethod.POST)
// @ResponseBody
// public String addApplications(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// Applications applications = new Applications();
//
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
// applications.setUserId(user.getId());
//
// String appName = request.getParameter("appName");
// String appType = request.getParameter("appType");
//
// applications.setAppName(appName);
// applications.setAppType(Integer.parseInt(appType));
// System.out.println(applications.getAppType());
//
// ServiceResult result = applicationsService.addApplications(applications);
// return JsonUtil.toJsonText(result);
// }
//
// // ------------------------------------------------------
// @RequestMapping(value = "/application/delete", method = RequestMethod.POST)
// @ResponseBody
// public String deleteApplications(Applications applications, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// System.out.println("Deletion");
// ServiceResult result = applicationsService.deleteApplications(applications);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/application/modify", method = RequestMethod.POST)
// @ResponseBody
// public String modifyApplications(Applications applications, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// ServiceResult result = applicationsService.updateApplications(applications);
// return JsonUtil.toJsonText(result);
// }
//
// // 这是查询个人信息的
// @RequestMapping(value = "/application/query", method = RequestMethod.POST)
// @ResponseBody
// public String queryApplications(Applications applications, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// String appId = request.getParameter("id");
// ServiceResult result = applicationsService.getApplication(Integer.parseInt(appId));
// return JsonUtil.toJsonText(result);
// }
//
// // ------------------------------------------------------
//
// // 这是查看所有用户的
// @RequestMapping(value = "/application/list", method = RequestMethod.POST)
// public ModelAndView getApplications(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// ModelAndView mv = new ModelAndView("list");
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// ServiceResult result = applicationsService.getUserAppList(user.getId());
// mv.addObject("result", result);
// mv.setViewName("index-detail");
// System.out.println("object->" + ((List) result.getObject()).size());
// return mv;
// }
//
// // 这是查看所有用户的
// @RequestMapping(value = "/application/weblog", method = RequestMethod.POST)
// public ModelAndView getWeblogApplication(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// ModelAndView mv = new ModelAndView("list");
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// ServiceResult result = ServiceResult.failResult("user is null");
// if (user != null) {
// result = applicationsService.getCategoryList(user.getId());
// }
// mv.addObject("result", result);
// mv.setViewName("weblogs/weblog_detail");
// return mv;
// }
//
// @RequestMapping(value = "/application/view", method = RequestMethod.POST)
// public ModelAndView viewApplication(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// ModelAndView mv = new ModelAndView();
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// String appId = request.getParameter("id");
//
// ServiceResult result = applicationsService.getApplication(Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("home/applicationInfo");
// return mv;
// }
//
// @RequestMapping(value = "/application/uploadjar", method = RequestMethod.POST)
// @ResponseBody
// public String uploadjar(HttpServletRequest request, HttpServletResponse response,
// @RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
//
// String appIdStr = request.getParameter("appid");
// String fileType = request.getParameter("fileType");
// if (appIdStr == null) {
// return JsonUtil.toJsonText("fail");
// }
// /*
// * MultipartHttpServletRequest multipart = (MultipartHttpServletRequest) request;
// * CommonsMultipartFile file = (CommonsMultipartFile) multipart.getFile("uploadfile");//
// * 表单中对应的文件名；
// */
//
// // 获取一个文件的输入流
// String savePath = null;
// HttpSession session = request.getSession();
// Integer appId = Integer.parseInt(appIdStr);
// ServiceResult result = null;
// if (file != null) {
// savePath = "upload\\" + appId;// user.getUserCode();
// String realPath = session.getServletContext().getRealPath(savePath);
// System.out.println(realPath);
// // 新建用户文件夹
// File path = new File(realPath);
// if (path.exists()) {
// System.out.println("文件夹已经被创建");
// } else {
// path.mkdirs();
// System.out.println("文件夹被创建");
// }
// String fileName = realPath + "\\" + file.getOriginalFilename();
// String serverpath = savePath + "\\" + file.getOriginalFilename();
// System.out.println("filename:" + fileName);
// try {
// File localFile = new File(fileName);
// file.transferTo(localFile);
// // FileUploadService.getInstance().fileUpload(path, fileName);
// result =
// applicationsService.updateApplication(appId, serverpath,
// Integer.parseInt(fileType));
// result.setObject(serverpath);
// } catch (Exception e) {
// System.out.println("文件传入失败");
// e.printStackTrace();
// }
// }
// return JsonUtil.toJsonText(result);
// }
// // //http://localhost:8080/sprouts/application/upload.do?task=2
// // @RequestMapping(value="/application/upload")
// // @ResponseBody
// // public String addApplications2( HttpServletRequest request,
// // HttpServletResponse response) throws Exception {
// // String taskId = request.getParameter("taskId");
// // String filePath = request.getParameter("filename");
// //
// // HttpSession session = request.getSession();
// //
// // String savePath="/home/zhou/upload/"+filePath;//user.getUserCode();
// // HDFSUtils.uploadFile(savePath);
// //
// //
// // SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// // Date date = new Date();
// // String dateCreated = formatter.format(date);
// //
// // Map<String, Object> mapCache = TaskConfiguration.initConfig("//upload//student.xml");
// // return JsonUtil.toJsonText(ServiceResult.successResult(dateCreated));
// // }
//
// }
