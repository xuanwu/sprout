// package com.sprout.webapp.controller;
//
// import java.io.BufferedReader;
// import java.io.File;
// import java.io.FileReader;
// import java.io.IOException;
// import java.text.SimpleDateFormat;
// import java.util.ArrayList;
// import java.util.Date;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
//
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
//
// import org.apache.hadoop.conf.Configuration;
// import org.apache.hadoop.hbase.client.Result;
// import org.apache.hadoop.hbase.util.Bytes;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.servlet.ModelAndView;
//
// import com.sprout.webapp.controller.loganalyzer.DayStatistics;
// import com.sprout.webapp.controller.loganalyzer.Loggers;
// import com.sprout.webapp.model.Category;
// import com.sprout.webapp.model.WebLogs;
// import com.sprout.webapp.pojo.SiteInfo;
// import com.sprout.webapp.server.hdfs.HBaseConnector;
// import com.sprout.webapp.server.hdfs.HBaseEngine;
// import com.sprout.webapp.server.hdfs.HbaseConfigure;
// import com.sprout.webapp.service.CategoryService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
//
// import org.springframework.beans.factory.annotation.Autowired;
//
// @Controller
// public class WebLogsController {
//
// @Autowired
// private CategoryService categoryService;
//
// private HBaseConnector hbaseConnector;
//
// public void buildConnection(String tablename)
// {
// this.hbaseConnector = new HBaseConnector();
// this.hbaseConnector.open(HbaseConfigure.ZOOKEEPER_QUORUM);
// try {
//
// this.hbaseConnector.setTable(tablename);
// } catch (IOException e) {
// System.out.println("The connection to HBase failed!");
// }
// }
//
// // @RequestMapping(value="/weblogs/browser", method=RequestMethod.POST)
// // @ResponseBody
// // public String startService()
// // {
// // String state = null;
// // LaunchAnalyzer
// // }
//
// @RequestMapping(value="/weblogs/browser", method=RequestMethod.POST)
// @ResponseBody
// public String getBrowser( HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// // System.out.println(request.getParameter("cid"));
// // int categoryId = Integer.parseInt(request.getParameter("cid"));
// //
// // buildConnection("NGINX"+categoryId);
// //
// // ServiceResult result0= categoryService.getCategoryByAppId(categoryId);
// // Category category = (Category)result0.getObject();
// //
// // ArrayList name=new ArrayList();
// // ArrayList data=new ArrayList();
// // Integer max=0;
// // WebLogs weblogs=new WebLogs(max,name,data);
// // Result hbaseData = this.hbaseConnector.get("125.216.243.183-20140807", null, null);
// // byte[] browser = hbaseData.getValue(Bytes.toBytes("20"), Bytes.toBytes("browser"));
// // String []lines = Bytes.toString(browser).split("\n");
// // for(int i = 0;i < lines.length;i++)
// // {
// // String []tempData = lines[i].split("\\W+");
// // weblogs.setName(tempData[0]);
// // weblogs.setData(Integer.parseInt(tempData[1]));
// // }
// // ServiceResult result =ServiceResult.successResult(weblogs);
// // return JsonUtil.toJsonText(result);
// String s=null;
// ArrayList name=new ArrayList();
// ArrayList data=new ArrayList();
// ArrayList ip = new ArrayList();
// Integer max=0;
// WebLogs weblogs=new WebLogs(max,name,data,ip);
// try
// {
// String rootPath = request.getRealPath("/");
// // System.out.println(rootPath);
// File f=new File(rootPath+"logs/brower_result.txt");
// BufferedReader reader=new BufferedReader(new FileReader(f));
// while((s=reader.readLine())!=null){
// String o[] = s.split("\\W+");
// String o1[] = s.split("\\s+");
// int datai=Integer.parseInt(o1[o1.length-1]);
// if(o[0].equalsIgnoreCase("")){
// //weblogs.setName(o[1]);
// weblogs.setNameAndData(o[1], datai);
// }else{
// // weblogs.setName(o[0]);
// weblogs.setNameAndData(o[0], datai);
// }
// // weblogs.setData(datai);
// if(datai>max){
// max=datai;
// }
// }
//
// weblogs.setMax(max);
// }catch(Exception e){
// e.printStackTrace();
// }
// ServiceResult result =ServiceResult.successResult(weblogs);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value="/weblogs/pv", method=RequestMethod.POST)
// @ResponseBody
// public String getPV( HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// String s=null;
// ArrayList name=new ArrayList();
// ArrayList data=new ArrayList();
// ArrayList ip = new ArrayList();
// Integer max=0;
// WebLogs weblogs=new WebLogs(max,name,data,ip);
// try
// {
// String rootPath = request.getRealPath("/");
// // System.out.println(rootPath);
// File f=new File(rootPath+"logs/pv_result.txt");
// BufferedReader reader=new BufferedReader(new FileReader(f));
// while((s=reader.readLine())!=null&&!"".equalsIgnoreCase(s)){
// String o[] = s.split("\\W+");
// String o1[] = s.split("\\s+");
// Integer datai=Integer.parseInt(o1[o1.length-1]);
// if(o[0].equalsIgnoreCase("")){
// weblogs.setName(o[1]);
// }else{
// weblogs.setName(o[0]);
// }
// weblogs.setData(datai);
// if(datai>max){
// max=datai;
// }
// }
//
// weblogs.setMax(max);
// }catch(Exception e){
// e.printStackTrace();
// }
// ServiceResult result =ServiceResult.successResult(weblogs);
// return JsonUtil.toJsonText(result);
// }
//
//
// @RequestMapping(value="/weblogs/pvall", method=RequestMethod.POST)
// @ResponseBody
// public String getPVAll( HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String s=null;
// ArrayList name=new ArrayList();
// ArrayList data=new ArrayList();
// ArrayList ip = new ArrayList();
// Integer max=0;
// WebLogs weblogs=new WebLogs(max,name,data,ip);
// try
// {
// String rootPath = request.getRealPath("/");
// // System.out.println(rootPath);
// File f=new File(rootPath+"logs/pv_all_result.txt");
// BufferedReader reader=new BufferedReader(new FileReader(f));
// while((s=reader.readLine())!=null){
// // String o[] = s.split("\\W+");
// String o1[] = s.split("\\s+");
// Integer datai=Integer.parseInt(o1[o1.length-1]);
// // if(o[0].equalsIgnoreCase("")){
// // weblogs.setName(o[1]);
// // }else{
// // weblogs.setName(o[0]);
// // }
// weblogs.setName(o1[0]);
// weblogs.setData(datai);
// if(datai>max){
// max=datai;
// }
// }
// weblogs.setMax(max);
// }catch(Exception e){
// System.out.println(e.toString());
// }
// ServiceResult result =ServiceResult.successResult(weblogs);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value="/weblogs/time", method=RequestMethod.POST)
// @ResponseBody
// public String getTime( HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String s=null;
// ArrayList name=new ArrayList();
// ArrayList data=new ArrayList();
// ArrayList ip = new ArrayList();
// Integer max=0;
// WebLogs weblogs=new WebLogs(max,name,data,ip);
// try
// {
// String rootPath = request.getRealPath("/");
// // System.out.println(rootPath);
// File f=new File(rootPath+"logs/pv_mothod_result.txt");
// BufferedReader reader=new BufferedReader(new FileReader(f));
// while((s=reader.readLine())!=null){
// // String o[] = s.split("\\W+");
// String o1[] = s.split("\\s+");
// Integer datai=Integer.parseInt(o1[o1.length-1]);
// // if(o[0].equalsIgnoreCase("")){
// // weblogs.setName(o[1]);
// // }else{
// // weblogs.setName(o[0]);
// // }
// weblogs.setName(o1[0]);
// weblogs.setData(datai);
// //weblogs.setNameAndData(getMonthDay(o1[0]), datai);
// weblogs.setIp(Integer.parseInt(o1[1]));
// if(datai>max){
// max=datai;
// }
// }
// weblogs.setMax(max);
// }catch(Exception e){
// System.out.println(e.toString());
// }
// ServiceResult result =ServiceResult.successResult(weblogs);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value="/weblogs/getstatus", method=RequestMethod.POST)
// @ResponseBody
// public String getStatus(HttpServletRequest request,
// HttpServletResponse response){
// int category_id = Integer.parseInt(request.getParameter("cid"));
// // ServiceResult result = categoryService.getWebStatusById(category_id);
// ServiceResult result = categoryService.getWebStatusById(45);
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/weblogs/requestlist")
// public ModelAndView requestList(HttpServletRequest request,
// HttpServletResponse response) throws Exception {
// String host = request.getParameter("host");
// ModelAndView mv = new ModelAndView();
//
// String filePath =
// "D:\\Program Files\\apache-tomcat-6.0.37\\apache-tomcat-6.0.37\\access_logs\\localhost_access_log.2015-03-25.txt";
// DayStatistics day = new DayStatistics();
// List<Loggers> list = new ArrayList<Loggers>();
//
// day.readFile(filePath, list);
// String regexcss = ".+(\\.css)(\\?.*)?";
// String regexjs = ".+(\\.js)(\\?.*)?";
// // String regeximage = ".+(\\.jpg|\\.png|\\.gif)(\\?.*)?";
// String dymregex = ".+(\\.do|\\.fm)(\\?.*)?";
// ServiceResult dymresult = ServiceResult.successResult(day.filterRegex(list, dymregex));
// mv.addObject("dymresult", dymresult);
// ServiceResult cssresult = ServiceResult.successResult(day.filterRegex(list, regexcss));
// mv.addObject("cssresult", cssresult);
// ServiceResult jsresult = ServiceResult.successResult(day.filterRegex(list, regexjs));
// mv.addObject("jsresult", jsresult);
// mv.setViewName("weblogs/statistics/statistics_detail");
// return mv;
// }
//
// public String getMonthDay(String date){
// String monthdayhour = date.substring(4);
// return Integer.parseInt(monthdayhour.substring(2, 4)) + "-" +
// Integer.parseInt(monthdayhour.substring(4));
// }
//
// public String buildRowkey(String host)
// {
// SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
// return host + "-" + format.format(new Date());
//
// }
//
// }
//
//
//
