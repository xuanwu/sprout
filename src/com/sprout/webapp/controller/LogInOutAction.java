﻿package com.sprout.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.UserInfoService;
import com.sprout.webapp.utils.ServiceResult;

@Controller
public class LogInOutAction {

    // 后台管理员登陆
    private UserInfo user;

    @Autowired
    private UserInfoService userService;

    @RequestMapping(value = "/logon", method = RequestMethod.POST)
    public ModelAndView logon(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();

        String usercode = request.getParameter("usercode");
        String password = request.getParameter("password");
        UserInfo user = new UserInfo();
        user.setUsercode(usercode);
        user.setUserpass(password);

        if (usercode != null && password != null) {
            System.out.println(usercode + " login : " + password);
            user = userService.isLogin(user);
            if (user != null) {
                session.setAttribute("currentUser", user);

                mv.setView(new RedirectView("pages/index.jsp"));
                mv.addObject("currentUser", user);
                return mv;
            }
        }
        mv.setViewName("redirect:pages/login-new.jsp");
        return mv;
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();

        UserInfo user = (UserInfo) session.getAttribute("currentUser");
        if (user != null) {
            session.removeAttribute("currentUser");
        }
        mv.setViewName("redirect:pages/login-new.jsp");
        return mv;
    }

    @RequestMapping(value = "/userlist")
    public ModelAndView userlist(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();

        UserInfo user = (UserInfo) session.getAttribute("currentUser");
        if (user == null) {
            mv.setViewName("redirect:pages/login-new.jsp");
        } else {
            List<UserInfo> list = userService.getUserList();
            mv.addObject("result", ServiceResult.successResult(list));
            mv.setViewName("users/userlist_content");
        }
        return mv;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public UserInfoService getUserService() {
        return userService;
    }

    public void setUserService(UserInfoService userService) {
        this.userService = userService;
    }

}
