// package com.sprout.webapp.controller;
//
// import java.util.ArrayList;
// import java.util.List;
//
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.servlet.ModelAndView;
//
// import com.sprout.webapp.model.Applications;
// import com.sprout.webapp.model.Category;
// import com.sprout.webapp.model.CategoryEvent;
// import com.sprout.webapp.model.CategoryState;
// import com.sprout.webapp.model.UserInfo;
// import com.sprout.webapp.pojo.KPI;
// import com.sprout.webapp.pojo.LogTask;
// import com.sprout.webapp.service.ApplicationsService;
// import com.sprout.webapp.service.CategoryService;
// import com.sprout.webapp.utils.JsonUtil;
// import com.sprout.webapp.utils.ServiceResult;
//
// @Controller
// public class CategoryController {
//
// @Autowired
// private CategoryService categoryService;
// @Autowired
// private ApplicationsService applicationsService;
//
// @RequestMapping(value = "/category/add", method = RequestMethod.POST)
// public ModelAndView addCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// // HttpSession session = request.getSession();
// // UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// Category category = new Category();
// String name = request.getParameter("categoryname");
// String type = request.getParameter("categorytype");
// // String appIdString = request.getParameter("appId");
//
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
// System.out.println("~~~~ i'm adding category:" + name + "," + type + "," + user.getId());
//
// category.setName(name);
// category.setType(Integer.parseInt(type));
// category.setUserId(user.getId());
// // category.setAppId(Integer.parseInt(appIdString));
//
// ServiceResult result = categoryService.addCategory(category);
//
// String resultString = JsonUtil.toJsonText(result);
// String[] resultStringArray = resultString.split(":");
// ModelAndView mv = new ModelAndView();
// // 如果result解析出来的json信息里面的success为true则返回添加成功
// if ("true".equals(resultStringArray[4].substring(0, resultStringArray[4].length() - 1))) {
// mv.setViewName("redirect:../pages/weblogs/logHome.jsp");// 注册成功返回日志首界面
// } else {
// mv.setViewName("redirect:../pages/weblogs/logHome.jsp");
// }
// return mv;
// }
//
// @RequestMapping(value = "/category/start-catogory", method = RequestMethod.POST)
// @ResponseBody
// public String startService(HttpServletRequest request, HttpServletResponse response) {
// int cid = Integer.parseInt(request.getParameter("cid"));
// System.out.println(cid);
// ServiceResult categoryResult = categoryService.getcategoryById(cid);
// List<String> ip_group = new ArrayList<String>();
// List<Category> categoryList = (List<Category>) categoryResult.getObject();
// Category category = categoryList.get(0);// (Category)categoryResult.getObject();
// // LogMonitorController logMonitor = new LogMonitorController();
//
// // try {
// // logMonitor.serviceInit(new Configuration());
// // } catch (Exception e) {
// // // TODO Auto-generated catch block
// // System.out.println("Service init failed!");
// // }
//
// // logMonitor.start();
// LogTask task = setLogTask(category);
// System.out.println(category.getLogpath() + " " + category.getIp());
// // WorkerEvent workerEvent = new WorkerEvent(WorkerState.START,category.getIp());
// // logMonitor.getDispatcher().getEventHandler().handle(workerEvent);
// // workerEvent.setWorkerstate(WorkerState.RUNNING);
// // logMonitor.getDispatcher().getEventHandler().handle(workerEvent);
// // ip_group.add(category.getIp());
// // LaunchAnalyzer analyzer = new LaunchAnalyzer(cid,ip_group,LogType.NGINX);
// CategoryEvent categoryEvent = new CategoryEvent(CategoryState.SCHEDULE, task);
// // logMonitor.getDispatcher().getEventHandler().handle(categoryEvent);
//
// // return "true";
// return "open_success";
// }
//
// @RequestMapping(value = "/category/stop-catogory", method = RequestMethod.POST)
// @ResponseBody
// public String stopService(HttpServletRequest request, HttpServletResponse response) {
// return "close_success";
// }
//
// @RequestMapping(value = "/category/submit", method = RequestMethod.POST)
// @ResponseBody
// public String submitCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// // HttpSession session = request.getSession();
// // UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// // String cid = request.getParameter("cid");
// // ServiceResult result = categoryService.submitCategory(Integer.parseInt("45"));
// String cid = request.getParameter("cid");
//
// ServiceResult result = categoryService.submitCategory(Integer.parseInt(cid));
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/category/disable")
// @ResponseBody
// public String disableCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// String disable = request.getParameter("disable");
//
// List<LogTask> logTask = (List<LogTask>) JsonUtil.toOjbect(disable);
//
// String result = categoryService.disableCategory(logTask);
// return JsonUtil.toJsonText(result);
// }
//
// // ------------------------------------------------------
// @RequestMapping(value = "/category/delete", method = RequestMethod.POST)
// @ResponseBody
// public String deleteCategory(Category category, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// HttpSession session = request.getSession();
// String deleteCategory_id = (String) request.getParameter("deleteCategory_id");
//
// System.out.println("deleting category which one's id is " + deleteCategory_id);
//
// category.setId(Integer.parseInt(deleteCategory_id));
//
// ServiceResult result = categoryService.deleteCategory(category);
//
// String[] resultStringArray = JsonUtil.toJsonText(result).split(":");
//
// if ("true".equals(resultStringArray[4].substring(0, resultStringArray[4].length() - 1))) {
// return "true";
// } else {
// return "false";
// }
//
// }
//
// @RequestMapping(value = "/category/modify", method = RequestMethod.POST)
// @ResponseBody
// public String modifyCategory(Category category, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// ServiceResult result = categoryService.updateCategory(category);
// return JsonUtil.toJsonText(result);
// }
//
// // 查询
// @RequestMapping(value = "/category/query", method = RequestMethod.POST)
// @ResponseBody
// public String queryCategory(Category category, HttpServletRequest request,
// HttpServletResponse response) throws Exception {
//
// String appId = request.getParameter("appId");
// String result = null;
// if (appId != null) {
// result = categoryService.getAppCategory(Integer.parseInt(appId));
// } else {
// result = JsonUtil.toJsonText(ServiceResult.failResult("appID is null"));
// }
// return result;
// }
//
// // ------------------------------------------------------
// // 作废， 转到：application/weblog
// // 这是查看所有用户的 springmvc js
// @RequestMapping(value = "/category/list")
// public ModelAndView getCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// ModelAndView mv = new ModelAndView("list");
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
// ServiceResult result = ServiceResult.failResult("user is null");
// if (user != null) {
// result = categoryService.getApplicationList(user.getId());
// }
// mv.addObject("result", result);
// mv.setViewName("weblogs/weblog_detail");
// // mv.setViewName("index-detail");
// return mv;
// }
//
// @RequestMapping(value = "/category/view", method = RequestMethod.POST)
// public ModelAndView viewApplication(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// ModelAndView mv = new ModelAndView();
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// String appId = request.getParameter("id");
//
// ServiceResult result = categoryService.getCategory(Integer.parseInt(appId));
// mv.addObject("result", result);
// mv.setViewName("home/categoryInfo");
// return mv;
// }
//
//
// @RequestMapping(value = "/category/record_view")
// @ResponseBody
// public String viewLogRecord(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// String logId = request.getParameter("id");
// ServiceResult result = null;
// if (logId != null) {
// // result = categoryService.getLogRecordFirst(Integer.parseInt(appId));
// // result = categoryService.getLogRecord(Integer.parseInt(logId));
// }
// return JsonUtil.toJsonText(result);
// }
//
// @RequestMapping(value = "/category/category", method = RequestMethod.POST)
// public ModelAndView getAllCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// ModelAndView mv = new ModelAndView("list");
// // String appIdString = request.getParameter("appId");
// // int appId = Integer.parseInt(appIdString.substring(1,appIdString.length()));//去除？
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// ServiceResult result = categoryService.getUserCategoryList(user.getId());
// mv.addObject("result", result);
// mv.setViewName("/weblogs/category-detail");
// return mv;
// }
//
// @RequestMapping(value = "/category/categorylist", method = RequestMethod.POST)
// public ModelAndView getCategoryList(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// ModelAndView mv = new ModelAndView();
// // String appIdString = request.getParameter("appId");
// // int appId = Integer.parseInt(appIdString.substring(1,appIdString.length()));//去除？
// HttpSession session = request.getSession();
// UserInfo user = (UserInfo) session.getAttribute("currentUser");
//
// ServiceResult result = categoryService.getUserCategoryList(user.getId());
// mv.addObject("result", result);
// mv.setViewName("/weblogs/list_content");
// return mv;
// }
//
// @RequestMapping(value = "/category/fuzzyQuery", method = RequestMethod.POST)
// public ModelAndView fuzzyQuery(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// System.out.println("~~~~~~~this is fuzzyQuery.fm");
//
// ModelAndView mv = new ModelAndView("list");
//
// String fuzzyQueryKey = request.getParameter("fuzzyQueryKey");
// String appIdString = request.getParameter("appId");
//
// System.out.println("~~~~~~~this is fuzzyQuery.fm fuzzQueryKey:" + fuzzyQueryKey);
//
// ServiceResult result =
// categoryService.fuzzyQueryCategory(fuzzyQueryKey,
// appIdString.substring(1, appIdString.length()));
//
// mv.addObject("result", result);
// mv.setViewName("/weblogs/category-detail");
//
// System.out.println("category->" + ((List) result.getObject()).size());
//
// return mv;
// }
//
// @RequestMapping(value = "/category/search", method = RequestMethod.POST)
// public ModelAndView log_search(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// ModelAndView mv = new ModelAndView();
//
// String sampleLogsResult = request.getParameter("sampleLogsResult");
// String type_Str = request.getParameter("type");
//
// System.out.println("sampleLogs--------->:" + sampleLogsResult + " re:" + type_Str);
// String[] lines = sampleLogsResult.split("\n");
// System.out.println(lines.length);
// Integer type = Integer.parseInt(type_Str);
// KPI kpi = new KPI();
// String line3 =
// "125.216.243.88 - - [25/Mar/2015:11:23:34 +0800] \"GET /index HTTP/1.1\" 302 - 272 \"http://112.74.125.71:8080/login\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0\"";
//
// switch (type) {
// case 0:
// kpi = KPI.parser(lines[0]);
// break;
// case 1:
// kpi = KPI.parser(line3);
// break;
// default:
// break;
// }
// List<KPI> kpiResult = new ArrayList<KPI>();
// kpiResult.add(kpi);
//
// System.out.println("KPI test : " + kpi);
// mv.addObject("result", kpiResult);
// mv.setViewName("/weblogs/search/search-detail");
// return mv;
// }
//
//
// @RequestMapping(value = "/category/validateSample", method = RequestMethod.POST)
// public ModelAndView validateSample(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
//
// // 验证正则表达式，并且key value的列表
// // System.out.println("~~~~~~~this is validateSample.fm");
//
// ModelAndView mv = new ModelAndView();
//
// String sampleLogsResult = request.getParameter("sampleLogsResult");
// String type_Str = request.getParameter("type");
//
// System.out.println("sampleLogs--------->:" + sampleLogsResult + " re:" + type_Str);
// String[] lines = sampleLogsResult.split("\n");
// System.out.println(lines.length);
// Integer type = Integer.parseInt(type_Str);
// KPI kpi = new KPI();
//
// switch (type) {
// case 0:
// kpi = KPI.parser(lines[0]);
// break;
// case 1:
// kpi = KPI.parser(lines[0]);
// break;
// default:
// break;
// }
// List<KPI> kpiResult = new ArrayList<KPI>();
// kpiResult.add(kpi);
//
// System.out.println("KPI test : " + kpi);
// mv.addObject("result", kpiResult);
// mv.setViewName("/weblogs/kpiResult-detail");
// return mv;
// }
//
// @ResponseBody
// @RequestMapping(value = "/category/updateCategoryConf", method = RequestMethod.POST)
// public String updateCategory(HttpServletRequest request, HttpServletResponse response)
// throws Exception {
// // @ResponseBody(返回值得时候需要)
// // 验证正则表达式，并且key value的列表
//
// String updateId = request.getParameter("updateId");
// String IPreInput = request.getParameter("IPreInput");
// String logPath = request.getParameter("logPath");
// String sampleLogs = request.getParameter("sampleLogs");
// String host = request.getParameter("host");
// String logType = request.getParameter("logType");
//
// String tempString = IPreInput.replace("\n", ";");
//
// ServiceResult result = categoryService.getcategoryById(Integer.parseInt(updateId));
// List<Category> list = (List) result.getObject();
// System.out.println("find by sql result id " + list.get(0).getId());
//
// Category temp = list.get(0);
// temp.setId(Integer.parseInt(updateId));
// temp.setLogpath(logPath);
// temp.setSamplelogs(sampleLogs);
// temp.setMachine(tempString);
// temp.setIp(host);
// temp.setLogType(Integer.parseInt(logType));
// result = categoryService.updateCategory(temp);
//
// return JsonUtil.toJsonText(result);
// }
//
// @ResponseBody
// @RequestMapping(value = "/category/deleteApplication", method = RequestMethod.POST)
// public String deleteApplication(HttpServletRequest request, HttpServletResponse response) {
//
// String applicationIdString = request.getParameter("applicationId");
//
// System.out.println("deleting application.do is deleting application id:"
// + applicationIdString);
//
// Applications temp = new Applications();
//
//
// ServiceResult listServiceResult =
// categoryService.getCategoryByAppId(Integer.parseInt(applicationIdString));
//
// List<Category> list = (List) listServiceResult.getObject();
//
// if (list.size() != 0) {
// for (Category tempCategory : list) {
// categoryService.deleteCategory(tempCategory);
// }
// } else
// System.out.println("application " + applicationIdString + " does not have category");
//
//
// temp.setId(Integer.parseInt(applicationIdString));
//
// ServiceResult result = applicationsService.deleteApplications(temp);
//
//
// String[] resultStringArray = JsonUtil.toJsonText(result).split(":");
//
// if ("true".equals(resultStringArray[4].substring(0, resultStringArray[4].length() - 1))) {
// return "true";
// } else {
// return "false";
// }
//
// }
//
// private LogTask setLogTask(Category category) {
// LogTask task = new LogTask();
// task.setCategoryId(category.getId());
// task.setFrequent(category.getFrequent());
// task.setIp(category.getIp());
// task.setMachine(category.getMachine());
// task.setLogpath(category.getLogpath());
// task.setType(1);
//
// return task;
// }
//
// }
