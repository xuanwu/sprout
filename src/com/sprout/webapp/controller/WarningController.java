package com.sprout.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.MonitorStatus;
import com.sprout.webapp.model.MonitorWarning;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.MonitorStatusService;
import com.sprout.webapp.service.MonitorWarningService;
import com.sprout.webapp.service.UserInfoService;
import com.sprout.webapp.utils.JsonUtil;
import com.sprout.webapp.utils.ServiceResult;

@Controller
public class WarningController {

    @Autowired
    private MonitorWarningService warningService;

    @Autowired
    private MonitorStatusService monitorService;

    @Autowired
    private UserInfoService userService;

    /**
     * warning 查找
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/findwarning")
    public ModelAndView findMonitors(HttpServletRequest request, HttpServletResponse response) {
        // String hostname = request.getParameter("host");// 监控路径
        String type = request.getParameter("type");// 监控路径
        ServiceResult result = null;
        String regex = "http|tcp|udp|ping";
        if (!type.matches(regex)) {
            result = warningService.findWarnings(null);
        } else {
            result = warningService.findWarnings(type);
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("result", result);
        mv.setViewName("site/warning/warninglist_content");

        return mv;
    }

    @RequestMapping(value = "/monitor/findwarningbyId")
    public ModelAndView findWarningById(HttpServletRequest request, HttpServletResponse response) {
        // String hostname = request.getParameter("host");// 监控路径
        String id = request.getParameter("id");// 监控路径
        String respone_html = request.getParameter("respone_html");//
        if (id == null) {
            return null;
        }
        ServiceResult result = warningService.findWarningById(Integer.parseInt(id));
        ModelAndView mv = new ModelAndView();
        mv.addObject("result", result);
        mv.setViewName("monitor/warning/" + respone_html);
        return mv;
    }

    @RequestMapping(value = "/warning/create")
    @ResponseBody
    public String warningCreate(HttpServletRequest request, HttpServletResponse response) {
        // String hostname = request.getParameter("host");// 监控路径
        String retry_times = request.getParameter("monitor_num");// 监控次数
        String name = request.getParameter("name");// 监控路径
        String monitor_people = request.getParameter("monitor_people");// 通知人员
        // String frequency = request.getParameter("frequency");//
        String resp_time = request.getParameter("resp_time");// 阈值
        String type = request.getParameter("type");// 警告类型
        String monitorId = request.getParameter("monitor_id");// 警告ID
        UserInfo user = userService.getUserInfo(monitor_people);
        MonitorStatus status = monitorService.getMonitor(Integer.parseInt(monitorId));

        if (status == null) {
            return JsonUtil.toJsonText(ServiceResult.failResult());
        }
        MonitorWarning warning = new MonitorWarning();
        warning.setType(type);
        warning.setMonitorId(status.getId());
        warning.setEmail(user.getEmail());
        warning.setRetry_time(Integer.parseInt(retry_times));
        // warning.setFrequency(Integer.parseInt(frequency));// 监控次数5
        warning.setThreshold(Integer.parseInt(resp_time));// 阈值
        warning.setFrequency(5);// 监控周期
        if (type.equalsIgnoreCase("http")) {

            warning.setDescription("如果 http.status 连续" + retry_times + "次 >= 400 THEN 通知");

        } else if (type.equalsIgnoreCase("ping")) {

            warning.setDescription("如果  ping.loss 平均值 连续" + retry_times + "次 >= 50 THEN 通知");

        } else if (type.equalsIgnoreCase("udp")) {
            warning.setDescription("如果 udp.status 连续" + retry_times + "次 != 200 THEN 通知");
        } else {
            warning.setDescription("如果 tcp.status 连续" + retry_times + "次 != 200 THEN 通知");
        }
        warning.setIs_start(0);
        ServiceResult result = warningService.createMonitor(warning);
        return JsonUtil.toJsonText(result);
    }

    @RequestMapping(value = "/warning/update")
    @ResponseBody
    public String warningUpdate(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");// 监控路径
        String path = request.getParameter("path");// 监控路径
        String port = request.getParameter("port");// 监控路径
        String mothod = request.getParameter("mothod");// 监控类型
        String frequency = request.getParameter("frequency");// 监控频率
        String type = request.getParameter("type");//
        String id = request.getParameter("id");

        MonitorWarning warning = warningService.getMonitor(Integer.parseInt(id));

        if (warning == null) {
            return JsonUtil.toJsonText(ServiceResult.failResult("更新错误"));
        }
        ServiceResult result = null;
        if (type.equalsIgnoreCase("stop")) {
            warning.setIs_start(0);
            result = warningService.updateMonitor(warning);
        }

        return JsonUtil.toJsonText(result);
    }

    @RequestMapping(value = "/warning/delete")
    @ResponseBody
    public String warningDelete(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");// 监控路径
        String id = request.getParameter("id");// 监控路径
        ServiceResult result = warningService.deleteMonitor(Integer.parseInt(id));// tomcatService.createTomcat(tomcat);

        return JsonUtil.toJsonText(result);
    }

    @RequestMapping(value = "/warning/view")
    public ModelAndView warningView(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");// 监控路径
        String id = request.getParameter("id");

        MonitorWarning warning = warningService.getMonitor(Integer.parseInt(id));
        ModelAndView mv = new ModelAndView();
        mv.addObject("result", ServiceResult.successResult(warning));
        mv.setViewName("tomcat/warning/waring_Info");
        return mv;
    }

}
