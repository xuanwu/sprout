package com.sprout.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.ServiceInfo;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.ServiceInfoService;
import com.sprout.webapp.utils.JsonUtil;
import com.sprout.webapp.utils.ServiceResult;

@Controller
public class ServiceInfoController extends HttpServlet {

	@Autowired
	private ServiceInfoService serviceInfoService;
	
	@RequestMapping(value="/service/create", method=RequestMethod.POST)
	@ResponseBody
	public String createService(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		
		UserInfo user=getCurrentUser(request);
		Integer userId=user.getId();
		//String appId="1";
		String appId=request.getParameter("id");
		String servicename=request.getParameter("servicename");
		String servicetype=request.getParameter("servicetype");
		
		ServiceInfo serviceInfo=serviceInfoService.checkUserCreatedService(userId, 
				Integer.parseInt(appId), servicetype);
		ServiceResult result=null;
		if(serviceInfo!=null){
			result=ServiceResult.failResult("服务已存在");
		}
		else{
			ServiceInfo serviceinfo=new ServiceInfo();
			serviceinfo.setUserId(userId);
			serviceinfo.setAppId(Integer.parseInt(appId));
			serviceinfo.setServicename(servicename);
			serviceinfo.setServicetype(servicetype);
			result=serviceInfoService.createService(serviceinfo);
		}
		return JsonUtil.toJsonText(result);
	}
	
	
	@RequestMapping(value="/service/status/change", method=RequestMethod.POST)
	@ResponseBody
	public String changeServiceStatus(HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		UserInfo user=getCurrentUser(request);
		Integer userId=user.getId();
		//String appId="1";
		String appId=request.getParameter("id");
		String serviceType=request.getParameter("serviceType");
		String status=request.getParameter("status");
		
		ServiceInfo serviceInfo=serviceInfoService.checkUserCreatedService(userId,
				Integer.parseInt(appId), serviceType);
		
		ServiceResult result=null;
		if(serviceInfo!=null){
			result=serviceInfoService.changeServiceStatus(serviceInfo,Integer.parseInt(status));
		}
		else{
			result=ServiceResult.failResult("服务不存在");
		}
		
		return JsonUtil.toJsonText(result);
	}
	
	//这是查看所有服务的
		@RequestMapping(value="/service/list", method=RequestMethod.POST)
		public ModelAndView getServices(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
			ModelAndView mv = new ModelAndView("list");
			
			String appId=request.getParameter("id");
			//String appId="1";
			ServiceResult result = serviceInfoService.getAppServiceList(Integer.parseInt(appId));
			mv.addObject("result", result);
			mv.setViewName("service/service-detail");
			return mv;
		}
	
	
	public UserInfo getCurrentUser(HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserInfo user = (UserInfo) session.getAttribute("currentUser");
		return user;
	}
}
