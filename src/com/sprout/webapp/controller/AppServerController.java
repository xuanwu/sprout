package com.sprout.webapp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.Tomcats;
import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.AppServerService;
import com.sprout.webapp.service.MonitorStatusService;
import com.sprout.webapp.service.MonitorWarningService;
import com.sprout.webapp.utils.JsonUtil;
import com.sprout.webapp.utils.ServiceResult;
import com.sprouts.spm_framework.utils.HTTPUtils;
import com.sprouts.spm_framework.utils.Logger;


@Controller
public class AppServerController {
    @Autowired
    private MonitorStatusService monitorService;

    @Autowired
    private AppServerService appserverService;

    @Autowired
    private MonitorWarningService warningService;

    private static Logger logger = new Logger();

    /**
     * 应用服务器监控列表
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/apps_list")
    public ModelAndView listMonitors(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        UserInfo user = (UserInfo) session.getAttribute("currentUser");

        System.out.println("the user send from jsp is :" + user.getUsername());
        ServiceResult result = appserverService.lists(user.getUsername());

        ModelAndView mv = new ModelAndView();
        mv.addObject("result", result);
        mv.setViewName("monitor/appserver/appserver_content");
        return mv;
    }

    /**
     * 应用服务器监控画图
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/apps_graphic")
    @ResponseBody
    public String graphicStatus(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");// 监控路径
        String port = request.getParameter("port");// 监控路径
        String choosetime = request.getParameter("choosetime");//

        ServiceResult result = null;
        result =
                appserverService.getAppServerStatus(hostname,
                        Integer.parseInt(port.replace(",", "")), Integer.parseInt(choosetime));

        return JsonUtil.toJsonText(result);
    }

    /**
     * 创建实例
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/apps_create")
    @ResponseBody
    public String createAppserver(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");// 监控路径
        String type = request.getParameter("type");// 监控类型
        String monitorname = request.getParameter("name");// 监控名称
        String frequency = request.getParameter("frequency");// 监控频率

        Tomcats tomcat = new Tomcats();
        tomcat.setDestination(hostname);
        tomcat.setMonitorname(monitorname);

        tomcat.setStatus(0);
        tomcat.setCenter_machine("125.216.243.185");
        tomcat.setType(type);
        tomcat.setUser("zhou");
        tomcat.setFrequency(Integer.parseInt(frequency));

        if (type.equals("tomcat")) {
            tomcat.setPort(8999);
            tomcat.setRealport(8080);
        } else if (type.equals("jetty")) {
            tomcat.setPort(8998);
            tomcat.setRealport(8081);
        }

        ServiceResult result = appserverService.create(tomcat);
        return JsonUtil.toJsonText(result);
    }

    /**
     * 应用服务器中的应用列表，url如 "http://125.216.243.193:8080/manager/text/list"
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/monitor/apps_managelist")
    @ResponseBody
    public String manageList(HttpServletRequest request, HttpServletResponse response) {

        String hostname = request.getParameter("host");
        String port = request.getParameter("port");

        StringBuilder builder = new StringBuilder();
        builder.append("http://");
        builder.append(hostname);
        builder.append(":");
        builder.append(port.replace(",", ""));
        builder.append("/manager/text/list");

        String result = null;
        try {
            result = HTTPUtils.doGet(builder.toString(), "appserver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        List list = null;
        if (result != null && result.startsWith("OK")) {
            list = handleResult(result);
        } else {
            return JsonUtil.toJsonText(ServiceResult.failResult("应用服务器没有启动！"));
        }

        return JsonUtil.toJsonText(ServiceResult.successResult(list));
    }

    @RequestMapping(value = "/monitor/apps_manage")
    @ResponseBody
    public String manageApps(HttpServletRequest request, HttpServletResponse response) {
        String hostname = request.getParameter("host");
        String port = request.getParameter("port");
        String type = request.getParameter("type");
        String path = request.getParameter("path");

        String regex = "start|stop|reload|unploy";
        if (!type.matches(regex)) {
            System.out.println("type error--->" + type);
            return JsonUtil.toJsonText(ServiceResult.failResult());
        }

        StringBuilder builder = new StringBuilder();
        builder.append("http://");
        builder.append(hostname);
        builder.append(":");
        builder.append(port.replace(",", ""));
        builder.append("/manager/text/" + type);
        builder.append("?path=/" + path);
        String result = null;
        try {
            result = HTTPUtils.doGet(builder.toString(), "appserver");
        } catch (Exception e) {
            logger.error("", e);
        }
        return result;
    }

    public List handleResult(String data) {

        String[] resultSet = StringUtils.split(data, "/");

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        for (int i = 2; i < resultSet.length; i++) {
            String[] splits = resultSet[i].split(":");
            Map<String, String> obejct = new HashMap<String, String>();
            obejct.put("status", splits[1]);
            obejct.put("session", splits[2]);
            obejct.put("app", splits[3]);
            list.add(obejct);
        }
        return list;
    }

}
