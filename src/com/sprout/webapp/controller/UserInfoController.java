package com.sprout.webapp.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sprout.webapp.model.UserInfo;
import com.sprout.webapp.service.UserInfoService;
import com.sprout.webapp.utils.JsonUtil;
import com.sprout.webapp.utils.MD5code;
import com.sprout.webapp.utils.ServiceResult;
@Controller
public class UserInfoController {

	@Autowired
	private UserInfoService userInfoService;
	
	@RequestMapping(value="/user/add", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView addUserInfo(UserInfo userInfo, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
//		System.out.println("I am here \n");
		List<String> ranRoute=new ArrayList<String>();
		
		MD5code md5 = new MD5code();
		String s = "";
		userInfo.setUsercode(request.getParameter("userName"));
		userInfo.setUsername(request.getParameter("userName"));
		userInfo.setEmail(request.getParameter("email2"));
		userInfo.setUserpass(md5.getMD5ofStr(request.getParameter("password")));
		userInfo.setPhone(request.getParameter("cellphone"));
		{
			java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	        s = format1.format(new Date());
		}
		userInfo.setCreatedDate(s);
		userInfo.setRole("1");
		//setRole
		//setStatus
		ServiceResult result = userInfoService.addUserInfo(userInfo);
		
		String resultString = JsonUtil.toJsonText(result);
		String[] resultStringArray = resultString.split(":");
		ModelAndView mv = new ModelAndView();
		//如果result解析出来的json信息里面的success为true则返回注册成功
		if("true".equals(resultStringArray[4].substring(0, resultStringArray[4].length() - 1))){
			doPost(userInfo.getUsername(),userInfo.getUserpass(),userInfo.getPhone(),userInfo.getEmail());
			mv.setViewName("redirect:../pages/login-new.jsp");//注册成功返回登录界面
		}
		else{
			mv.setViewName("redirect:../pages/register.jsp?registeError");//注册失败，返回注册界面并且在url里面加入错误信息
		}
		return mv;
	}
	//------------------------------------------------------
	@RequestMapping(value="/user/delete", method=RequestMethod.POST)
	@ResponseBody
	public String deleteUserInfo(UserInfo userInfo, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		System.out.println("Deletion");
		ServiceResult result = userInfoService.deleteUserInfo(userInfo);
		return JsonUtil.toJsonText(result);
	}
	
	@RequestMapping(value="/user/modify", method=RequestMethod.POST)
	@ResponseBody
	public String modifyUserInfo(UserInfo userInfo, HttpServletRequest request,
			HttpServletResponse response)throws Exception{
		
		ServiceResult result = userInfoService.updateUserInfo(userInfo);
		return JsonUtil.toJsonText(result);
	}
	
	//这是查询个人信息的
	@RequestMapping(value="/user/query", method=RequestMethod.POST)
	public UserInfo queryUserInfo(UserInfo userInfo, HttpServletRequest request,
			HttpServletResponse response)throws Exception{
        return userInfo;
	}
	//------------------------------------------------------
	
	//这是查看所有用户的
	@RequestMapping(value="/user/list", method=RequestMethod.POST)
	public ModelAndView getUserInfo(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mv = new ModelAndView("list");
		List<UserInfo> result = userInfoService.getUserList();
		mv.addObject("userList", result); 
		mv.setViewName("userInfo/manageUser");
		System.out.printf("endl");
		return mv;
	}
	
	
	@RequestMapping(value="/add")
	@ResponseBody
	public String addUser( HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		UserInfo userInfo = new UserInfo();
		
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date date = new Date();
		 String dateCreated = formatter.format(date);
		 MD5code md5 = new MD5code();
		
		userInfo.setCreatedDate(dateCreated);
		userInfo.setEmail("zhou@gmail.com");
		userInfo.setPhone("1300000000");
		userInfo.setRole("1");
		userInfo.setStatus(0);
		userInfo.setUsercode("zhou");
		userInfo.setUsername("zhou");
		userInfo.setUserpass(md5.getMD5ofStr("zhou"));
		
		ServiceResult result = userInfoService.addUserInfo(userInfo);
		return JsonUtil.toJsonText(result);
	}
	
	public void doPost(String username, String password, String phone, String email){
		try {
			String parameterData = "username="+username+"&email="+email+"&phone="+phone+"&password="+password;
			URL url = new URL("http://125.216.243.85:8080/openstack/registerUser");
			URLConnection connection  = url.openConnection();
			HttpURLConnection httpURLConnection = (HttpURLConnection)connection;
		        
		    httpURLConnection.setDoOutput(true);
		    httpURLConnection.setDoInput(true);
		    httpURLConnection.setUseCaches(false); 
		    httpURLConnection.setRequestMethod("POST");
		    httpURLConnection.setRequestProperty("Accept-Charset", "utf-8");
		    //httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		    httpURLConnection.setRequestProperty("Content-Length", String.valueOf(parameterData.length()));	
		    httpURLConnection.connect();
		    OutputStream outputStream = null;
	        OutputStreamWriter outputStreamWriter = null;
	        InputStream inputStream = null;
	        InputStreamReader inputStreamReader = null;
	        BufferedReader reader = null;
	        StringBuffer resultBuffer = new StringBuffer();
	        String tempLine = null;
	        
	        try {
	            outputStream = httpURLConnection.getOutputStream();
	            outputStreamWriter = new OutputStreamWriter(outputStream);
	            
	            outputStreamWriter.write(parameterData.toString());
	            outputStreamWriter.flush();
	            
//	            if (httpURLConnection.getResponseCode() >= 300) {
//	                throw new Exception("HTTP Request is not success, Response code is " + httpURLConnection.getResponseCode());
//	            }
	            
	            inputStream = httpURLConnection.getInputStream();
	            inputStreamReader = new InputStreamReader(inputStream);
	            reader = new BufferedReader(inputStreamReader);
	            
	            while ((tempLine = reader.readLine()) != null) {
	                resultBuffer.append(tempLine);
	            }
	            
	        } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
	            
	            if (outputStreamWriter != null) {
	                outputStreamWriter.close();
	            }
	            
	            if (outputStream != null) {
	                outputStream.close();
	            }
	            
	            if (reader != null) {
	                reader.close();
	            }
	            
	            if (inputStreamReader != null) {
	                inputStreamReader.close();
	            }
	            
	            if (inputStream != null) {
	                inputStream.close();
	            }
	            
	        }

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
