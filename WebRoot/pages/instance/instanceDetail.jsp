<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/appDetail.css">
<head>
<body>
<#if result.success>
<#assign task=result.object>
	<h4 class="detail_title">基本信息</h4>
	<div class="detail_box">
	<ul>
		<li>实例ID:<span>${task.id}</span></li>
		<li>实例名称：<span>${task.taskname?default('0') }</span></li>
		<li>所属应用ID：<span>${task.appId }</span></li>
	</ul>
	<ul>
		<li>用户ID：<span>${task.userId }</span></li>
		<li>用户名称：<span>${task.username?default('0') }</span></li>
		<li>应用类型：<span>${task.applicationType?default('0')}</span></li>
	</ul>
	<ul>
		<li>创建日期：<span>${task.createdDate }</span></li>
		<li>状态：<span>${task.status }</span></li>
		<li>最终状态：<span>${task.finalStatus?default('0')}</span></li>
	</ul>
	<ul>
		<li>已用时间：<span>${task.usedTime?default('0')}</span></li>
		<li>开始时间：<span>${task.startTime?default('0') }</span></li>
		<li>结束时间：<span>${task.finishTime?default('0') }</span></li>
	</ul>
	<ul>
		<li>assumeTime：<span>${task.assumeTime?default('0')}</span></li>	
	</ul>
	</div>
	
	<h4 class="detail_title">配置信息</h4>
	<div class="detail_box">
		<ul>
		<li style="width:100%">jar路径：<span>${task.jarPath?default('')}</span></li>
		</ul>
		<ul>
		<li style="width:100%">日志路径：<span>${task.logsPath?default('0') }</span></li>
		</ul>
		<ul>
		<li style="width:100%">环境变量：<span>${task.envVariables?default('0') }</span></li>
		</ul>
	</div>
	
	<h4 class="detail_title">CPU及内存</h4>
	<div class="detail_box">
	<ul>
		<li>CPU：<span>${task.cpu?default('0') }</span></li>
		<li>内存：<span>${task.memory?default('0')}M</span></li>
		<li>已用内存：<span>${task.usedMem?default('0')}M</span></li>
	</ul>
	<ul>
		<li>所需内存：<span>${task.neededMem?default('0')}M</span></li>
		
	</ul>
	</div>
	
	<h4 class="detail_title">任务信息</h4>
	<div class="detail_box">
	<ul>
		<li>任务ID：<span>${task.jobId?default('0')}</span></li>
		<li>任务名称：<span>${task.jobName?default('0') }</span></li>
		<li>任务文件：<span>${task.jobFile?default('0') }</span></li>
	</ul>
	<ul>
		<li>队列：<span>${task.queue?default('0') }</span></li>
		<li>map进度：<span>${task.mapProgress?default('0') }</span></li>
		<li>reduce进度：<span>${task.reduceProgress?default('0') }</span></li>
	</ul>
	<ul>
		<li>已用Slots数量：<span>${task.numUsedSlots?default('0') }</span></li>
	</ul>
	</div>
	
	<h4 class="detail_title">其他</h4>
	<div class="detail_box">
	<ul>
		<li><span></span></li>
		<li><span></span></li>
		<li><span></span></li>
	</ul>
	</div>
	</#if>	
</body>
</html>