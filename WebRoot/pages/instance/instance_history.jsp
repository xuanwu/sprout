﻿<!DOCTYPE html>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/style.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/servInfo.css">
	
	<script type="text/javascript" src="${requestatt.contextPath}/js/instance.js"></script>
    <script type="text/javascript" src="${requestatt.contextPath}/js/app.js"></script>
   
<style text="text/css">
.add_intance{
	width: 160px;
	height: 40px;
	margin:0 auto;
	padding:0;
}
.logmanager{
	width: 76px;
}
</style>
<script>
$(document).ready(function () { 
	//查看日志
	$('.result').click(function () {
		$("#taskresult").css("display", "block");
		$("#all").css("display", "block");
		
		var  taskId = $(this).attr("taskId");
		//$("#taskId").val(taskId);
		//var taskId = $("#taskId").val();
		console.log("history "+taskId);
		
		var url = "../task/readresult.do";	
		//组装请求参数
        $.post(url, "taskId="+taskId, function(result){
          		$("#task_result").html(result);
		});
		//eval("document.getElementById('taskresult').style.display='block'");
	    //eval("document.getElementById('all').style.display='block'");
	});
	
	$(".logmanager").click(function(){
		var  taskId = $(this).attr("taskId");
		var url = "../task/getlog.do";
		$.post(url, "taskId="+taskId, function(result){
          		$("#showLogbox").html(result);
		});
	});
});
</script>
</head>
<body>
        <table class="serv_table" id="instTable">
            <thead>
                <tr>
                    <th>实例</th>
                    <th>名称</th>
                    
                     <th>开始时间</th>
                     <th>结束时间</th><!--
                     <th>运行状态</th>
                     --><th>结束状态</th>
                      <th>map进度</th>
                      <th>reduce进度</th>
                     <th width="145px">操作</th>
                      <th >查询</th>
                </tr>
            </thead>
  <#if result.success>
	<#assign lists=result.object>     
		<#if lists?size &gt; 0>
            <tbody id="instTable_tbody">
            	<#list lists as task>  
                <tr id="${task.jobId?default('0')}" >
                 	<td>${task.jobId?default('0')}</td>
                    <td>${task.jobName?default('0')}</td><!--
                    <td>${task.numUsedSlots?default('0')}</td>
                    --><td>${task.startTime?default('0')}</td>
                    <td>${task.finishTime?default('0')}</td>
                    <!--
                    <#assign status=task.status>   
                    	<#if status==0 >
                    		<td>未初始化</td>
                    	<#elseif status = 1>
                    		<td>Pending</td>
                    	<#elseif status = 2>
                    		<td>Running</td>
                    	<#elseif status = 3>
                    		<td>finished</td>
                    	<#elseif status = 5>
                    		<td>已完成</td>
                    	</#if>
                     -->
                     <#assign finalStatus=task.finalStatus>   
                    	<#if finalStatus==0 >
                    		<td>正在运行</td>
                    	<#elseif finalStatus = 1>
                    		<td>成功运行</td>
                    	<#elseif finalStatus = 2>
                    		<td>运行失败</td>
                    	</#if>
                   	   <td>${task.mapProgress?default('0')*100}%</td>
                   	    <td>${task.reduceProgress?default('0')*100}%</td>
                   	    
                    <td class="caozuo_button">
                    	<#if status==2 >
                   			<button class="serv_table_unbound" id="running_task" taskId="${task.id}">已启动</button>
                   		<#else>
                   		 	<button id="serv_manage" class="serv_table_bound manage" taskId="${task.id}">管理</button>
                   		 	
                   		 	<button id="serv_remove" class="serv_table_unbound remove" style="margin-left:10px" taskId="${task.id}">删除</button>
                   		</#if>
                    </td>
                    <td>
                    	<button class="serv_table_bound logmanager" taskId="${task.id}" style="margin-left:5px" >查看日志</button>
                    	<button class="serv_table_unbound result" taskId="${task.id}">结果</button>
                    </td>
                </tr>
                 </#list>
                 
            </tbody>
           </#if>
      </#if>
        </table>

</body>
</html>