﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/manageService.css">
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
</head>
<script>
$(document).ready(function () { 
	//查看日志
	
});
</script>	
<body>
		<input type="hidden" name="taskId" id="taskId" value="20">
		<div class="popWindow_title">
			<div style="float:left">运行结果</div>
			<div style="float:right; cursor:pointer" onclick=closeLogWindow()>close</div>
		</div>
		<div class="popWindow_content" id="manage_service_content">
			<div class="description">运行结果： <a href="">mapreduce</a></div>
			<hr/>
			<div id="task_result"></div>
		</div>
</body>
</html>