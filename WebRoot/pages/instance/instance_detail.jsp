﻿<!DOCTYPE html>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<script>  
	$(document).ready(function () { //页面加载完成后事件
			console.log("taskId-->"+ $("#start_task").attr("taskId"));
			$("#start_task").click(function() {  
    	
		    	var url = "../task/submit.do";	
		         //组装请求参数
		    	var text=" <button class='serv_table_unbound' >启动中</button>";
		    	$(this).parents(".caozuo_button").html(text);
		    	
		    	var taskId = $(this).attr("taskId");
		    	$.post(url, "taskId="+taskId, function(result){
					var jsonResult = $.parseJSON(result);
		//			console.log(jsonResult);
				}); 
				
					
   		 });  
   		 //任务进度条的百分比
   		 var maptemp = "50%";
		$("#mapPercent").css("width",maptemp);
		//var reducetemp='20%';
		$("#reducePercent").css("width",maptemp);
	});
</script>
<body>
        <table class="serv_table" id="instTable">
            <thead>
                <tr>
                    <th>实例</th>
                    <th>名称</th>
                     <th>资源</th>
                     <th>开始时间</th>
                     <th>结束时间</th>
                     <th>运行状态</th>
                      <th>map进度</th>
                      <th>reduce进度</th>
                     <th width="145px">操作</th>
                </tr>
            </thead>
  <#if result.success>
	<#assign lists=result.object>     
		<#if lists?size &gt; 0>
            <tbody id="instTable_tbody">
            	<#list lists as task>  
                <tr >
                 	<td>${task.jobId?default('0')}</td>
                    <td>${task.jobName?default('0')}</td>
                    <td>${task.numUsedSlots?default('0')}</td>
                    <td>${task.startTime?default('0')}</td>
                    <td>${task.finishTime?default('0')}</td>
                    <#assign status=task.status>   
                    	<#if status==0 >
                    		<td>未初始化</td>
                    	<#elseif status = 1>
                    		<td>Pending</td>
                    	<#elseif status = 2>
                    		<td>Running</td>
                    	<#elseif status = 3>
                    		<td>finished</td>
                    	<#elseif status = 5>
                    		<td>已完成</td>
                    	</#if>
                    	<!-- 
                     <#assign finalStatus=task.finalStatus>   
                    	<#if finalStatus==0 >
                    		<td>正在运行</td>
                    	<#elseif finalStatus = 1>
                    		<td>成功运行</td>
                    	<#elseif finalStatus = 2>
                    		<td>运行失败</td>
                    	</#if>
                    	 -->
                    	<td> 
                   <input name="mapPercent" class="mapPercent" value="${task.mapProgress?default('0')*100}" type="hidden"/>
				  <div class="progress" style="width:120px">
					   <span id="mapPercent" class="progress_bar"></span>
			      </div>
                    	</td>
                   	    <td>
                   	     <input name="reducePercent" class="reducePercent" value=" ${task.reduceProgress?default('0')*100}" type="hidden"/>
				  		<div class="progress" style="width:120px">
					   		<span id="reducePercent" class="progress_bar"></span>
			      		</div>
                   	   </td>
                   	    
                    <td class="caozuo_button">
                    	<#if status==2 >
                   			<button class="serv_table_unbound" id="running_task" taskId="${task.id}">已启动</button>
                   		<#else>
                   		 	<button class="serv_table_unbound" id="start_task" taskId="${task.id}">启动</button>&nbsp;<button class="serv_table_bound">管理</button>
                   		</#if>
                       
                    </td>
                </tr>
                 </#list>
                 
            </tbody>
           </#if>
      </#if>
        </table>
</body>
</html>