﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>


<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	
	<script type="text/javascript" src="<%=path%>/js/highcharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	

</head>
	
<body>
			<table cellpadding="0" cellspacing="0" border="0" id="appContent">
						<tbody>
							<tr id="instances_th" class="managemen_mon_title">
								<th>服务器名称</th>
								<th>操作系统</th>
								<th>IP</th>
								<th>CPU使用率</th>
								<th>内存使用率</th>
								<th>监控</th>
							</tr>
							<tr class="managemen_mon_info">
								<td>${app.createdDate}</td>
								<td>4</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="managemen_mon_info_sp1">配置任务</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" onclick=showAppDetail()>详情管理</span>
								</td>
							</tr>
						</tbody>
			</table>
</body>
</html>