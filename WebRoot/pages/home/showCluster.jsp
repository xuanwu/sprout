﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>


<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	
	<script type="text/javascript" src="<%=path%>/js/highcharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	

</head>
	
<body>
		<div class="popWindow_title">
			<div style="float:left">查看统计图</div>
			<div style="float:right; cursor:pointer" onclick=closeCpuWindow()>close</div>
		</div>
		<div class="popWindow_content" >
			<div id="chartCatalogBox" class="chart_catalog">
				<ul>
					<li>一个月</li>
					<li id="chart_catalog_weekly" class="chart_catalog_selected">一周</li>
					<li class="radius_style_left">一日</li>
				</ul>
			</div>
			<div id="showChartBox" style="margin:10px;margin-top:80px;">
			</div>
			
		</div>
</body>
</html>