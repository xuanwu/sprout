﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/code.css">
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
	<script type="text/javascript" src="<%=path%>/js/jquery.uploadify.min.js"></script>  
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/uploadify.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/uploadfile.css">
	
	
<script type="text/javascript">
$(document).ready(function() {  
	  $("#file_upload1").uploadify({  
          'buttonText' : '上传文件',  
          'height' : '30',  
          'swf' : '<%=path%>/js/uploadify.swf',  
          'uploader' : '<%=path%>/application/uploadjar.do',  
          'width' : '80',  
          'auto':true,  
          'fileObjName'   : 'file',  
          'onUploadSuccess' : function(file, data, response) {  
              alert( file.name + ' 上传成功！ ');  
          }  
      });  
	});
</script>
</head>
<body>

				     <!--网页代码管理-->
				     <div class="code_border">
					     <div class="code_description">
						     代码包格式只能是war或zip类型，且大小不能超过50M。上传需要一段时间，上传完毕后有提示
						 </div>
						 <div class="managemen_cod_title">
						     <!--上传代码按钮-->

							
							<div  type="file" name="fileName" id="file_upload1" ></div> 
							
							<div class="code_name">
			                     <div class="code_line_name">网页代码管理</div>
			                </div>
							 
						 </div>
					 </div>
					 
					<!--git代码管理-->
					<div id="git">
					     <div class="code_border" style="margin-top:20px">
						     <div class="code_description">
							     <div>
							         目前支持Git@OSC,CSDN Code,GitHub 三种类型Git代码管理。如果您还没使用Git，请参见
								     <a href="" target="_blank" style="color: rgb(56, 94, 162);">帮助文档</a>
								 </div>
								  <div>
							         如果部署java源代码程序包，您还需要为您的程序设置maven以支持MoPaaS云编译，如何设置请参见
								     <a href="" target="_blank" style="color: rgb(56, 94, 162);">帮助文档</a>
								 </div>
							 </div>
							 
						     <div class="managemen_cod_title" style="margin-bottom:10px">
						         <div class="code_git_upload" style="margin-right:20px;" onclick="">
							         <div style="width:100px;height:35px;text-align:center;">
		     			                 <div>Git Pull</div>
		     		                 </div>
							     </div>
							 
							     <div class="code_git_upload" style="margin-right:20px;" onclick="">
							         <div style="width:100px;height:35px;text-align:center;">
		     			                 <div>Git设置向导</div>
		     		                 </div>
							     </div>
							 
							     <div class="code_name">
				                     <div class="code_left_frame">GIT</div>
			                         <div class="code_line_name">Git代码管理</div>
			                     </div>
						     </div>
							 
							 <div class="code_line_name2">
								 Git托管类型：您可以点击"git设置向导"设置托管类型
			                 </div>
							 <div class="code_line_name2" style="margin-top:10px">
								Git仓库地址：您可以点击"git设置向导"设置仓库地址
			                 </div>
							
							 <div class="code_manager">
							     <div class="code_git_upload" style="float:left;margin-left:20px;" onclick="">
							         <div style="width:100px;height:35px;text-align:center;">
		     			                 <div>获取公钥</div>
		     		                 </div>
							     </div>
							 
							     <div class="code_git_upload" style="float:left;margin-left:20px;" onclick="">
							         <div style="width:100px;height:35px;text-align:center;">
		     			                 <div>清空设置</div>
		     		                 </div>
							     </div>
							 </div>
						 </div>
					</div>
	        	</div>
			</div>
		</div>   
    </div>
	
	
</body>
</html>