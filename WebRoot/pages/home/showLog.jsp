﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
</head>
	
<body>
		<div class="popWindow_title">
			<div style="float:left">查看日志</div>
			<div style="float:right; cursor:pointer" onclick=closeLogWindow()>close</div>
		</div>
		<div class="popWindow_content" id="showLogbox">
		</div>
</body>
</html>