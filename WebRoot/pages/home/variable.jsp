﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/servInfo.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/variable.css">

</head>
<body>

		<table class="var_table">
            <thead>
                <tr>
                    <th>变量名</th>
                    <th>变量值</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="createVar_tbody">
                <tr >
					<td><input type="text" name="var_name" id="varName" placeholder="自定义变量名"/></td>
                    <td><input type="text" name="var_val" id="varVal" placeholder="自定义变量值"/></td>
                    <td>
                        <button class="button_save" onclick="varCreate()" id="createVar">保存</button>
                    </td>
                </tr>
			
            </tbody>
        </table>
</body>
</html>