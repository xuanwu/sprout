<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<script type="text/javascript" src="<%=path%>/js/instance.js"></script>
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
</head>
	
<body>
		<span class="text_color_1">
		<div  class="unified_bk">
		     <div class="unified_bk_2_del">
		     	<div style="float:left;">你确定要删除这个实例吗？</div>
		     	<div style="float:right; cursor:pointer" onclick="closeRemoveWindow()">close</div>
		     	</div>
			 <div class="unified_bk_3">
			 	<button id="delete_only" class="unified_bk_3_2">删除</button>
			    <button id="cancle_only" class="unified_bk_3_1">取消</button>
			 </div>
		</div>
	</span>
</body>
</html>