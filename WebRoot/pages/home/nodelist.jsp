﻿
				<table cellpadding="0" cellspacing="0" border="0" id="appContent">
						<tbody>
							<tr id="instances_th" class="managemen_mon_title">
								<th width="5%">选择</th>
								<th>服务器名称</th>
								<th>操作系统</th>
								<th>IP</th>
								<th>CPU使用率</th>
								<th>内存使用率</th>
								<th>监控</th>
							</tr>
						<#if result.success>
								<#assign nodes=result.object>
								<#if nodes?size &gt; 0>	
								<#list nodes as node>
							<tr class="managemen_mon_info server_manager">
								<td><input class="checkbox_radio" type="checkbox" name="ids" value="${node.id}" /></td>
								<td>${node.hostname?default('null')}</td>
								<td>${node.nodesystem?default('null')}</td>
								<td>${node.nodeIP?default('null')}</td>
								<td>${node.usedCpu?default('0')}%</td>
								<td>${node.usedMem?default('0')}M</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="managemen_mon_info_sp1">资源配置</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" >详情管理</span>
								</td>
							</tr>
							 </#list>
							 </#if>
						<#else>
						<div class="create_content_2">
			         		<div class="create_content_url_left">
			         		 暂无数据
							 </div>     
						</div>	
						 </#if>
						</tbody>
				</table>