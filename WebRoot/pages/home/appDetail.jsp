﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/appDetail.css">
<head>
<body>
	<h4 class="detail_title">基本信息</h4>
	<div class="detail_box">
	<ul>
		<li>ID:<span>123456</span></li>
		<li>名称：<span>xxx</span></li>
		<li>状态：<span></span></li>
	</ul>
	<ul>
		<li><span></span></li>
		<li>描述：<span></span></li>
	</ul>
	</div>
	
	<h4 class="detail_title">配置信息</h4>
	<div class="detail_box">
	<ul>
		<li>CPU:<span>1核</span></li>
		<li>内存：<span>1GB</span></li>
		<li>数据盘：<span>5G</span></li>
	</ul>
	<ul>
		<li>操作系统：<span></span></li>
		<li>公网IP：<span></span></li>
		<li>内网IP:<span></span></li>
	</ul>
	<ul>
		<li>地域：<span></span></li>
		<li>实力规格：<span></span></li>
		<li><span></span></li>
	</ul>
	</div>
	
	<h4 class="detail_title">其他</h4>
	<div class="detail_box">
	<ul>
		<li><span></span></li>
		<li><span></span></li>
		<li><span></span></li>
	</ul>
	</div>
</body>
</html>