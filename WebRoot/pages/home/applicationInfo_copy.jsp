﻿
<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/style.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/showchart.css">
	<script type="text/javascript" src="${requestatt.contextPath}/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
</style>

<script>
$(document).ready(function () { 
   /*查看统计图 参数*/
	var legend1='CPU使用量(百分比)'; //标注
	var chartTitle1='CPU使用量(百分比)'; //统计图顶部标题
	var xAxisArray1=new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'); //横坐标
	var chartData1=new Array(7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6); //数据
	var yAxisTitle1=''; //左侧标题 
	var suffix1='%';  //单位
	
	$('#managemen_mon_info_sp1').click(function () {
		createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,chartData1,yAxisTitle1,suffix1);
		$('#chartCatalogBox ul li').removeClass("chart_catalog_selected");
		$('#chartCatalogBox ul li#chart_catalog_weekly').addClass("chart_catalog_selected");  
	});
	
	$('#chartCatalogBox ul li').each(function(index){
		$(this).mouseover(function () { 
			$("#chartCatalogBox ul li.chart_catalog_selected:not(this)").removeClass("chart_catalog_selected"); 
			$(this).addClass("chart_catalog_selected");  
			if (index==0) createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,chartData1,yAxisTitle1,suffix1); //日统计图
			if (index==1) createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,chartData1,yAxisTitle1,suffix1);//周统计图
			if (index==2) createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,chartData1,yAxisTitle1,suffix1); //月统计图
		});
	});

    //底部统计图
	/*参数*/
	var legend2='CPU使用量(百分比)'; //标注
	var chartTitle2='CPU使用量(百分比)'; //统计图顶部标题
	var xAxisArray2=new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'); //横坐标
	var chartData2=new Array(9.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6); //数据
	var yAxisTitle2=''; //左侧标题 
	var suffix2='%';  //单位
	
	createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);

	$('#bottomChartCatalogBox ul li').each(function(index){
		$(this).mouseover(function () { 
			$("#bottomChartCatalogBox ul li.chart_catalog_selected:not(this)").removeClass("chart_catalog_selected"); 
			$(this).addClass("chart_catalog_selected");  
		
			if (index==0) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2); //日统计图
			if (index==1) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);//周统计图
			if (index==2) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2); //月统计图
		});
	});
	
	//查看日志
	$('#managemen_mon_info_sp2').click(function () {
		$('#showLogbox').load("../logs/hadoop.log");
		
	});
});
</script>
<head>

<body>
<#if result.success>
<#assign app=result.object>
					<input type="hidden" id="app_type" value="${app.appType}"/>
					<input type="hidden" id="app_appName" value="${app.appName}"/>
					<table cellpadding="0" cellspacing="0" border="0" id="appContent">
						<tbody>
							<tr id="instances_th" class="managemen_mon_title">
								<th>任务类型</th>
								<th>任务状态</th>
								<th>运行时间</th>
								<th>运行次数</th>
								<th>操作</th>
							</tr>
							<tr class="managemen_mon_info">
								<#assign type=app.appType>
								<#if type = 0>
									<td>mapreduce</td>
								 	<#elseif type = 1>
									<td>spark</td>
									 <#elseif type = 2>
									<td>storm</td>
								</#if>	
								<#assign status=app.status>
									<#if status = 0>
									<td>未启动</td>
								 	<#elseif status = 1>
									<td>运行中</td>
									 <#elseif status = 2>
									<td>已完成</td>
									</#if>
								<td>${app.createdDate}</td>
								<td>4</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="managemen_mon_info_sp1">配置任务</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" onclick=showAppDetail()>详情管理</span>
								</td>
							</tr>
						</tbody>
					</table>
<#else>
				<div class="create_content_2">
			         <div class="create_content_url_left">
						 <input type="text" id="instName" name="create_instName" style="padding-left:5px;line-height:30px;color:#999;" placeholder="请填入新实例名称" size="40"/>
					 </div>     
			        	<div class="create_content_button" onclick="" id="create_content_button"><a href="">创建实例</a></div>
				</div>			
</#if>	
	
				<div id="bottomChartCatalogBox" class="chart_catalog">
					<ul>
						<li>一个月</li>
						<li class="chart_catalog_selected">一周</li>
						<li class="radius_style_left">一日</li>
					</ul>
				</div>
	
				<div id="showBottomChartBox" style="margin:10px;margin-top:20px;">
				</div>
</body>
</html>