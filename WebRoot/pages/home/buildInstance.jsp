﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>

<%
String path = request.getContextPath();

%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/addInstance.css">
	<script type="text/javascript" src="<%=path%>/js/jquery.uploadify.min.js"></script>  
	<script type="text/javascript" src="<%=path%>/js/instance.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/uploadify.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/uploadfile.css">
	
	<style type="text/css">
	.uploadify {
		float:left;
		margin-left:15px;
	}

	</style>
</head>
<script type="text/javascript">
$(document).ready(function() {  
 	  var appid = location.search.substr(4); //获取url中"?"符后的字串
	  $("#file_upload").uploadify({  
          'buttonText' : '选择文件',  
          'height' : '30',  
          'swf' : '<%=path%>/js/uploadify.swf?var=' + (new Date()).getTime(),
          'uploader' : '<%=path%>/application/uploadjar.do',  
          'width' : '80', 
          'auto':true, 
          'formData': {
					'appid'     :appid,
					'fileType'  : 0
				},
          'fileObjName'   : 'file',  
          'onUploadSuccess' : function(file, data, response) {  
              $("#file_upload_name").text("上传成功："+file.name);
               $("#file_upload_name").val(file.name);
          }  
      });  
	 
	  $("#jarfile").uploadify({  
          'buttonText' : '选择文件',  
          'height' : '30',  
          'swf' : '<%=path%>/js/uploadify.swf?var=' + (new Date()).getTime(),  
          'uploader' : '<%=path%>/application/uploadjar.do',  
          'width' : '80', 
          'auto':true,  
           'formData': {
					'appid'     :appid,
					'fileType'  : 1
				},
          'fileObjName'   : 'file',  
          'onUploadSuccess' : function(file, data, response) {  
               $("#jarfile_name").text("上传成功："+file.name);
                $("#jarfile_name").val(file.name);
          }  
      }); 
      
      $("#file_upload_btn_app").click(function(){
      		$('#file_upload').uploadify('upload', '*');
      });
      
      $(".history_back").click(function(){
      	$("#app_selector li.appnav_selected").click();
      });
	});
</script>
<body>
    

		<div class="addInst_title">配置任务</div>
			<table class="addInst_table">
				<tbody>
				<tr>
					<td>
						<em>*</em>
						任务类型:
					</td>
					<td>
						<label id="apptype">
									&nbsp;&nbsp;&nbsp;&nbsp;mapreduce
								</label>
					</td>
				</tr>
				
				<tr>
					<td>
						<em>*</em>
						配置文件:
					</td>
					<td>
						<input name="configure_file" type="file" id="file_upload" />
					</td>
						
				</tr>
				<tr>
					<td>
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;<label name="file_upload_name" id="file_upload_name" ></label>
					</td>
						
				</tr>
				<tr>
					<td>
						<em>*</em>
						执行文件:
					</td>
					<td>
						<input name="jarfile" type="file" id="jarfile" onclick="$('#control_btn2').css('display','block');"/>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;<label name="jarfile_name" id="jarfile_name" ></label>
					</td>
						
				</tr>
				
				<tr>
					<td>
						<button class="addInst_button history_back" style="margin-top:50px;margin-left:20px;" id="build_button" >完成配置</button>
					</td>
					<td><!--  onclick="javascript:history.go(-1);" -->
						<button class="addInst_button history_back" style="margin-top:50px;margin-left:20px;" id="history_back">返回</button>
					</td>
				</tr>
				</tbody>
			</table>
		
</body>
</html>