﻿
<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/style.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/showchart.css">
	<script type="text/javascript" src="${requestatt.contextPath}/js/highcharts.js"></script>
	<script type="text/javascript" src="${requestatt.contextPath}/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
.servers_title {
margin-top: 20px;
margin-left: 0px;
text-align: left;
font-size: 20px;
line-height: 65px;
color: #ccc;
}
.compute_title {
margin-left: 0px;
text-align: left;
font-size: 20px;
color: #ccc;
}
</style>

<script>
  /*查看统计图 参数*/
	var legend1='时间'; //标注
	var chartTitle1='CPU使用量(百分比%)'; //统计图顶部标题
	var chartTitle1Idle = '进程数(个)';
	var chartTitle1Memory = '可用内存(兆M)';
	var xAxisArray1 = new Array();
	var charData1 = new Array();
	var charData1Memory = new Array();
	var charData1Idle = new Array();	
	var yAxisTitle1='百分比'; //左侧标题 
	var yAxisTitle1Memory = '可用内存';
	var yAxisTitle1Idle = '进程数';
	var suffix1='%';  //单位
	var suffix1Memory = 'M';
	var suffix1Idle = '个'

/*截取数字小数两位*/
function getFloat2(value){
	var insert = new Number(value);
	insert = insert.toFixed(2);
	return parseFloat(insert);
}
/*获取cpu使用率及对应的时间*/
$(document).ready(function () { 	
     var nodeurl = "../node/updatestatus.do";
	 $.get(nodeurl, "hostname=ubuntu-3", function(result){
			 // console.log("result"+result.object);
			  var jsonResult = $.parseJSON(result);
	 		  var jsonData = jsonResult.object;
	 		  console.log(jsonData);
	 		  var status = { };
	 		  for(var i=0; i < jsonData.length; i++){
	 		  	status = jsonData[i];
	 		  	 xAxisArray1[i] = status.created_time.slice(-8,-3);
	 		  	 charData1[i] = getFloat2(status.cpu_used);
	 		  	 charData1Idle[i] = getFloat2(status.cpu_idle);
	 		  	 charData1Memory[i] = getFloat2(status.mem_free/1000);
	 		  }
	 		  createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,charData1,yAxisTitle1,suffix1);
	 });

	$('#managemen_mon_info_sp1').click(function () {
		createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,charData1,yAxisTitle1,suffix1);
		$('#chartCatalogBox ul li').removeClass("chart_catalog_selected");
		$('#chartCatalogBox ul li#chart_catalog_weekly').addClass("chart_catalog_selected");  
	});
	
 	$('#chartCatalogBox ul li').each(function(index){
		$(this).mouseover(function () { 
			$("#chartCatalogBox ul li.chart_catalog_selected:not(this)").removeClass("chart_catalog_selected"); 
			$(this).addClass("chart_catalog_selected");  
			if (index==0) createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,charData1,yAxisTitle1,suffix1); 
			if (index==1) createChart('#showChartBox',legend1,chartTitle1Memory,xAxisArray1,charData1Memory,yAxisTitle1Memory,suffix1Memory);
			if (index==2) createChart('#showChartBox',legend1,chartTitle1Idle,xAxisArray1,charData1Idle,yAxisTitle1Idle,suffix1Idle); 
		});
	}); 

    //底部统计图
	/*参数*/
 	var legend2='CPU使用量(百分比)'; //标注
	var chartTitle2='CPU使用量(百分比)'; //统计图顶部标题
	var xAxisArray2=new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'); //横坐标
	var chartData2=new Array(9.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6); //数据
	var yAxisTitle2=''; //左侧标题 
	var suffix2='%';  //单位
	
	createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);

	$('#bottomChartCatalogBox ul li').each(function(index){
		$(this).mouseover(function () { 
			$("#bottomChartCatalogBox ul li.chart_catalog_selected:not(this)").removeClass("chart_catalog_selected"); 
			$(this).addClass("chart_catalog_selected");  
		
			if (index==0) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2); //日统计图
			if (index==1) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);//周统计图
			if (index==2) createChart('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2); //月统计图
		});
	}); 
	
	//查看日志
	$('#managemen_mon_info_sp2').click(function () {
		//$('#showLogbox').load("../logs/hadoop.log");
		
	});
	
  	$("#jump_buildInst_btn").click(function() {  
    	// $('#mainInfobox1').load("addInstance.jsp");  
		 $('#mainInfobox1').load("home/buildInstance.jsp");  
		 
		 var url = "../application/query.do";	
		
		//组装请求参数
		 var search = location.search; //获取url中"?"符后的字串
		 var str = search.substr(1);

		 $.post(url, str, function(result){
			 // console.log("result"+result.object);
			  var jsonResult = $.parseJSON(result);
			  $("#file_upload_name").text("配置文件："+jsonResult.object.configFile);
			  $("#jarfile_name").text("运行文件："+jsonResult.object.jarPath);
		 });
    });
    
    // 节点加载
    var nodeurl = "../node/findnode.fm";	
	//组装请求参数
    $.get(nodeurl, null, function(result){
		$("#nodelist_content").html(result);
		$(".server_manager").mouseenter(function(){
    		$(this).css("background-color","#E9E9E4");
  		});
  	
	  	$(".server_manager").mouseleave(function(){
	    		$(this).css("background-color","");
	  	});
	  	
	  	$(".server_manager").click(function(){
	  		
  	        $(".checkbox_radio").prop('checked', false);;
  	        $(this).children("td").children(".checkbox_radio").prop('checked', true);
  	         // $(this).children("td").children(".checkbox_radio").attr("checked","unchecked");
  	         //加载数据
  	         
  	     });
	  	
	});
	
	
	//任务进度条的百分比
	var tasknum = $("#tasknum").val();
	console.log("task num->"+tasknum);
	$("#taskPercent").css("width", tasknum*10+"%");
  	  
});

</script>
<head>

<body>
	<div class="compute_title">计算任务：</div>
<#if result.success>
<#assign app=result.object>
					<input type="hidden" id="app_type" value="${app.appType}"/>
					<input type="hidden" id="app_appName" value="${app.appName}"/>
					<input type="hidden" id="tasknum" value="${app.tasknum}"/>
					<table cellpadding="0" cellspacing="0" border="0" id="appContent">
						<tbody>
							<tr id="instances_th" class="managemen_mon_title">
								<th>任务类型</th>
								<th>任务状态</th>
								<th>运行时间</th>
								<th>运行次数</th>
								<th>操作</th>
							</tr>
							<tr class="managemen_mon_info">
								<#assign type=app.appType>
								<#if type = 0>
									<td>mapreduce</td>
								 	<#elseif type = 1>
									<td>spark</td>
									 <#elseif type = 2>
									<td>storm</td>
								</#if>	
								<#assign status=app.status>
									<#if status = 0>
									<td>未启动</td>
								 	<#elseif status = 1>
									<td>运行中</td>
									 <#elseif status = 2>
									<td>已完成</td>
									</#if>
								<td>${app.createdDate}</td>
								<td>${app.tasknum}</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="jump_buildInst_btn">配置任务</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" onclick=showAppDetail()>详情管理</span>
									<span class="managemen_mon_info_sp" id="managemen_mon_info_sp2" style="margin-left:10px;" onclick=showLog()>查看日志</span>
								</td>
							</tr>
						</tbody>
					</table>
<#else>
				<div class="create_content_2">
			         <div class="create_content_url_left">
						 <input type="text" id="instName" name="create_instName" style="padding-left:5px;line-height:30px;color:#999;" placeholder="请填入新实例名称" size="40"/>
					 </div>     
			        	<div class="create_content_button" onclick="" id="create_content_button"><a href="">创建实例</a></div>
				</div>			
</#if>	
				<div class="up1">
						<div class="servers_title">计算节点：</div>
				</div>
				<div id="nodelist_content">
				<table cellpadding="0" cellspacing="0" border="0" id="appContent">
						<tbody>
							<tr id="instances_th" class="managemen_mon_title">
								<th>服务器名称</th>
								<th>操作系统</th>
								<th>IP</th>
								<th>CPU使用率</th>
								<th>内存使用率</th>
								<th>监控</th>
							</tr>
							<tr class="managemen_mon_info server_manager">
								<td>ubuntu-3</td>
								<td>4</td>
								<td>4</td>
								<td>4</td>
								<td>4</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="managemen_mon_info_sp1">资源配置</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" >详情管理</span>
								</td>
							</tr>
							<tr class="managemen_mon_info server_manager">
								<td>ubuntu-6</td>
								<td>4</td>
								<td>ubuntu</td>
								<td>4</td>
								<td>4</td>
								<td align="center">
									<span class="managemen_mon_info_sp"  id="managemen_mon_info_sp1">资源配置</span>
									<span class="managemen_mon_info_sp" style="margin-left:10px;" >详情管理</span>
								</td>
							</tr>
						</tbody>
				</table>
				</div>
				
				<div class="popWindow_content" >
					 <div id="chartCatalogBox" class="chart_catalog">
						  <ul>
							<li>CPU</li>
							<li id="chart_catalog_weekly" class="chart_catalog_selected">内存</li>
							<li class="radius_style_left">进程数</li>
						  </ul>
					 </div>
				     <div id="showChartBox" style="margin:10px;margin-top:80px;">
					 </div>
				</div>
				
</body>
</html>