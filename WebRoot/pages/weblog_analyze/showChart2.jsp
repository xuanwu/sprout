
<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
</style>

<script>
$(document).ready(function () { 

	var url = "../../weblogs/pv.do";	
    $.post(url,"", function(result){
        var jsonResult = $.parseJSON(result);
		
		var legend2='网页浏览量(次数)'; 
		var chartTitle2='网页浏览量(次数)';
		//var xAxisArray2=new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		//var chartData2=new Array(9.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6); 
		var xAxisArray2=jsonResult.object.name;
		//alert(xAxisArray2);
		var chartData2=jsonResult.object.data;
		var max=jsonResult.object.max;
		//alert(chartData2);
		var yAxisTitle2='次数'; 
		var suffix2='次'; 
		
		//createChart('#showBottomChartBox2',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);
		//createChartForWeblogs('#showBottomChartBox2',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2,max);
		createChartForCategory('#showBottomChartBox2',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);                    
	});
});

function createChartForCategory(boxID,legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2){
	$(function () {                                                                
	    $(boxID).highcharts({                                           
	        chart: {                                                           
	            type: 'bar'                                                    
	        },                                                                 
	        title: {                                                           
	            text: chartTitle2                    
	        },                                                                 
	        subtitle: {                                                        
	            text: ' '                                  
	        },                                                                 
	        xAxis: {                                                           
	            categories: xAxisArray2,
	            title: {                                                       
	                text: null                                                 
	            }                                                              
	        },                                                                 
	        yAxis: {                                                           
	            min: 0,                                                        
	            title: {                                                       
	                text: '次',                             
	                align: 'high'                                              
	            },                                                             
	            labels: {                                                      
	                overflow: 'justify'                                        
	            }                                                              
	        },                                                                 
	        tooltip: {                                                         
	            valueSuffix: suffix2                                  
	        },                                                                 
	        plotOptions: {                                                     
	            bar: {                                                         
	                dataLabels: {                                              
	                    enabled: true                                          
	                }                                                          
	            }                                                              
	        },                                                                 
	        legend: {                                                          
	            layout: 'vertical',                                            
	            align: 'right',                                                
	            verticalAlign: 'top',                                          
	            x: -40,                                                        
	            y: 100,                                                        
	            floating: true,                                                
	            borderWidth: 1,                                                
	            backgroundColor: '#FFFFFF',                                    
	            shadow: true                                                   
	        },                                                                 
	        credits: {                                                         
	            enabled: false                                                 
	        },                                                                 
	        series: [{                                                         
	            name: legend2,                                             
	            data: chartData2                                  
	        }]                                                                 
	    });                                                                    
	}); 
}

</script>
<head>

<body>
	
				<div id="showBottomChartBox2" style="margin:10px;margin-top:20px">

				</div>
</body>
</html>