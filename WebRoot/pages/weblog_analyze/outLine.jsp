<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
<script type="text/javascript" src="<%=path%>/js/app.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/highcharts.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/webLog.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
<!--<script type="text/javascript"  src="<%=path%>/js/highstock.js"></script>
	
	-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>-->

</head>

<body>


	<!--顶部app信息-->
	<div class="up" style="margin-top: 2px">
		<div class="left">
			<div class="app_icon">
				<img src="<%=path%>/images/app_icon.png" />
			</div>
			<div class="app_name">
				<a id="applink" href="" target="_blank">service</a>
			</div>
		</div>
		<div style="margin-right: 100px; padding-top: 30px">
			<select name="sldd" style="width: 58px; float: right" onchange="">
				<option value="1" selected="">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
			</select>
			<div style="float: right">请选择服务：</div>
		</div>
	</div>

	<script>
		var categoryInformation = window.location.search.split('&&');
		var categoryName = categoryInformation[1];
		document.getElementById('applink').innerHTML = '正在查看Category : '
				+ categoryName;

		$(document).ready(function() { //页面加载完成后事件

			$("#mainInfobox1").load("showChart1.jsp");
			$("#app_selector li").each(function(index) { //循环每个li，index表示循环当前li的索引
				$(this).click(function() { //为li注册点击事件
					$("li.appnav_selected").removeClass("appnav_selected");
					$(this).addClass("appnav_selected");
					if (index == 0) {
						$("#mainInfobox1").load("showChart1.jsp");
					} else if (index == 1) {

						$("#mainInfobox1").load("showChart2.jsp"); //服务详情
					} else if (index == 2) {
						$("#mainInfobox1").load("showChart3.jsp"); //环境变量
					} else if (index == 3) {
						$("#mainInfobox1").load("showChart4.jsp");//代码管理
					}
				});
			});

		});
	</script>

	<div class="maincontent">
		<!--左边菜单栏-->
		<ul class="appMan_nav" id="app_selector">
			<li id="appnav1" class="appnav appnav_selected">浏览器浏览量</li>
			<li id="appnav2" class="appnav">网页浏览量</li>
			<li id="appnav3" class="appnav">网页浏览状态</li>
			<li id="appnav4" class="appnav">独立访问IP</li>
		</ul>

		<!--右边详细信息-->
		<div class="maininfo">
			<!--应用详情界面-->
			<div id="mainInfobox1"></div>



		</div>
	</div>

</body>
</html>