<!DOCTYPE html>

<%@ page language="java" import="com.sprout.webapp.model.UserInfo,java.io.*,java.util.*" pageEncoding="UTF-8" %>

<%
String path = request.getContextPath();
UserInfo user = (UserInfo) session.getAttribute("currentUser");
if (user == null) {
	// response.setContentType("application/json; charset=utf-8");
    response.setHeader("cache-control", "no-cache");
	PrintWriter outprint = response.getWriter();
	outprint.write("<script type=\"text/javascript\">(function() {window.location = '../login-new.jsp';})();</script>");
	outprint.flush();
	outprint.close();
	return;
}
%>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<script type="text/javascript" src="<%=path%>/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
	<script type="text/javascript" src="<%=path%>/js/service/create.js"></script>
	
</head>

<body>

    <div class="header"  style="position:static">
		<div class="header1">
		<div class="headline" style="padding:0 40px;">
				<img src="<%=path%>/images/heading.png"/>
			</div>
			<ul id="nav">
			  <li><a href="<%=path%>/pages/index.jsp">云服务</a></li>
				<li><a href="<%=path%>/pages/server.jsp">服务列表</a></li>
			</ul>
			
			<ul class="upright">
				<!--登出-->
				<li id="logout"><a href="<%=path%>/logout.do">退出登录</a></li>
				
				<!--用户登录信息-->
				<li id="login_info"><%="尊敬的 " + user.getUsername() + " , 欢迎您!"%></li>
			</ul>
		</div>
    </div>
	
	<!--顶部信息和左边菜单栏-->
    <div class="container">
		<jsp:include page="outLine.jsp"/>
    </div>
    
	<div class="footer">
		<div class="footer_Info">
			  <a href="<%=path%>/">联系我们</a></li>
		</div >
		<div class="footer_Info">
			  <a href="<%=path%>/">about sprout</a></li>
		</div >
	</div>
	
	
</body>
</html>