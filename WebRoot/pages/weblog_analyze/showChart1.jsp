
<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
</style>

<script> 
/*
$(document).ready(function () { 
	var url = "../../weblogs/brower.do";	
    $.post(url,"", function(result){
        var jsonResult = $.parseJSON(result);
		
		var legend2='各浏览器浏览量(次数)'; 
		var chartTitle2='各浏览器浏览量(次数)';
		//var xAxisArray2=new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		//var chartData2=new Array(9.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6); 
		var xAxisArray2=jsonResult.object.name;
		//alert(xAxisArray2);
		var chartData2=jsonResult.object.data;
		var max=jsonResult.object.max;
		//alert(chartData2);
		var yAxisTitle2='次数'; 
		var suffix2='次'; 
		
		createChartForWeblogs('#showBottomChartBox',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2,max);
	});
});*/

$(document).ready(function(){
	var url = "../../weblogs/browser.do";
	var cid = window.location.search.substring(1,window.location.search.length).split('&&');
	$.post(url,{cid : cid[0]},function(result){
		var jsonResult = $.parseJSON(result);
		
		var tableColumn1 = jsonResult.object.name;
		var tableColumn2 = jsonResult.object.data;
		
		var jsTable = document.getElementById('table');
		var jsTableBody = document.getElementById('tbody1');
		
		for(var i = 0;i < tableColumn1.length;i++){
			var row = document.createElement('tr');
			row.class = 'class' + i;
			
			var cell1 = document.createElement('td');
			cell1.class = 'text-left';
			cell1.innerHTML = tableColumn1[i];
			
			var cell2 = document.createElement('td');
			cell2.class = 'text-right';
			cell2.innerHTML = tableColumn2[i];
			
			row.appendChild(cell1);
			row.appendChild(cell2);
			
			jsTableBody.appendChild(row);
		}
		
		
	});
});

</script>
<head>

<body>
	
				<!-- <div id="showBottomChartBox" style="margin:10px;margin-top:20px;">
				</div> -->
				
	<table id="table" class='table'>
		<thead>
			<tr class="category_htr">
				<th class="text-left width-25">浏览器</th>
				<th class="text-right width-25">次数</th>
			</tr>
		</thead>
	<tbody id='tbody1'>
	</table>
</body>
</html>