<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String username = request.getParameter("username");
	String password = request.getParameter("password");
%>
<html>
<script type="text/javascript">

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
#ali-btn {
	padding: 0;
	list-style: none;
	margin: 0 auto;
	border: 1px solid #eee;
	width: 511px;
	height: 350px;
}

#ali-btn li {
	text-align: center;
	position: relative;
	width: 255px;
	height: 175px;
	float: left;
	border-right: 1px dashed #ddd;
	border-bottom: 1px dashed #ddd;
}

#ali-btn li.last {
	border: none;
	border-bottom: 1px dashed #ddd;
}

#ali-btn p {
	font-size: 12px;
}

#ali-btn .box,#ali-btn .box-hover {
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
}

#ali-btn .box-hover {
	color: #fff;
}

#ali-btn .box-hover.blue {
	background-color: #00a2ca;
}

#ali-btn .box-hover.green {
	background-color: #39cea3;
}

#ali-btn .box-hover.orange {
	background-color: #f06736;
}

#ali-btn .box-hover.yellow {
	background-color: #ffb90f;
}

#ali-btn .box p {
	font-size: 16px;
	font-weight: 500;
}

#ali-btn .box-hover a {
	color: #fff;
	text-decoration: none;
	padding: 0 30px 1px;
	margin-top: 60px;
	border: 1px solid #fff;
	border-radius: 4px;
	line-height: 33px;
	font-size: 14px;
	display: inline-block;
	-webkit-transition: background-color 0.3s, color 0.3s;
	-moz-transition: background-color 0.3s, color 0.3s;
	-ms-transition: background-color 0.3s, color 0.3s;
	transition: background-color 0.3s, color 0.3s;
}

#ali-btn .box-hover a:hover {
	color: #000;
	background-color: #fff;
}

#ali-btn img {
	height: 60px;
	margin-bottom: 10px;
	padding-top: 10px;
}

#ali-btn .hide {
	display: none;
}

#ali-btn .price {
	height: 60px;
	margin-bottom: 10px;
	padding-top: 4px;
}

#ali-btn .price p {
	margin: 0;
	padding: 0;
}

#ali-btn .price span {
	font-size: 18px;
}

#ali-btn .text {
	padding-top: 15px;
	color: #666;
}

#ali-btn .title {
	font-size: 16px;
	font-weight: 500;
	margin-top: 10px;
}
</style>
</head>
<body>
	<ul id="ali-btn">
		<li id="server">
			<div class="box">
				<h2 class="title">云主机管理</h2>
				<img src="<%=path%>/images/ecs.png" />
				<p class="text">虚拟主机服务</p>
			</div>
			<div class="box-hover yellow hide">
				<h2 class="title">云主机管理</h2>
				<a href="http://125.216.243.85:8080/openstack/loginCheck?username=<%=username%>&password=<%=password%>" id='select1'>进入</a>
			</div>
		</li>
		<li class="last">
			<div class="box">
				<h2 class="title">数据分析服务</h2>
				<img src="<%=path%>/images/odps.png" />
				<p class="text">日志分析/性能预测</p>
			</div>
			<div class="box-hover blue hide">
				<h2 class="title">数据处理服务</h2>
				<a href="application_list.jsp" id='select2'>进入</a>
			</div>
		</li>
		<li>
			<div class="box">
				<h2 class="title">日志监控服务</h2>
				<!--<img src="<%=path%>/images/index_logs.jpg" />
				--><img src="<%=path%>/images/log.png" />
				<p class="text">高效日志分析</p>
			</div>
			<div class="box-hover orange hide">
				<h2 class="title">简单日志分析服务</h2>
				<a href="weblogs/logHome.jsp" id='select3'>进入</a>
			</div>
		</li>
		<li class="last">
			<div class="box">
				<h2 class="title">云监控</h2>
				<img src="<%=path%>/images/site.png" />
				<p class="text">警报保障</p>
			</div>
			<div class="box-hover yellow hide">
				<h2 class="title">云监控服务</h2>
				<a href="monitor/monitor_index.jsp" id='select4'>进入</a>
			</div>
		</li>
<%-- 		<li class="last">
			<div class="box">
				<h2 class="title">应用服务器监控</h2>
				<img src="<%=path%>/images/monitor.png" />
				<p class="text">tomcat</p>
			</div>
			<div class="box-hover green hide">
				<h2 class="title">服务器监控服务</h2>
				<a href="tomcat/index.jsp" id='select3'>进入</a>
			</div>
		</li>
		<li class="last">
			<div class="box">
				<h2 class="title">站点监控服务</h2>
				<img src="<%=path%>/images/site.png" />
				<p class="text">警报保障</p>
			</div>
			<div class="box-hover yellow hide">
				<h2 class="title">站点监控服务</h2>
				<a href="site/index.jsp" id='select4'>进入</a>
			</div>
		</li> --%>
	</ul>

	<script>
		$('#ali-btn').on('mouseover', '.box', function() {
			$('#ali-btn').find('.box-hover').addClass('hide');
			$(this).parent().find('.box-hover').removeClass('hide');
		});
	</script>
</body>
</html>