<!DOCTYPE html >

<html>
<head>
	<meta http-equiv="pragma" content="no-cache"  />
   <meta http-equiv="content-type" content="no-cache, must-revalidate" />
   <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/console/console.css">
		
</head>
<body>
	<div id="top_cat">
		<div id="top_title">警报管理列表</div>	
	</div>
	
	<button class="create_btn" id="createMonitor">创建新警报</button>
	<div class="_content" style="border-top:none;top:5px;">
		<input type="hidden" id="monitor_type" value="">
		<input type="hidden" id="monitorid" value="">
		<table  id="categoryContent"  >
			<thead>
				<tr class="category_htr">
					<th class="text-center width-15">监控名称</th>
					<th class="text-center width-15">通知用户</th>
					<th class="text-center width-15">监控类型</th>
					<th class="text-center width-15">实例名称</th>
					<th class="text-center width-15">描述</th>
					<th class="text-center width-15">状态</th>
					<th class="text-right width-25" style="padding-right:110px;">操作</th>
				</tr>
			</thead>
			<tbody >
			<#if result.success>
						<#assign lists=result.object>
						<#if lists?size &gt; 0>	
						<#list lists as node>	
						<tr class="managemen_mon_info server_manager">
							<td><a class="server_host" >${node.monitorname?default('null')}</a></td>
							<td>${node.user?default('null')}</td>
							<td id="monitor_type">${node.type?default('null')}</td>
							<td>${node.instancename?default('null')}</td>
							<td>${node.describle?default('')}</td>
							<#if node.has_send==0>
								<td style="color: #090">正常</td>
							<#else>
								<td ><font color="#FF0000">已警报</font></td>
							</#if>
							<td align="center">
								<span class="managemen_mon_info_sp monitor_start" value="${node.monitorId}" style="margin-left:10px; color: #C0C0C0" >启动</span>
								<span class="managemen_mon_info_sp monitor_view" value="${node.monitorId}" style="margin-left:10px;" >详情</span>
								<span class="managemen_mon_info_sp monitor_delete" value="${node.monitorId}" style="margin-left:10px;" >删除</span>
							</td>
						</tr>
						<tr class="managemen_mon_info server_manager">
							<td><a class="server_host" >${node.monitorname_resp?default('null')}</a></td>
							<td>${node.user?default('null')}</td>
							<td id="monitor_type">${node.type?default('null')}</td>
							<td>${node.instancename?default('null')}</td>
							<td>${node.describle_resp?default('')}</td>
							<#if node.has_send==0>
								<td style="color: #090">正常</td>
							<#else>
								<td ><font color="#FF0000">已警报</font></td>
							</#if>
							
							<td align="center">
								<span class="managemen_mon_info_sp monitor_start" value="${node.monitorId}" style="margin-left:10px;color: #C0C0C0" >启动</span>
								<span class="managemen_mon_info_sp monitor_view" value="${node.monitorId}" style="margin-left:10px;" >详情</span>
								<span class="managemen_mon_info_sp monitor_stop" value="${node.monitorId}" style="margin-left:10px;" >暂停</span>
								<span class="managemen_mon_info_sp monitor_delete" value="${node.monitorId}" style="margin-left:10px;" >删除</span>
							</td>
						</tr>
						</#list>
						<#else>
						<tr>
							<td class="text-center create" colspan="4">
								暂无数据！
							</td>
						</tr>
					</#if>
			 </#if>
			 </tbody>
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
		<div id="hiden_content" style="display: none;">
		</div>
</body>
	<script>
	$(document).ready(function () { //页面加载完成后事件
			$("#hiden_content").load("warning/warning_create.jsp");
			
			var search = location.search; //获取url中"?"符后的字串
			var str = search.substr(1);
				
			$(".monitor_view").click(function(){
				var id = $(this).attr("value");
				var url = "../../warning/view.fm";	
			   //组装请求参数
	           	$.post(url, str+"&id="+id, function(result){
	           		 $("#mainInfobox1").html(result);
				});
				
				
			});
				
			$("#createMonitor").click(function(){
				$("#monitor_id").val($("#monitorid").val());
				$("#type").val($("#monitor_type").val());
				var result = $("#mainInfobox1").children();
				$("#mainInfobox1").html($("#hiden_content").children());
				$(".back_monitor").click(function(){
					$("#mainInfobox1").children().remove();
					$("#mainInfobox1").html(result);
				});
			});
			$(".monitor_stop").click(function(){
				var id = $(this).attr("value");
				$(this).removeClass("monitor_stop");
				$(this).addClass("monitor_start");
				var url = "../../warning/update.fm";	
			   //组装请求参数
	           	$.post(url, str+"&type=stop&status=0&id="+id, function(result){
	           		  var jsonResult = $.parseJSON(result);
						if(jsonResult.success){
							alert(jsonResult.object);
						}
				});
			});
			
			$(".monitor_start").click(function(){
				var id = $(this).attr("value");
				(this).removeClass("monitor_start");
				$(this).addClass("monitor_stop");
				
				var url = "../../warning/update.fm";	
			   //组装请求参数
	           	$.post(url, str+"&type=start&status=1&id="+id, function(result){
	           		  var jsonResult = $.parseJSON(result);
						if(jsonResult.success){
							alert(jsonResult.object);
						}
				});
			});
			
			$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
		  	});
		  	
		  	$(".server_manager").mouseleave(function(){
		    		$(this).css("background-color","");
		  	});
			
	});
	/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>

</html>