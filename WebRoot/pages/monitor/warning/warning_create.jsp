<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>
$(document).ready(function () { //页面加载完成后事件

	$("#create_monitor").click(function(){
		//$(this).attr("style", "");
			// 节点加载
	    var nodeurl = "../../warning/create.fm";
		//组装请求参数
		var data = $("#create_form").serialize();
		console.log("data -->"+data);
		
	    $.post(nodeurl, data, function(result){
	    	console.log("result -->"+result);
	    	var jsonResult = $.parseJSON(result);
	    	if(jsonResult.success==true){
	    		alert(jsonResult.message);
	    		load_list();
	    		//$("#mainInfobox1").load("pinglist.jsp");
	    	 }else{
	    	 	alert("添加失败");
	    	 }
		});
	});
	
});

function load_list(){
		var monitor_id = $("#monitor_id").val();
		var type_val = $("#type").val();
		var search = location.search.substr(1); //获取url中"?"符后的字串
		var warningurl = "../../monitor/findwarningbyId.fm"
		$.post(warningurl, search+"&type="+type_val+"&respone_html=warning_list_type&id="+monitor_id, function(result){
			$("#mainInfobox1").children().remove();
			$("#mainInfobox1").html(result);
			console.log("dataod--->"+monitor_id);
			$("#monitorid").val(monitor_id);
			$("#monitor_type").val(type_val);
		}); 
}
</script>
<style>
._content .addPro_table{
	margin:20px 0;
}

.form-item-note {
padding-top: 4px;
color: #A09B9B;
line-height: 18px;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">创建新警告</div>
		<a id="history_back" class="back_monitor">返回数据表列表</a>		
	</div>
	
	<div class="_content">
			<table class="addPro_table" >
			 <form id="create_form" method="post" action=" " >
			 		<input type="hidden" id="monitor_id" name="monitor_id" value="-1">
			 		<input type="hidden" id="type" name="type" value="-1">
					<tbody id="server_info">
					<tr>
						<td>
						监控名称:
						</td>
						<td colspan="5"><input name="name" type="text" placeholder="只能包含英文字母、数字，长度小于128个字符"/></td>
					</tr>
					<tr>
						<td>
						监控次数:
						</td>
						<td colspan="5"><select name="monitor_num" >
										<option value="3" selected="selected" >3</option>
										<option value="5" >5</option>
										<option value="10">10</option>
										</select>
										<span class="form-item-note ">重试几次后报警(连接不上和响应大于阈值的连续次数)</span>
										</td>
										
					</tr>
					<tr>
						<td>
						响应阈值:
						</td>
						<td ><input name="resp_time" type="text" placeholder="输入数字" value="5000" style="width: 80px"/>
						<span class="form-item-note "> 响应时间大于阈值则报警(毫秒)</span>
						</td>
					</tr>
					<tr>
						<td>
						通知人员:
						</td>
						<td colspan="5"><select name="monitor_people">
										<option value="zhou" selected="selected">zhou</option>
										<option value="haosheng">haosheng</option>
										</select><span class="form-item-note ">选择预警通知人员</span></td>
					</tr>
					
					</tbody>
				</form>
			</table>
			
		<button class="webadd_button" id="create_monitor">创建</button>
		<button class="webcancel_button back_monitor" >取消</button>
		
	
	</div>
</body>	
</html>