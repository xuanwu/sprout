<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>
$(document).ready(function () { //页面加载完成后事件
	// 节点加载
    var nodeurl = "../../warning/view.fm";
	//组装请求参数
	var search = location.search; //获取url中"?"符后的字串
	var str = search.substr(1)+"&port=8080";
    $.get(nodeurl, str, function(result){
       responedata = result;
	   console.log(result);
	   var jsonResult = $.parseJSON(result);
	  if(jsonResult.success==true){
	       /* var list = jsonResult.object;
	         console.log("hello list"+list);
	  		for(var i =0; i< list.length; i++){
	  			var html = "<tr class='web_data'> "+
	  			 		   "<td class='text-left web_name' id='catName'><em>*</em>"+list[i].name+"</td>"+
	  			 		   "<td class='text-center value'>"+list[i].value+"</td>"+
						   "</tr>";
				console.log("end app"+html);
				$("#server_info").append(html);
	  		}*/
	  }
	});	
});
</script>
<style>
._content .addPro_table{
	margin:20px 0;
}
addPro_table td {
padding: 8px;
border-bottom: 1px solid #eeeeee;
line-height: 15px;
}
.detail_value {
border-right: 1px solid #eeeeee;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">系统信息</div>
	</div>
	
	<div class="_content">
		<form id="createList_form" method="post" action=" " >
			<table class="addPro_table" cellpadding="0" cellspacing="0" width="100%">
				<tbody id="server_info">
				<tr>
					<tr>
			            <td width="10%" style="border-left:1px solid #eeeeee">监控项：</td>
			            <td width="20%" class="detail_value">ping.loss</td>
			            <td width="10%">监控目标：</td>
			            <td width="30%" class="detail_value">instanceId=5615366f-d786-40b7-84db-192af362a098</td>
			            <td width="10%">当前状态：</td>
			            <td width="20%" class="detail_value"><font color="#32CD32">正常</font></td>
        			</tr>
        			<tr>
			            <td style="border-left:1px solid #eeeeee">统计方法：</td>
			            <td class="detail_value">平均值</td>
			            <td>操作符:</td>
			            <td class="detail_value">&gt;</td>
			            <td>阈值:</td>
			            <td class="detail_value">50</td>
        			</tr>
        			
        			<tr>
			            <td style="border-left:1px solid #eeeeee">统计周期：</td>
			            <td class="detail_value">5分钟</td>
			            <td>重试次数：</td>
			            <td class="detail_value">3</td>
			            <td>通知方式：</td>
			            <td class="detail_value">zhou</td>
       				 </tr>
				</tr>
				</tbody>
			</table>
			
		</form>
		
	</div>
</body>	
</html>