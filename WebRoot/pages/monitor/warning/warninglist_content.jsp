
			<tbody  >
<#if result.success>
			<#assign lists=result.object>
			<#if lists?size &gt; 0>	
			<#list lists as node>	
			<tr class="managemen_mon_info server_manager">
							<td><a class="server_host" >${node.monitorname?default('null')}</a></td>
							<td>${node.user?default('null')}</td>
							<td id="monitor_type">${node.type?default('null')}</td>
							<td>${node.instancename?default('null')}</td>
							<td>${node.describle?default('')}</td>
							<#if node.has_send==0>
								<td style="color: #090">正常</td>
							<#else>
								<td ><font color="#FF0000">已警报</font></td>
							</#if>
							<td align="center">
								<span class="managemen_mon_info_sp monitor_start" value="${node.id}" style="margin-left:10px; color: #C0C0C0" >启动</span>
								<span class="managemen_mon_info_sp monitor_view" value="${node.id}" style="margin-left:10px;" >详情</span>
								<span class="managemen_mon_info_sp monitor_delete" value="${node.id}" style="margin-left:10px;" >删除</span>
							</td>
						</tr>
						<tr class="managemen_mon_info server_manager">
							<td><a class="server_host" >${node.monitorname_resp?default('null')}</a></td>
							<td>${node.user?default('null')}</td>
							<td id="monitor_type">${node.type?default('null')}</td>
							<td>${node.instancename?default('null')}</td>
							<td>${node.describle_resp?default('')}</td>
							<#if node.has_send==0>
								<td style="color: #090">正常</td>
							<#else>
								<td ><font color="#FF0000">已警报</font></td>
							</#if>
							<td align="center">
								<span class="managemen_mon_info_sp monitor_start" value="${node.id}" style="margin-left:10px; color: #C0C0C0" >启动</span>
								<span class="managemen_mon_info_sp monitor_stop" value="${node.id}" style="margin-left:10px;" >暂停</span>
								<span class="managemen_mon_info_sp monitor_delete" value="${node.id}" style="margin-left:10px;" >删除</span>
							</td>
						</tr>
			</#list>
			<#else>
			<tr>
				<td class="text-center create" colspan="3">
					暂无数据！
				</td>
				<td class="text-center create" colspan="4">
					暂无数据！
				</td>
			</tr>
		</#if>
 </#if>
 </tbody>