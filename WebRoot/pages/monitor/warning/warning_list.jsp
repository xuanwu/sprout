<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
	<meta http-equiv="pragma" content="no-cache"  />
   <meta http-equiv="content-type" content="no-cache, must-revalidate" />
   <meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
		
</head>
<body>
	<div id="top_cat">
		<div id="top_title">警报管理列表</div>	
	</div>
	<input type="hidden" id="monitorid" value="-1">
	<div class="_content" style="border-top:none;top:5px;">
		<table  id="categoryContent"  >
			<thead>
				<tr class="category_htr">
					<th class="text-center width-15">监控名称</th>
					<th class="text-center width-15">通知用户</th>
					<th class="text-center width-15">监控类型</th>
					<th class="text-center width-15">实例名</th>
					<th class="text-center width-15">描述</th>
					<th class="text-center width-15">状态</th>
					<th class="text-right width-25" style="padding-right:50px;">监控操作</th>
				</tr>
			</thead>
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</body>
	<script>
	$(document).ready(function () { //页面加载完成后事件
			
			var url = "../../monitor/findwarning.fm";	
			//组装请求参数
			var search = location.search; //获取url中"?"符后的字串
			var str = search.substr(1);
           	$.post(url, str+"&type=", function(result){
				$("#categoryContent").append(result);
				$(".monitor_view").click(function(){
						//var hostname = $(this).attr("data");
						$("#mainInfobox1").load("warning/waring_Info.jsp");
				});
				
				$(".monitor_delete").click(function(){
					//var hostname = $(this).attr("data");
					var id=$(this).val();
					var deleteurl = "../../warning/delete.fm";	
					$.post(deleteurl, str+"&id="+id, function(result){
						$("#mainInfobox1").load("warning/warning_list.jsp");
					});
					
				});
				
				$(".server_manager").mouseenter(function(){
			    		    $(this).css("background-color","#E9E9E4");
			  	});
			  	
			  	$(".server_manager").mouseleave(function(){
			    		$(this).css("background-color","");
			  	});
			  	$(".warning_status").css("color","#090");
			});
			
	});
	/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>

</html>