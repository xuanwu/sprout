﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String host = request.getParameter("host");
String port = request.getParameter("port");
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	
<script>

var responedata;
$(document).ready(function () { //页面加载完成后事件
	// 节点加载
    var nodeurl = "../../monitor/apps_managelist.fm";
	var hostip = "<%=host%>";
	var mport = "<%=port%>";
	//组装请求参数

    $.get(nodeurl, "host="+hostip+"&port="+mport, function(result){
       responedata = result;
	   var jsonResult = $.parseJSON(result);
	  if(jsonResult.success==true){
	        var list = jsonResult.object;
			if(list.length > 0){
		  		for(var i =0; i< list.length; i++){
		  			var html = "<tr class='web_data server_manager'> "+
		  			 		   "<td class='text-left web_name' id='catName'>"+list[i].app+"</td>"+
		  			 		   "<td class='text-center' >"+list[i].session+"</td>"+
		  			 		   "<td class='text-center web_status'>"+list[i].status+"</td>"+
		  			 		   "<td class='text-center' app='"+list[i].app+"'>"+
		  			 		   "<span class='managemen_mon_info_sp start_tomcat' style='margin-left:10px;'>启动</span>"+
		  			 		   "<span class='managemen_mon_info_sp stop_tomcat' style='margin-left:10px;' >关闭</span>"+
		  			 		   "<span class='managemen_mon_info_sp' style='margin-left:10px;' >部署</span>"+
		  			 		   "<span class='managemen_mon_info_sp' style='margin-left:10px;' >卸载</span>"+
							   "</td></tr>";
					$("#webs_content").before(html);
						
		  		}
		  		$(".web_name").click(function(){
		  				var index=$(".web_name").index(this);
	
		  				var web_name = $(this).text();
		  				var hostname = location.origin;
		  				if(index==0){
		  					web_name="";
		  				}
		  				window.open(hostname+"/"+web_name);
		  		});
		  		$(".web_status").each(function(){
		  		    var managers = $(this).next().children();
		  			if(!$(this).text().search("running")){
		  				change_class_unclick($(managers[0]));
		  			}else{
		  				change_class_unclick($(managers[1]));
		  			}
		  		});
		  		//start当前web项目启动
		  		$(".start_tomcat").click(start_click);
		  		//stop当前web项目启动
		  		$(".stop_tomcat").click(stop_click);
		  		
		  		$(".server_manager").mouseenter(function(){
			    		$(this).css("background-color","#E9E9E4");
			  	});
			  	
			  	$(".server_manager").mouseleave(function(){
			    		$(this).css("background-color","");
			  	});
			  	
			  	$("#create").hide();
			}
			else{
				$("#create").show();
			}
			
	  }
	  else{
		  alert(jsonResult.message);
		  $("#mainInfobox1").load("../monitor/appserver/appserver_list.jsp");
	  }
	});	
	
	$("#createMonitor").click(function(){
		$("#mainInfobox1").load("../monitor/appserver/appserver_create.jsp");
	});   
});
	function start_click(){
  			var app = $(this).parent("td").attr("app");
  			var lable= $(this).parent("td");

  			var url = "../../monitor/apps_manage.fm";	
			//组装请求参数

           	$.post(url, "host="+hostip+"&port="+mport+"&type=start&path="+app, function(result){
           		startresult=lable;
				console.log("result--->"+$(this).parent("td").prev());
				if(result.search("OK")==0){
				    lable.prev().html("running");
				    change_class_unclick(lable.children(".start_tomcat"));
					 change_class_click(lable.children(".stop_tomcat"));
           			alert(result);	
           		}else{
           			alert("服务器启动失败！");
           		}
			});
	  }
	  
	function stop_click(){
  			var app = $(this).parent("td").attr("app");
  			var lable= $(this).parent("td");

  			var url = "../../monitor/apps_manage.fm";	
			//组装请求参数
           	$.post(url, "host="+hostip+"&port="+mport+"&type=stop&path="+app, function(result){
				//console.log("result--->"+result);
				 stopresult=lable;
				 //console.log("change--->"+$(this).parent("td").prev());
				if(result.search("OK")==0){
					 lable.prev().html("stopped");
					 change_class_unclick(lable.children(".stop_tomcat"));
					 change_class_click(lable.children(".start_tomcat"));
           			alert(result);	
           		}else{
           			alert("服务器停止失败！");
           		}
			});
	}
	function change_class_unclick(selector){
		selector.removeClass("managemen_mon_info_sp");
		selector.addClass("managemen_mon_info_unclick");
	} 
	function change_class_click(selector){
		selector.removeClass("managemen_mon_info_unclick");
		selector.addClass("managemen_mon_info_sp");
	}   		
	function fun_onclick(selectorName){
		$("#"+selectorName).on("click",function () {
				if(selectorName==="createList"||selectorName==="createList2")
				$("#mainInfobox1").load("createList.jsp");
		});
	}
/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>
</head>
	<div id="top_cat">
		<div id="top_title">监控列表</div>	
	</div>
	<button class="create_btn" id="createList">创建新监控</button>
	<div class="c_search">
		<input type="text" placeholder="请输入监控服务器名进行模糊查询">
		<button>搜索</button>
	</div>
	
	
	<table  id="categoryContent"  >
		<thead>
			<tr class="category_htr">
				<th class="text-left width-15" style="margin-left:10px;">web程序</th>
				<th class="text-center width-15">sessions</th>
				<th class="text-center width-25">状态</th>
				<th class="text-center width-35">操作</th>
			</tr>
		</thead>
		<tbody id="webs_content">
		  <tr>
				<td class="text-center create" colspan="4" style="display:none;">
					部署应用？立即<a  id="createMonitor" >创建新web程序</a>
				</td>
			</tr>
		</tbody>
	</table>	
	<div id="page">
		<ul>
			<li><a href="">&laquo;</a></li>
			<li><a href="">&lsaquo;</a></li>
			<li class="selected"><a href="">1</a></li>
			<li><a href="">&rsaquo;</a></li>
			<li class="right"><a href="">&raquo;</a></li>
		</ul>
		<span>共有0条，每页显示10条</span>
	</div>
</html>