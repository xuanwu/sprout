<!DOCTYPE html >
<%@ page language="java" import="com.sprout.webapp.model.UserInfo,java.io.*,java.util.*" pageEncoding="UTF-8" %>
<%
String path = request.getContextPath();
String host = request.getParameter("host");
UserInfo user = (UserInfo) session.getAttribute("currentUser");
String username = user.getUsername();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<!-- <style text="text/css">

.servers_title {
margin-top: 10px;
margin-left: 0px;
text-align: left;
font-size: 20px;
line-height: 55px;
color: #ccc;
}
</style> -->	
<script>

$(document).ready(function () { //页面加载完成后事件
	 get_tomcat_data();
	 
});
	function get_tomcat_data(){
			var url = "../../monitor/apps_list.fm";	
			//组装请求参数
           	$.post(url, "type=tomcat", function(result){
				$("#categoryContent").append(result);
				$(".monitor_name").click(function(){
					var host = $(this).attr("host");
					var port = $(this).attr("port");
					$("#mainInfobox1").load("../monitor/appserver/serverInfo.jsp",{"host":host,"port":port});
				});
			
				$(".webs_monitor_button").click(function(){
					var host = $(this).attr("host");
					var realport = $(this).attr("realport");
					$("#mainInfobox1").load("../monitor/appserver/manage_apps.jsp",{"host":host,"port":realport});
				});
				$(".tomcat_monitor_button").click(function(){
					var host = $(this).attr("host");
					var port = $(this).attr("port");
					$("#mainInfobox1").load("../monitor/appserver/overall_performance.jsp",{"host":host,"port":port});
				});
				
				$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
		  		});
		  	
			  	$(".server_manager").mouseleave(function(){
			    		$(this).css("background-color","");
			  	});
				
			
				$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
		  		});
		  	
			  	$(".server_manager").mouseleave(function(){
			    		$(this).css("background-color","");
			  	});
				
			  	$(".createMonitor").click(function(){
					$("#mainInfobox1").load("../monitor/appserver/appserver_create.jsp");
			}); 
		  	
		});
}
</script>
</head>
	<div id="top_cat">
		<div id="top_title">服务器监控列表</div>	
	</div>
	<button class="create_btn" id="createMonitor">创建新监控</button>
	<div class="c_search">
		<input type="text" placeholder="请输入监控服务器名进行模糊查询">
		<button>搜索</button>
	</div>			
	<div class="_content" style="border-top:none;top:5px;">
	<table id="categoryContent">
	</table>
	<div id="page">
		<ul>
			<li><a href="">&laquo;</a></li>
			<li><a href="">&lsaquo;</a></li>
			<li class="selected"><a href="">1</a></li>
			<li><a href="">&rsaquo;</a></li>
			<li class="right"><a href="">&raquo;</a></li>
		</ul>
		<span>共有0条，每页显示10条</span>
	</div>
	</div>
</html>