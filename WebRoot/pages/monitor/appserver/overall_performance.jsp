<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String host = request.getParameter("host");
String port = request.getParameter("port");
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/tabs.css">
	<script type="text/javascript" src="<%=path%>/js/bootstrap.min.js"></script>

	<script>
		$("#chart").load("../tomcat/tomcat_view_cpu.jsp",{"host":"<%=host%>","port":"<%=port%>"});

		$("#mytabs a").click(function(e){
				
			$(this).tab("show");
			var str = this.id;
			var url="../tomcat/tomcat_view_" + str + ".jsp";
			$("#chart").load(url,{"host":"<%=host%>","port":"<%=port%>"});
		});
	</script>
<!-- <style type="text/css">
._content1{
	position: relative;
	top: 25px;
	width:100%;
	border-top:1px solid #cccccc;

}
</style> -->

</head>
<body>
	<div id="top_cat">
		<div id="top_title">Tomcat监控信息</div>
	</div>
	<a id="history_back" class="back_monitor"></a>
	<br>
	<div class="_content">
		<!-- break line -->
		<div class="popWindow_content">
			<div class="container">
				<ul id="mytabs" class="nav nav-pills" role="tablist">
					<li role="presentation" class="active"><a id="cpu">CPU监控</a></li>
					<li role="presentation"><a id="mem">内存监控</a></li>
					<li role="presentation"><a id="thread">线程监控</a></li>
					
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="chart"></div>			
				</div>
			</div>
		</div>
		
	</div>

</body>
</html>