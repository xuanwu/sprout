 <thead>
				<tr class="category_htr">
					<th class="text-center width-15">监控名称</th>
					<th class="text-center width-15">类型</th>
					<th class="text-center width-15">IP</th>
					<th class="text-center width-15">端口</th>
					<th class="text-center width-15">状态</th>
					<th class="text-right width-25" style="padding-right:150px;">监控操作</th>
				</tr>
			</thead>
			<tbody >
<#if result.success>
			<#assign lists=result.object>
			<#if lists?size &gt; 0>	
			<#list lists as node>	
			<tr class="managemen_mon_info server_manager">
				<td>
				<a class="server_host monitor_name" host="${node.destination}" port="${node.port}">
				<span class="managemen_mon_info_sp" style="margin-left:10px;" >
				${node.monitorname?default('null')}
				</span></a></td>
				<td>${node.type?default('null')}</td>
				<td>${node.destination?default('null')}</td>
				<td>${node.realport?default('0')}</td>
				<#if node.status==1>
					<td>运行</td>
				<#else>
					<td>未启动</td>
				</#if>
				<td align="center">
					<span class="managemen_mon_info_sp webs_monitor_button" host="${node.destination}" realport="${node.realport}" style="margin-left:10px;" >项目管理</span>
					<a class="server_host tomcat_monitor_button" host="${node.destination}" port="${node.port}"><span class="managemen_mon_info_sp" style="margin-left:10px;" >监控管理</span></a>
				</td>
			</tr>
			</#list>
			<#else>
			<tr>
			<td class="create" colspan="6">
					你还没有添加数据表，立即<a  id="createMonitor" >创建数据表</a>
			</td>
			</tr>
		</#if>
 </#if>
 </tbody>
