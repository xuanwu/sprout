<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>
$(document).ready(function () { //页面加载完成后事件

	$("#create_monitor").click(function(){
			// 节点加载
	    var nodeurl = "../../monitor/apps_create.fm";
		//组装请求参数
		var search = location.search; //获取url中"?"符后的字串
		var data = search.substr(1)+"&"+$("#create_form").serialize();
		console.log("data -->"+data);
	    $.post(nodeurl, data, function(result){
	    	var jsonResult = $.parseJSON(result);
	    	if(jsonResult.success==true){
	    		alert("添加成功");
	    		$("#mainInfobox1").load("../monitor/appserver/appserver_list.jsp");
	    	 }else{
	    	 	alert("添加失败");
	    	 }
		});
	});

	$(".back_monitor").click(function(){
		$("#mainInfobox1").load("tomcat_list.jsp");
	});
});
</script>
<style>
._content .addPro_table{
	margin:20px 0;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">创建新监控</div>
		<a id="history_back" class="back_monitor">返回监控列表</a>		
	</div>
	
	<div class="_content">
			<table class="addPro_table" >
			 <form id="create_form" method="post" action=" " >
					<tbody id="server_info">
					<tr>
						<td>
						监控地址:
						</td>
						<td colspan="5"><input name="path" type="text" placeholder="125.216.243.192"/></td>
					</tr>
					<tr>
						<td>
						监控名称:
						</td>
						<td colspan="5"><input name="name" type="text" placeholder="只能包含英文字母、数字、下划线，且只能以字母开头，长度小于128个字符"/></td>
					</tr>
					<tr>
						<td><em>*</em>
						监控端口:
						</td>
						<td colspan="5"><input name="port" type="text" placeholder="不能输入&lt;&gt;&quot;'\字符，最多1024个字符。"/></td>
					</tr>
					<tr>
						<td>
						监控频率:
						</td>
						<td ><input name="frequency" type="text" placeholder="输入数字"/>(秒)</td>
					</tr>
					<tr>
						<td>
						监控用户:
						</td>
						<td colspan="5"><input name="username" type="text" placeholder="输入username:password"/>格式:username:password</td>
					</tr>
					<tr>
						<td>
						监控类型:
						</td>
						<td ><input name="type" type="text" placeholder="tomcat"/></td>
					</tr>
					</tbody>
				</form>
			</table>
			
		<button class="addPro_button" id="create_monitor">创建</button>
		<button class="cancel_button back_monitor" >取消</button>
		
	
	</div>
</body>	
</html>