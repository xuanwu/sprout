<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String host = request.getParameter("host");
String port = request.getParameter("port");
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<%-- <script type="text/javascript" src="<%=path%>/js/creatPercentChar.js"></script> --%>

<script>                                                                               				 
			var chartTitle3='活跃线程(个)';
			var xAxisTitle ='时间';
			var yAxisTitle3='活跃线程(个)';
			var suffix3='个';
			var hostip = "<%=host%>";
			var mport = "<%=port%>";
			
$(document).ready(function () { 
				/*查看统计图 参数*/
			Highcharts.setOptions({                                                     
            global: {                                                               
                useUTC: false                                                       
            }
        	}); 
			var choosetime =1;
			getTomcatStatusThread(choosetime);			
		   
});

function selecttime(){
	var obj = document.getElementById('selectOption'); 
	var index = obj.selectedIndex; 
	var choosetime=0;
	if (index==0) {		
		choosetime=1;
		getTomcatStatusThread(choosetime);
	}
	if (index==1) {	
		choosetime =15;
		getTomcatStatusThread(choosetime);
	}
	if (index==2) {	
		choosetime =30;
		getTomcatStatusThread(choosetime);
	}
	
}

function getTomcatStatusThread(choosetime){
			var url = "../../monitor/apps_graphic.fm";
			 //组装请求参数

			var time1 = choosetime;
   	       	$.get(url, "host="+hostip+"&port="+mport+"&choosetime="+time1, function(result){
				 console.log(result);
				 var jsonResult = $.parseJSON(result);
				 var jsonData = jsonResult.object;
				 var data = [];
				 if(jsonData!=null||jsonData!=undefined){
					 for(var i=0; i <=jsonData.length-1; i++){
			 		  	 var time  = jsonData[i].time_current.slice(0);
			 		  	 var value = jsonData[i].alive_thread;
			 		  	 var date1 =new Date(Date.parse(time.replace(/-/g,   "/")));//转化成国际标准格式
			 		  	 var r = Date.UTC(date1.getFullYear(),date1.getMonth(),date1.getDate(),
	          				date1.getHours(),date1.getMinutes(),date1.getSeconds() );
			 		  	 data.push({                                                 
	                       x: r-8*60*60*1000,
	                       y: value                                        
	                   });
		 			}
					$("#showChartBox").show();
		 			creatPercentChart('#showChartBox', chartTitle3,xAxisTitle,yAxisTitle3,data);
		 			$("#noData").hide();
				 }
				 else{
					 $("#showChartBox").hide();
					 $("#noData").show();
				 }
			});
			
}

function creatPercentChart (boxID, chartTitle, xAxisTitle,yAxisTitil, data) {
    $(boxID).highcharts({
        chart: {
            //zoomType: 'x',
            //spacingRight: 20
        },
        title: {
            text: chartTitle
        },
        xAxis: {
        	title: {
                text: xAxisTitle
            },
        	tickPixelInterval: 150,//x轴上的间隔
            type: 'datetime' //定义x轴上日期的显示格式
        },
        yAxis: {
            title: {
                text: yAxisTitil
            }
        },
        tooltip: {
        	enabled: true,
            formatter: function() {
                return '<b>'+ this.series.name +': '+ this.y +' '+suffix3+'</b>'+'<br/>'+Highcharts.dateFormat('%Y-%m-%d %H:%M:%S',this.x) ;
            }
        },
        legend: {
        	enabled: false
        },
        credits: {
          	enabled: false
        },
        exporting: {                                                            
            enabled: false                                                      
        },  
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'area',
            name: '活跃线程数',
            turboThreshold:0, //不限制数据点个数
            data: (function() {                                                                                       
                var data1=data;                                              
                return data1;                                                   
            })()
        }]
    });			
}

</script>
<style>
._content .addPro_table{
	margin:20px 0;
}
.form-control{
	width: 70px;
  	height: 28px;
	float:right;
	padding: 2px;
	margin-right: 40px;
}
</style>
</head>
<body>
	<select id="selectOption" class="form-control" onchange="selecttime()">
		<option id="24">1天</option>
		<option id="15*24">15天</option>
		<option id="30*24">30天</option>
	</select>
	<p style="float:right;font-size: 14px;padding: 3px 5px;">最近<p>
	<div class="popWindow_content" >
			<div id="tomcat_status" class="chart_catalog">

		</div>
		<div id="showChartBox" style="margin:10px;margin-top:30px;">
		</div>
		<div id="noData" style="width:100%;height:100%;text-align:center;line-height:400px;display:none;">没有数据.</div>
	</div>
</body>	
</html>