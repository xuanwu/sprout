<thead>
				<tr class="category_htr">
					<th class="text-center width-15">监控点名称</th>
					<th class="text-center width-15">监控地址</th>
					<th class="text-center width-15">监控类型</th>
					<th class="text-center width-15">监控频率</th>
					<th class="text-center width-15">状态(响应时间)</th>
					<th class="text-right width-25" style="padding-right:110px;">监控操作</th>
				</tr>
			</thead>
			<tbody >
<#if result.success>
			<#assign lists=result.object>
			<#if lists?size &gt; 0>	
			<#list lists as node>	
			<tr class="managemen_mon_info server_manager">
				<td>${node.monitorname?default('null')}</td>
				<td>${node.destination?default('null')}:${node.port?default('null')}</td>
				<td>${node.type?default('null')}</td>
				<td>${node.frequency?default('0')} 分钟</td>
				<td class="monitor_status"><#if node.status==1>
					<font color="#090">正常</font>
				<#else>
					<font color="#FF0000">
					异常</font>
				</#if></td>
				<td align="center">
					<span class="managemen_mon_info_sp monitor_view" style="margin-left:20px;" monitorname="${node.monitorname}" destination="${node.destination}"  port="${node.port}" >监控状态</span>
					<a class="server_host monitor_warning" data="${node.id}"><span class="managemen_mon_info_sp" style="margin-left:10px;" >警告管理</span></a>
				</td>
			</tr>
			</#list>
			<#else>
			<tr>
				<td class="text-center create" colspan="4">
					暂无数据！
				</td>
			</tr>
		</#if>
 </#if>
 </tbody>