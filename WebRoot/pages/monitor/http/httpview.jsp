<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String destination = request.getParameter("destination");
String monitorname = request.getParameter("monitorname");
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<script type="text/javascript" src="<%=path%>/js/createChart.js"></script>
 	
<script>

			var Destination = "<%=destination%>";
			var Monitorname = "<%=monitorname%>";

			var legend1='响应时间'; //标注
			var legend2='状态';
			var chartTitle1='响应时间(毫秒)'; //统计图顶部标题
			var chartTitle2='状态';
			var yAxisTitle1='响应时间(毫秒)'; //左侧标题 
			var yAxisTitle2='状态';
			var xAxisTitle1='时间';//x轴标题
			var suffix1='毫秒';  //单位
			var suffix2='';
			var global_type=0;
/*查看统计图 参数*/
$(document).ready(function () { 
			global_type=0;
			createChartStatus(1);
			
			$("#mytabs a").click(function(e){
				
				$(this).tab("show");
				var str=this.id;
				if(str=="status"){
					global_type=0;
					createChartStatus(1);
				}else{
					global_type=1;
					createChartStatus(1);
				}				
				
			});
			
			$("#history_back").click(function(){
				$("#mainInfobox1").load("httplist.jsp");
			});
		
});

function selecttime(){
	var obj = document.getElementById('selectOption'); 
	var index = obj.selectedIndex; 
	var choosetime = 0;
	if (index==0) {		
		choosetime=1;
		createChartStatus(choosetime);
	}
	if (index==1) {	
		choosetime=15;
		createChartStatus(choosetime);
	}
	if (index==2) {	
		choosetime=30;	
		createChartStatus(choosetime);
	}
	
}

function createChartStatus(choosetime){
			var xAxisArray1 = new Array();
			var charData1 = new Array();
			var url = "../../monitor/graphic.fm";	
			$.get(url, "destination="+Destination+"&name="+Monitorname+"&type=http&choosetime="+choosetime, function(result){
				 console.log(result);
				 var jsonResult = $.parseJSON(result);
				 var jsonData = jsonResult.object;
				 if(jsonData!=null||jsonData!=undefined){
					 for(var i=0; i <=jsonData.length-1; i++){
		                var status = jsonData[i];
			 		  	xAxisArray1[i] = String(status.timestamp.substr(0));
		                if(global_type==0){	                	
			 		  		charData1[i] =Math.round(status.status*100)/100 ;	
		                }else{
			 		  		charData1[i] = status.avg_res_time;
		 		  		}
					 }

					 $("#showChartBox").show();
					 if(global_type==0){
						 createChart('#showChartBox',legend2,chartTitle2,xAxisArray1,charData1,yAxisTitle2,xAxisTitle1,suffix2);
					 }else{
						 createChart('#showChartBox',legend1,chartTitle1,xAxisArray1,charData1,yAxisTitle1,xAxisTitle1,suffix1);
					 }
		             $("#noData").hide();		
					
				 }else{
					 $("#showChartBox").hide();
					 $("#noData").show();
				 }
			});
}

</script>
<style>
/* ._content .addPro_table{
	margin:20px 0;
} */
.form-control{
	width: 70px;
  	height: 28px;
	float:right;
	padding: 2px;
	margin-right: 40px;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">HTTP监控信息</div>
	</div>
	<a id="history_back" class="back_monitor">返回数据表列表</a>
	<br>
	<div class="_content" >
	<select id="selectOption" class="form-control" onchange="selecttime()">
		<option id="24">1天</option>
		<option id="15*24">15天</option>
		<option id="30*24">30天</option>
	</select>
	<p style="float:right;font-size: 14px;padding: 3px 5px;">最近<p>
	<div class="popWindow_content" >
		<div class="container">
				<ul id="mytabs" class="nav nav-pills" role="tablist">
					<li role="presentation" class="active"><a id="status">状态</a></li>
					<li role="presentation"><a id="response">响应时间</a></li>				
				</ul>
		
		</div>
		
		<div id="showChartBox" style="margin:10px;margin-top:80px;">
		</div>
		<div id="noData" style="width:100%;height:100%;text-align:center;line-height:400px;display:none;">没有数据.</div>
	</div>
	</div>
		
</body>	
</html>