<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>
$(document).ready(function () { //页面加载完成后事件

	$("#create_monitor").click(function(){
			// 节点加载
	    var nodeurl = "../../monitor/httpcreate.fm";
		//组装请求参数
		var search = "125.216.243.192"; //获取url中"?"符后的字串
		var data = search+"&"+$("#create_form").serialize();
		console.log("data -->"+data);
	    $.post(nodeurl, data, function(result){
	    	var jsonResult = $.parseJSON(result);
	    	if(jsonResult.success==true){
	    		alert(jsonResult.message);
	    		$("#mainInfobox1").load("httplist.jsp");
	    	 }else{
	    	 	alert("添加失败");
	    	 }
		});
	});

	$(".back_monitor").click(function(){
		$("#mainInfobox1").load("httplist.jsp");
	});
});
</script>
<style>
._content .addPro_table{
	margin:20px 0;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">创建新监控</div>
		<a id="history_back" class="back_monitor">返回数据表列表</a>		
	</div>
	
	<div class="_content">
			<table class="addPro_table" >
			 <form id="create_form" method="post" action=" " >
					<tbody id="server_info">
					<tr>
						<td><em>*</em>
						监控名称:
						</td>
						<td colspan="5"><input name="name" type="text" placeholder="只能包含英文字母、数字、下划线，且只能以字母开头，长度小于128个字符"/></td>
					</tr>
					<tr>
						<td>
						监控地址:
						</td>
						<td colspan="5"><input name="path" type="text" placeholder="不能输入&lt;&gt;&quot;'\字符，最多1024个字符。"/></td>
					</tr>
					<tr>
						<td>
						监控频率:
						</td>
						<td ><input name="frequency" type="text" placeholder="输入数字"/></td>
					</tr>
					<tr>
						<td>
						监控方式:
						</td>
						<td ><input name="method" type="text" placeholder="输入post get"/></td>
					</tr>
					</tbody>
				</form>
			</table>
			
		<button class="webadd_button" id="create_monitor">创建</button>
		<button class="webcancel_button back_monitor" >取消</button>
		
	
	</div>
</body>	
</html>