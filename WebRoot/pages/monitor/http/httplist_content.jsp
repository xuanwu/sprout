
<#if result.success>
			<#assign lists=result.object>
			<#if lists?size &gt; 0>	
			<#list lists as node>	
			
			<tr class="managemen_mon_info server_manager">
				<td>${node.monitorname?default('null')}</td>
				<td>${node.destination?default('null')}</td>
				<td>${node.type?default('null')}: ${node.monitorMethod?default('')}</td>
				<td>${node.frequency?default('0')} 分钟</td>
				<td class="monitor_status" >
				<#if node.status==1>
					<font color="#090">正常</font>
				<#else>
					<font color="#FF0000">
					异常</font>
				</#if>
				</td>
				<td align="center">
					<a class="server_host monitor_view"  hostname="${node.destination}" monitorname="${node.monitorname}"><span class="managemen_mon_info_sp" style="margin-left:20px;" >监控状态</span></a>
					<a class="server_host monitor_warning" data="${node.id}" ><span class="managemen_mon_info_sp" style="margin-left:10px;" >警告管理</span></a>
				</td>
			</tr>
			 </#list>
				 </#if>
			<#else>
						
		<tr>
			<td class="text-center create" colspan="4">
				暂无数据！
			</td>
		</tr>
 </#if>