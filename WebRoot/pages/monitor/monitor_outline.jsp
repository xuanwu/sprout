<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/logOutline.css">
	<script type="text/javascript" src="<%=path%>/js/highcharts.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/tabs.css">
	<script type="text/javascript" src="<%=path%>/js/bootstrap.min.js"></script>
</head>
	
<body>

	
	<!--顶部app信息-->
        <div class="up" style="margin-top:2px;margin-left:15px;">
			<div class="left">
				<div class="app_name"><a id=" " href="index.jsp" target="_blank">project</a></div>
				<div class="log_title">站点监控</div>
			</div>	
        </div>
		
      <script>  
      var responeres;
      var request=null;
      var add_queue=null;
			$(document).ready(function () { //页面加载完成后事件

				$("#subnav_1").slideDown();
				$("#mainInfobox1").load("showall.jsp");
				
				$(".appnav").click(function(){
					var indexnav=$(".appnav").index(this);
					var indexbef = $("#lognav .appnav").index($(".appnav_selected"));
					console.log("nav-->"+indexnav+"  indexbef-->"+indexbef);
					if(indexnav != indexbef){
						$("#lognav li.appnav_selected").next(".subnav").slideUp();
						$("#lognav li.appnav_selected").removeClass("appnav_selected"); 
						$(this).addClass("appnav_selected");
					}
					$(this).next(".subnav").slideToggle();
				});
				$("#lognav .subnav_li").click(function(){
					if(add_queue){
						clearInterval(request);
						clearInterval(add_queue);
					}
					var index2=$(".subnav_li").index(this);
					$("li.subnav_selected").removeClass("subnav_selected"); 
						$(this).addClass("subnav_selected"); 
						switch(index2) {  
							case(0):
								$("#mainInfobox1").load("showall.jsp");break;//http监控列表
							case(1):
								$("#mainInfobox1").load("http/httplist.jsp");break;//http监控列表
							case(2):
								$("#mainInfobox1").load("ping/pinglist.jsp");break;//ping监控列表
							case(3): 
								$("#mainInfobox1").load("tcp/tcplist.jsp");break;//tcp监控列表
								break;
							case(4):  
								$("#mainInfobox1").load("udp/udplist.jsp");break;//udp监控列表
							case(5):  
								$("#mainInfobox1").load("synthesis/synthesis.jsp");break;//综合分析列表
							case(6):  
								$("#mainInfobox1").load("appserver/appserver_list.jsp");break;//应用服务器监控列表
								break;
							case(7):  
								$("#mainInfobox1").load("task/task_list.jsp");break;//进程监控列表
								break;
							case(8):  
								$("#mainInfobox1").load("warning/warning_list.jsp");break;//告警列表
								break;
							case(9): 
								$("#mainInfobox1").load("../users/userlist.jsp");break;//用户列表
								break;
						}
				});
			
			});
		</script>
		
		<div class="maincontent">
			<!--左边菜单栏-->
			<ul class="appMan_nav" id="lognav">
				<li id="lognav1" class="appnav appnav_selected" >网络协议监控</li>
				<ul id="subnav_1" class="subnav">
					<li class="subnav_li subnav_selected">所有监控</li>
					<li class="subnav_li">HTTP监控</li>
					<li class="subnav_li">PING监控</li>
					<li class="subnav_li">TCP监控</li>
					<li class="subnav_li">UDP监控</li>
					<li class="subnav_li">综合监控</li>
				</ul>
				<li id="lognav2" class="appnav">应用服务器监控</li>
				<ul id="subnav_2" class="subnav">
					<li class="subnav_li">监控列表</li>
				</ul>
				<li id="lognav3" class="appnav">服务进程监控</li>
				<ul id="subnav_3" class="subnav">
					<li class="subnav_li">监控概况</li>
				</ul>
				<li id="lognav5" class="appnav">警报管理</li>
				<ul id="subnav_5" class="subnav">
					<li class="subnav_li">警报概况</li>
<!-- 					<li class="subnav_li">服务器监控</li> -->
				</ul>
				<li id="lognav6" class="appnav" >用户管理</li>
				<ul id="subnav_6" class="subnav">
					<li class="subnav_li">用户列表</li>
				</ul>
			</ul>
			
			<!--右边详细信息-->
			<div class="maininfo">
				<!--应用详情界面-->
				<div id="mainInfobox1" >
					
				</div>

			</div>
		</div>   
		
</body>

</html>