<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	
	<script>

	$(document).ready(function () { //页面加载完成后事件
				var url = "../../monitor/showall.fm";	
				//组装请求参数
				var search = location.search; //获取url中"?"符后的字串
				var str = search.substr(1);
            	$.post(url, str, function(result){
					$("#all_content").append(result);
					
					$(".monitor_view").click(function(){
						var hostname = $(this).attr("hostname");
						var monitorname = $(this).attr("monitorname");
						var type = $(this).attr("type");
						if(type=="http"){
							$("#mainInfobox1").load("http/httpview.jsp",{"destination":hostname,"monitorname":monitorname});	
						}else if(type=="ping"){
							$("#mainInfobox1").load("ping/pingview.jsp",{"destination":hostname,"monitorname":monitorname});
						}else if(type=="tcp"){
							$("#mainInfobox1").load("tcp/tcpview.jsp",{"destination":hostname,"monitorname":monitorname});
						}else if(type=="udp"){
							$("#mainInfobox1").load("udp/udpview.jsp",{"destination":hostname,"monitorname":monitorname});
						}else{
							alert("Type not found!");
						}
					});
					
					$(".monitor_warning").click(function(){
						var data = $(this).attr("data");
						var warningurl = "../../monitor/findwarningbyId.fm";
						$.post(warningurl, str+"&type=http&respone_html=warning_list_type&id="+data, function(result){
							$("#mainInfobox1").children().remove();
							$("#mainInfobox1").html(result);
							$("#monitorid").val(data);
						}); 
					});
					$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
				  	});
				  	
				  	$(".server_manager").mouseleave(function(){
				    		$(this).css("background-color","");
				  	});
				});

	});
	function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}
	
</script>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">站点监控列表</div>	
	</div>
	
	<div class="_content" style="border-top:none;top:5px;">
		<table  id="categoryContent"  >
			<thead>
				<tr class="category_htr">
					<th class="text-center width-15">监控点名称</th>
					<th class="text-center width-15">监控地址</th>
					<th class="text-center width-15">监控类型</th>
					<th class="text-center width-15">监控频率</th>
					<th class="text-center width-15">状态(响应时间)</th>
					<th class="text-right width-25" style="padding-right:110px;">监控操作</th>
				</tr>
			</thead>
			<tbody id="all_content">
		
			</tbody>
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</body>

</html>