<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>


	$(document).ready(function () { //页面加载完成后事件
			var url = "../../monitor/webstatus.fm";	
			//组装请求参数
			var search = location.search; //获取url中"?"符后的字串
			var str = search.substr(1);
           	$.post(url,"type=tcp&responehtml=tcplist_content", function(result){
				$("#categoryContent").append(result);
				$(".monitor_view").click(function(){
						var hostname = $(this).attr("host");
						var nodeip = $(this).attr("nodeip");
						var monitorname = $(this).attr("monitorname");
						var port = $(this).attr("port");
						$("#mainInfobox1").load("tcpview.jsp",{"host":hostname,"nodeip":nodeip,"monitorname":monitorname,"port":port});
				});
				
				$(".monitor_warning").click(function(){
						var data = $(this).attr("data");
						var warningurl = "../../monitor/findwarningbyId.fm";
						$.post(warningurl,"type=tcp&respone_html=warning_list_type&id="+data, function(result){
							$("#mainInfobox1").children().remove();
							$("#mainInfobox1").html(result);
							$("#monitorid").val(data);
							$("#monitor_type").val("tcp");
							
						}); 
				});
				
				$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
			  	});
			  	
			  	$(".server_manager").mouseleave(function(){
			    		$(this).css("background-color","");
			  	});
			  	$(".monitor_status").css("color","#090");
			});
			
			$("#createMonitor").click(function(){
				$("#mainInfobox1").load("tcpCreate.jsp");
			});  
	});
	/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>
</head>
	<div id="top_cat">
		<div id="top_title">TCP监控列表</div>	
	</div>
	<button class="create_btn" id="createMonitor">创建新监控</button>
	<div class="_content" style="border-top:none;top:5px;">
		<table  id="categoryContent"  >
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</html>