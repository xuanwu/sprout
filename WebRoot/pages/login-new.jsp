<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>用户登录</title>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/reg_log.css">
</head>
<body>
     <div class="header">
	     <div class="header1">
		     <div class="header_title">
			     <div class="header_title_action">
				     <div style="border-bottom-left-radius:5px;" class="header_title_action_detail" onclick="location.href ='<%=path%>/pages/register.jsp'">
					 注册
					 </div>
					 <div id="login">
					     <div style="margin-left:-1px;border-bottom-right-radius:5px;" class="header_title_action_detail" onclick="location.href ='<%=path%>/pages/login-new.jsp'">
					     登录
					     </div>
					 </div>
				 </div>
			 </div>
			 <div class="headline">
				<img src="<%=path%>/images/heading.png"/>
			 </div>
		 </div>
	 </div>
     <div class="main" style="min-height:610px;">
	     <div class="maincontent">
		     <div class="mainlist_reg_log">
			     <div class="mainlist_reg_log_left">
			      <form action="<%=request.getContextPath()%>/logon.do" method="post" >
				     <div class="mainlist_reg_log_title">
					 登录
					 </div>
					 <div class="mainlist_reg_log_input" style="margin-top:20px">
					     <input class="mainlist_reg_log_input_item" type="text" placeholder="请输入您的邮箱" name="usercode" >
					 </div>
					 <div class="mainlist_reg_log_input_error">
					 </div>
					 <div class="mainlist_reg_log_input" style="margin-top:20px">
					     <div>
					     <input class="mainlist_reg_log_input_item" type="password" name="password" placeholder="请输入您的密码">
						 </div>
					 </div>
					 <div class="mainlist_reg_log_input_error">
					 </div>
					 <div style="margin-top:40px;">
						   <a style="color:#2C86C2;" href="">忘记密码 </a>
					 </div>
					 <button class="mainlist_reg_log_button" id="register" onclick="logon();">
					 登录
					 </button>
                     <div class="go_register_link" onclick="location.href ='<%=path%>/pages/register.jsp'">立即注册</div>
					 <div class="no_account">还没有账号</div>
					 </form><!--
					  <a href="http://rizhibao.com" name="rizhi085b84cb448d07432e576ebb58603f3dbao" >日志宝-在线日志分析平台</a>
 				--></div>
			 </div>
		 </div>
	 </div>
     <div class="home_footer">
     </div>
     
     <script>
		function logon() {
			targetForm=document.forms[0];
			//targetForm.action="${path}/logon.do";
			targetForm.submit();
		}
	</script>
	
	 </body>
</html>