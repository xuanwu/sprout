﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<script type="text/javascript" src="<%=path%>/js/app.js"></script>
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>
	
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>-->

</head>
	
<body>

	
	<!--顶部app信息-->
        <div class="up" style="margin-top:2px">
			<div class="left">
				<div class="app_icon"><img src="<%=path%>/images/app_icon.png" /></div>
				<div class="app_name"><a id="applink" href="" target="_blank">app</a></div>
			</div>	
			<div class="appcolumn">
				<div class="box_1">任务</div>
				<div class="progress_back">
					<div class="progress" style="width:220px">
						<span id="taskPercent" class="progress_bar"></span>
					</div>
				</div>
				<div id="tasks" class="box_1_1">(10/20个)</div>
			</div>
			<div id="appstate" class="power_button appon" onclick="changeState()"><img src="<%=path%>/images/on.png" /></div>
        </div>
		
     <script>  
	$(document).ready(function () { //页面加载完成后事件
	
				var url = "../application/view.fm";	
				//组装请求参数
				var search = location.search; //获取url中"?"符后的字串
				var str = search.substr(1);
				//console.log(url+"  \n"+str);
				
            	$.post(url, str, function(result){
            		//console.log("  \n"+result);
					$("#mainInfobox1").html(result);
					
					var appType = $("#app_type").val();
					//console.log("  \n"+appType);
					
					if(appType==0){
					$(".app_icon").children().attr("src","<%=path%>/images/mapreduceIcon.png");
					}
					else if(appType==1){
					$(".app_icon").children().attr("src","<%=path%>/images/sparkIcon.png");
					}
					else if(appType==2){
					$(".app_icon").children().attr("src","<%=path%>/images/stormIcon.png");
					}
					else if(appType==3){
					$(".app_icon").children().attr("src","<%=path%>/images/mahoutIcon.png");
					}
					else{
					$(".app_icon").children().attr("src","<%=path%>/images/javawebIcon.png");
					}
					
					var  app_appName= $("#app_appName").val();
					$("#applink").text(app_appName);
				});
				
	$("#app_selector li").each(function (index) {  //循环每个li，index表示循环当前li的索引
        $(this).click(function () {  //为li注册点击事件
            $("li.appnav_selected").removeClass("appnav_selected"); 
            $(this).addClass("appnav_selected");  
            if (index == 0) {  
            	var url = "../application/view.fm";	
				//组装请求参数
				var search = location.search; //获取url中"?"符后的字串
				var str = search.substr(1);
				//console.log(url+"  \n"+str);
            	$.post(url, str, function(result){
            		//console.log("  \n"+result);
					$("#mainInfobox1").html(result);
				});
            }
            else if (index == 1) {
                $("#mainInfobox1").load("service/servInfo.jsp"); //服务详情
            }
            else if (index == 2) {
                $("#mainInfobox1").load("home/variable.jsp"); //环境变量
            }
			else if (index==3){
				 $("#mainInfobox1").load("home/code.jsp");//代码管理
			}
			else if (index==4){
				 $("#mainInfobox1").load("instance/instance_home.jsp");//实例管理
			}else if (index==5){
				var url = "../task/history.fm";	
				//组装请求参数
				var search = location.search; //获取url中"?"符后的字串
				var str = search.substr(1);
				console.log(url+"  \n"+str);
            	$.post(url, str, function(result){
					$("#mainInfobox1").html(result);
				});
				 //$("#mainInfobox1").load("instance.jsp");//实例管理
			}
        });
    });
    
		
		//任务进度条的百分比
	var temp='50%';
	document.getElementById("taskPercent").style.width=temp;
	
	});
		</script>
		
		<div class="maincontent">
			<!--左边菜单栏-->
			<ul class="appMan_nav" id="app_selector">
				<li id="appnav1" class="appnav appnav_selected" >应用详情</li>
				<li id="appnav2" class="appnav" >服务详情</li>
				<li id="appnav3" class="appnav" >环境变量</li>
				<li id="appnav4" class="appnav" >代码管理</li>
				<li id="appnav5" class="appnav" >实例管理</li>
				<li id="appnav5" class="appnav" >历史实例管理</li>
			</ul>
			
			<!--右边详细信息-->
			<div class="maininfo">
				<!--应用详情界面-->
				<div id="mainInfobox1" >
				</div>
				
				

			</div>
		</div>   
		
</body>
</html>