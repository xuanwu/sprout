﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>


<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/project.css">
	

</head>
<style>
.cancel_button{
	float:right;
	display:block;
	background-color:#fff;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	height:38px;
	width:80px;
	line-height:38px;
    color: #666;
    cursor: pointer;
    font-size: 13px;
	margin:30px auto;
	margin-right:20px;
}
.addPro_button{
	float:right;
	height:38px;
	width:80px;
	line-height:38px;
}
#manageCat_form input{
	width:20px;
	vertical-align:middle;
}
.addPro_table{
	margin:0;
}
.btn_box{
	margin-top:20px;
}
</style>
<script>	
	function closeCategory(){
		$("#all").hide();
	}
</script>	
<body>
		<div class="popWindow_title">
			<div style="float:left">管理日志数据消费模式</div>
			<div style="float:right; cursor:pointer" onclick=closeCategory()>close</div>
		</div>
		<div class="popWindow_content" >
			<form id="manageCat_form" method="post" action=" " >
				<table class="addPro_table" style="margin-top:50px;">
					<tbody>
					<tr>
						<td><em>*</em>
						Category名称:
						</td>
						<td>sample</td>
					</tr>
					
					<tr>
						<td><em>*</em>
						选择日志数据消费模式:
						</td>
						<td>
							<input type="checkbox" checked="true" value=" " name=" "/>在线实时查询，保存7天
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<input type="checkbox" checked="true" value=" " name=" " onclick="if(this.checked==false)$('#attachInfo').css('visibility','hidden');else $('#attachInfo').css('visibility','visible');"/>离线归档到ODPS
						</td>
					</tr>
					<tr id="attachInfo">
						<td>
						</td>
						<td>
							离线归档数据源</br>
							ODPS项目名称：</br>
							ODPS表名称：
						</td>
					</tr>
					</tbody>
				</table>
				
			</form>
			<div class="btn_box">
				<button class="addPro_button" onclick="">确定</button>
				<button class="cancel_button" onclick=closeCategory()>取消</button>
			</div>
		</div>
</body>
</html>