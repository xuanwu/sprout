﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/logOutline.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>
	

</head>
	
<body>

	
	<!--顶部app信息-->
        <div class="up" style="margin-top:2px;margin-left:15px;">
			<div class="left">
				<div class="app_name"><a id=" " href="project.jsp" target="_blank">project</a></div>
				<div class="log_title">简单日志分析</div>
			</div>	
        </div>
		
        <script>  
			$(document).ready(function () { //页面加载完成后事件
				var flag=true;
				$("#mainInfobox1").load("category.jsp");
				
				$(".appnav").on("click",function () {  
					$("li.appnav_selected").removeClass("appnav_selected"); 
					$(this).addClass("appnav_selected"); 
					var index1=$(".appnav").index(this);
					if (index1 == 0) {  
						flag=true;
						$("#mainInfobox1").load("category.jsp"); //Category管理
					} else if (index1 == 1) {         //服务管理
						flag=!flag;
					}
					if(flag){ 
						$("#subnav_1").slideUp();
					}else{
						$("#subnav_1").slideDown();
					}	
				});
				
				$("#subnav_1").on("click", "li",function () {  //
					$("li.subnav_selected").removeClass("subnav_selected"); 
					$(this).addClass("subnav_selected"); 
					var index2=$(this).index();
					switch(index2) {  
						case("0"):
							$("#mainInfobox1").load(" ");break; //使用时长
						case("1"):
							$("#mainInfobox1").load(" "); break;//使用频率
						case("2"):
							$("#mainInfobox1").load(" "); break;//访问页面
						case("3"):
							$("#mainInfobox1").load(" ");break;//使用间隔
					}
				});
			
			});
		</script>
		
		<div class="maincontent">
			<!--左边菜单栏-->
			<ul class="appMan_nav" id="lognav">
				<li id="lognav1" class="appnav appnav_selected" >Category管理</li>
				<li id="lognav2" class="appnav" >服务管理</li>
				<ul id="subnav_1" class="subnav">
					<li class="subnav_li">使用时长</li>
					<li class="subnav_li">使用频率</li>
					<li class="subnav_li">访问页面</li>
					<li class="subnav_li">使用间隔</li>
				</ul>
			</ul>
			
			<!--右边详细信息-->
			<div class="maininfo">
				<!--应用详情界面-->
				<div id="mainInfobox1" >
				</div>
				
				

			</div>
		</div>   
		
</body>
</html>