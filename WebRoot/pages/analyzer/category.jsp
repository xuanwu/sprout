﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>


<script>


	$(document).ready(function () { //页面加载完成后事件
		fun_onclick("createConfig");
		fun_onclick("createCategory");
		fun_onclick("manageCat");
		fun_onclick("catName");
	});
	function fun_onclick(selectorName){
		$("#"+selectorName).on("click",function () {
			if(selectorName==="createConfig"){
				$("#mainInfobox1").load("config.jsp");
			}else if(selectorName==="createCategory"||selectorName==="manageCat"){
				var fileName=selectorName+".jsp";
				popWindows(fileName);
			}else if(selectorName==="catName"){
				$("#container").load("categoryDetail.jsp");
			}
		});
	}
	function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}
</script>
</head>
	<div class="search">
		<input type="text" placeholder="请输入Category名进行模糊查询">
		<button>搜索</button>
	</div>
	<button class="create_btn" onclick=popWindows("createCategory.jsp")>创建Category</button>
	
	<table  id="categoryContent"  >
		<thead>
			<tr class="category_htr">
				<th class="text-left width-25">Category</th>
				<th class="text-center width-50">Logtail状态</th>
				<th class="text-right width-25" style="padding-right:110px;">操作</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left" id="catName">sample</td>
				<td class="text-center">未应用</td>
				<td class="text-right">
					<span class="managemen_mon_info_sp" id="manageCat">管理</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" id="createConfig">创建配置</span>
					<span class="managemen_mon_info_sp" id="category_del" style="margin-left:10px;" onclick="">删除Category</span>
				</td>
			</tr>
			<tr>
					<td></td>
					<td class="text-center create">
						你还没有添加Category，立即<a  id="createCategory">创建Category</a>
					</td>
					<td></td>
				</tr>
		</tbody>
	</table>	
	<div id="page">
		<ul>
			<li><a href="">&laquo;</a></li>
			<li><a href="">&lsaquo;</a></li>
			<li class="selected"><a href="">1</a></li>
			<li><a href="">&rsaquo;</a></li>
			<li class="right"><a href="">&raquo;</a></li>
		</ul>
		<span>共有0条，每页显示10条</span>
	</div>
</html>