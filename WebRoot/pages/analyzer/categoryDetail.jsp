﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/categoryDetail.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<%=path%>/js/highcharts.js"></script>
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>

<script>

	
$(document).ready(function () { 
   /*查看统计图 参数*/
	var legend1='CPU使用量(百分比)'; //标注
	var chartTitle1='CPU使用量(百分比)'; //统计图顶部标题
	var xAxisArray1=new Array('SUN','MON', 'TUE', 'WED', 'THUR', 'FRI', 'SAT'); //横坐标
	var chartData1=new Array(7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2); //数据
	var yAxisTitle1=''; //左侧标题 
	var suffix1='%';  //单位
	
	createChart('#cat-chart',legend1,chartTitle1,xAxisArray1,chartData1,yAxisTitle1,suffix1);//生成统计图
});

</script>
</head>
	<div id="top_cat">
		<div id="top_title">sample</div>
		<a href="loghome.jsp">返回category列表</a>		
	</div>
	
	<div id="cat-content">
		<!-- 搜索 -->
		<div class="cat-search">
			<input type="text" placeholder="请输入关键字进行搜索">
			<select>
				<option value="1小时">1小时</option>
				<option value ="4小时">4小时</option>
				<option value="1天">1天</option>
				<option value="1周">1周</option>
			</select>
			<button>搜索</button>
		</div>
		
		<!-- 条形图 -->
		<div id="cat-chart">
			
		</div>
		
		<!-- 日志 -->
		<h5>匹配日志</h5>
		<table  id="cat-log"  >
			<thead> 
				<tr>
					<th>时间/IP</th><th>内容</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>14年9月20日</br>17时23分22秒</br></br>202.38.126.23</td> 
					<td>IP:<span>110.64.94.212</span></br>
						browser:<span>Mozilla/5.0</span></br>
						fromurl:<span>-</span></br>
						http:<span>HTTP/1.1</span></br>
						method:<span>GET</span></br>
						statu:<span>200</span></br>
						time:<span>25/Aug/2014:13:04:23</span></br>
						url:<span>/Running/invitedInfo</span></br>
					</td>
				</tr>
				<tr>
					<td>14年9月20日</br>17时23分22秒</br></br>202.38.126.23</td> 
					<td>IP:<span>110.64.94.212</span></br>
						browser:<span>Mozilla/5.0</span></br>
						fromurl:<span>-</span></br>
						http:<span>HTTP/1.1</span></br>
						method:<span>GET</span></br>
						statu:<span>200</span></br>
						time:<span>25/Aug/2014:13:04:23</span></br>
						url:<span>/Running/invitedInfo</span></br>
					</td>
				</tr>
			</tbody>
		</table>
		
		
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</html>