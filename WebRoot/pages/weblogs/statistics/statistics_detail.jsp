﻿<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../css/weblog/category_list.css">

</head>
<body>
	<div class="up1">
						<div class="servers_title">请求列表：</div>
	</div>
	<div>
	<table id="categoryContent">
		<thead>
			<tr class="category_htr" bgcolor="#E9E9E4">
					<th class="text-center width-15"  style="padding-left:30px;">请求</th>
					<th class="text-center width-15" >状态</th>
					<th class="text-center width-15" >方法</th>
					<th class="text-center width-15" >文件大小</th>
					<th class="text-center width-15">耗时</th> 
				</tr>
		</thead>
		<#if dymresult.success> 
			<#assign lists=dymresult.object>
		<tbody id='tbody1'>
			<#if lists?size &gt; 0> <#list lists as item>
				<tr class="managemen_mon_info server_manager">
				<td class="text-center" >${item.request?default('/')}</td>
				<td class="text-center" >${item.status?default('/')}</td>
				<td class="text-center" > ${item.method?default('/')}</td>
				<td class="text-center" > ${item.size?default('/')}</td>
				<td class="text-center" > ${item.resp_times?default('/')}</td>
			</tr>
			</#list>
			</#if>
		</tbody>
		<#else> error:${result.message}</#if>
	</table>
	</div>	

	<div class="compute_title">css列表：</div>
	<div id="nodelist_content">
			<table cellpadding="0" cellspacing="0" border="0" id="appContent">
					<thead>
						<tr class="category_htr" bgcolor="#E9E9E4">
								<th class="text-center width-15"  style="padding-left:30px;">请求</th>
								<th class="text-center width-15" >状态</th>
								<th class="text-center width-15" >方法</th>
								<th class="text-center width-15" >文件大小</th>
								<th class="text-center width-15">耗时</th> 
							</tr>
					</thead>
					<#if cssresult.success> 
						<#assign lists=cssresult.object>
					<tbody id='tbody1'>
						<#if lists?size &gt; 0> <#list lists as item>
							<tr class="managemen_mon_info server_manager">
							<td class="text-center" >${item.request?default('/')}</td>
							<td class="text-center" >${item.status?default('/')}</td>
							<td class="text-center" > ${item.method?default('/')}</td>
							<td class="text-center" > ${item.size?default('/')}</td>
							<td class="text-center" > ${item.resp_times?default('/')}</td>
						</tr>
						</#list>
						</#if>
					</tbody>
					</#if>
			</table>
	</div>
	<!-- navigation holder -->
	<div class="holder"></div>
	
</body>
</html>