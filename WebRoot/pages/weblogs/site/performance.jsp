<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	
<style type="text/css">
	.a1{
		font-size:14px; 
		text-align: center;
	}
	.a2{
		text-align: center;
		color:#FF9933;
		font-size:18px;
		font-weight:bold;
		margin:8px;  
	}
	.a22{
		text-align: center;
		color:#458B74;
		font-size:18px;
		font-weight:bold;
		margin:8px;  
	}
	.a3{
		text-align: center;		
		font-size:14px; 
		color:#009ACD;
	}
	.content{
		font-size:16px; 
	
	}
	#toptop{
		height:70px;
		text-align:center;
		
	}

	#top_title2{
		margin:0 15px 0 10px;
		font-size:16px;
		color:#333;
		text-align:center;
	}
	#top_title3{
		margin:0 15px 0 10px;
		font-size:18px;
		color:#ff0000;
		text-align:center;
	}
	#top_title4{
		font-weight:bold;
		margin:0 15px 0 10px;
		font-size:18px;
		color:#333;
		text-align:center;
	}

#customers
  {
  
  width:100%;
  border-collapse:collapse;
  font-size: 14px;
  }

#customers td, #customers th 
  {
  font-size:1em;
  border:0px solid #000;
  padding:8px 7px 8px 7px;
  
  }

#customers th 
  {
  font-size: 18px;
  text-align:left;
  padding-top:8px;
  padding-bottom:8px;
  background-color:#e9e9e9;
  color:#aaa;
  }

#customers tr.alt td 
  {
  color:#333;
  background-color:#eee;
  padding-top:8px;
  padding-bottom:8px;
  }
  
.width-10 {
	width: 10%;
}
.width-20 {
	width: 20%;
}
.width-40 {
	width: 40%;
}

</style>
<script type="text/javascript">
$(document).ready(function (){
	//$( "#tabs" ).tabs();
});	
	
</script>	

	
</head>
<body>
	<div id="top_cat">
		<div id="top_title">当日概览</div>	
	</div></br></br>
	<div id="toptop">
		<div id="top_cat">
			<div id="top_title4">76ms</div>
			<div id="top_title2">平均响应时间</div>
		</div>
		<div id="top_cat">
			<div id="top_title3">19491.00ms</div>
			<div id="top_title2">访问量</div>
		</div>
		<div id="top_cat">
			<div id="top_title4">13个</div>
			<div id="top_title2">域名个数</div>	
		</div>
		<div id="top_cat">
			<div id="top_title4">85个</div>	
			<div id="top_title2">css平均大小</div>	
		</div>
		<div id="top_cat">
			<div id="top_title4">1.05MB</div>
			<div id="top_title2">js平均大小</div>		
		</div>
		<div id="top_cat">
			<div id="top_title4">1.05MB</div>
			<div id="top_title2">images平均大小</div>		
		</div>
	</div>
	
	
  <div id="tabs-1">
    	<div class="_content" style="border-top:none;top:5px;">

		<table id="customers">
			<tr>
					<th class="text-left width-10">序号</th>
					<th class="text-left width-20">分析项</th>
					<th class="text-left width-10">数量</th>
					<th class="text-left width-40">存在问题</th>
					<th class="text-left width-20">优化建议</th>	
			</tr>
			
			<tr>
			<td>1</td>
			<td>HTTP请求</td>
			<td>28</td>
			<td>这个页面有20个JS文件	这个页面有3个样式表</td>
			<td>减少HTTP请求次数</td>
			</tr>
			
			<tr class="alt">
			<td>2</td>
			<td>DNS查询次数</td>
			<td>55</td>
			<td>静态资源分布的域名数超过4个</td>
			<td>减少DNS查询次数</td>
			</tr>
			
			<tr>
			<td>3</td>
			<td>URL重定向</td>
			<td>90</td>
			<td>存在1个URL跳转</td>
			<td>避免URL跳转</td>
			</tr>
			
			<tr class="alt">
			<td>4</td>
			<td>Ajax请求缓存</td>
			<td>100</td>
			<td>没有问题</td>
			<td>无需优化</td>
			</tr>
			
			<tr>
			<td>5</td>
			<td>DOM元素数量</td>
			<td>100</td>
			<td>没有问题</td>
			<td>无需优化</td>
			</tr>
			
			<tr class="alt">
			<td>6</td>
			<td>HTTP404错误</td>
			<td>100</td>
			<td>没有问题</td>
			<td>无需优化</td>
			</tr>
			
			
		</table>
	</div>
  </div>

</body>

</html>