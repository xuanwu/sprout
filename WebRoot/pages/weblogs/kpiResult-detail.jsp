<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
.analyse_result {
	padding: 5px;
	margin: 20px auto;
	background-color: #fff;
	color: #666;
	width: 640px;
	font-size: 13px;
}

.analyse_result em {
	color: red;
	font-size: 14px;
}

.analyse_result input {
	height: 28px;
	line-height: 28px;
	color: #999;
	padding: 1px;
	background-color: white;
	cursor: auto;
	width: 480px;
	border: #cccccc 1px solid;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	text-indent: 4px;
}

#analysis_box {
	border: 1px solid #ccc;
	width: 480px;
	padding-left: 8px;
	clear: left;
	float: right;
	margin-right: 20px;
}

#analysis_box ul {
	clear: both;
	float: right;
	width: 480px;
	list-style: none;
	margin: 0px;
}

#analysis_box input {
	width: 320px;
	margin-right: 0;
}

#analysis_box ul li.tr_value {
	width: 240px;
	float: left;
	text-align: left;
}

.analyse_result_title {
	float: left;
	height: 28px;
	line-height: 28px;
	width: 140px;
}

.addPro_button {
	float: left;
}

#analysis_box ul {
border-bottom: 1px solid #e9e9e9;
}

</style>

</head>
<body>
	<div class="analyse_result">
		<div class="analyse_result_title">
			<em>&nbsp;&nbsp;&nbsp;&nbsp;*</em>&nbsp;解析结果:
		</div>
		<div class="analyse_result_title">标识</div>
		<div class="analyse_result_title">取值</div>

		<div id="analysis_box" class="container">
			<#assign lists=result> 
			<#list lists as app>
			<ul>
				<li>客户端IP地址</li>
				<li class="tr_value">：${(app.remote_addr)!}</li>
			</ul>
			<#if app.remote_user?exists >
			<ul>
			<li>客户端用户名称</li>
			<li class="tr_value">：${(app.remote_user)!}</li>
			</ul>
			</#if>
			<#if app.time_local?exists >
			<ul>
			<li>访问时间与时区</li>
			<li class="tr_value">：${(app.time_local)!}</li>
			</ul>
			</#if>
			<#if app.request?exists >
			<ul>
			<li>请求的URL与HTTP协议</li>
			<li class="tr_value">：${(app.request)!}</li>
			</ul>
			</#if>
			<#if app.status?exists >
			<ul>
			<li>请求状态</li>
			<li>：${(app.status)!}</li>
			</ul>
			</#if>
			<#if app.body_bytes_sent?exists >
			<ul>
			<li>文件内容大小</li>
			<li>：${(app.body_bytes_sent)!}</li>
			</ul>
			</#if>
			<#if app.http_referer?exists >
			<ul>
			<li>页面链接</li>
			<li>：${(app.http_referer)!}</li>
			</ul>
			</#if>
			<#if app.http_user_agent?exists >
			<ul>
			<li>客户浏览器</li>
			<li>：${(app.http_user_agent)!}</li>
			</ul>
			</#if>
			<#if app.method?exists >
			<ul>
			<li>请求方法</li>
			<li>：${(app.method)!}</li>
			</ul>
			</#if>
			<#if app.http_version?exists >
			<ul>
			<li>HTTP版本</li>
			<li>：${(app.http_version)!}</li>
			</ul>
			</#if>
			</#list>

		</div>
	</div>
</body>
</html>