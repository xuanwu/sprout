<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
   
	<script type="text/javascript" src="<%=path%>/js/jquery-1.11.0.min.js"></script>
 	<script type="text/javascript" src="<%=path%>/js/weblog/weblog.js"></script>
<style type="text/css">
	.log_title{
	margin-top: 13px;
	margin-left:100px;
	height: 70px;
	float: left;
	text-align: center;
	font-size: 24px;
	line-height: 65px;
	color:#ccc;
}
</style>
</head>
<body>
    <div class="header">
		<div class="header1">
			<div class="headline">
				<img src="<%=path%>/images/heading.png"/>
			</div>
			<ul id="nav" style="padding:0 40px;">
				<li><a href="<%=path%>/pages/index.jsp">应用列表</a></li>
				<li><a href="<%=path%>/pages/server.jsp">服务列表</a></li>
			</ul>
		
			<ul class="upright">
				<!--登出-->
				<li id="logout"><a href="login-new.jsp">退出登录</a></li>
				
				<!--用户登录信息-->
				<li id="login_info">username</li>
			</ul>
	</div>
    </div>
    <div class="container">
        <div class="up">
        	<div class="log_title">简单日志分析</div>
        </div>
    </div>
		
	<div class="create_content">
		  		<div class="create_content_illustration">请选择应用类型</div>	
		  		 
                <div id="serviceTypes" class="create_content_1">
				     <div class="create_content_box create_content_selected" id="appIcon1" onclick=appIconOnclick(1)>
					      <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/mapreduceIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">MapReduce</div>
					 </div>
					 <div class="create_content_box" id="appIcon2" onclick=appIconOnclick(2)>
					      <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/sparkIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">Spark</div>
					 </div>
					 <div class="create_content_box" id="appIcon3" onclick=appIconOnclick(3)>
					     <div class="create_content_box_ser_bk" >
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/stormIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">Storm</div>
					 </div>
					 <div class="create_content_box" id="appIcon4" onclick=appIconOnclick(4)>
					     <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/mahoutIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">Mahout</div>
					 </div>
					<div class="create_content_box" id="appIcon5" onclick=appIconOnclick(5)>
					     <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/javawebIcon.png) center no-repeat"></div>
						 </div>
						<div class="managemen_ser_word">Java_Web</div>
					 </div>
					 <div class="create_content_box" id="appIcon6" onclick=appIconOnclick(6)>
					      <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/weblog.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">weblog</div>
					 </div>
				</div>
				<form action="" method="post" id="app_create_form" >
                <div class="create_content_2">
			         <div class="create_content_url_left">
						 <input type="text" id="appName" name="appName" style="padding-left:5px;line-height:30px;color:#999;" placeholder="服务名称" size="40"/>
						 
						 <input id="app_type" name="appType" type="hidden" value="5"/>
					 </div>     
			        	<div class="create_content_button" onclick="" id="createApplication">确认创建应用</div>
			   </div>
			   </form>
	</div>
	<div class="footer">
		<div class="footer_Info">
			  <a href="<%=path%>/">联系我们</a></li>
		</div >
		<div class="footer_Info">
			  <a href="<%=path%>/">about sprout</a></li>
		</div >
	</div>
</body>
</html>