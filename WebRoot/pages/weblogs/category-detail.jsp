﻿<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../../js/showChart.js"></script>
<link rel="stylesheet" href="../../css/weblog/category_list.css">

<script src='../../js/category/categoryOperate.js'></script>
<script>
	/*查看统计图 参数*/
		var legend1='时间'; //标注
		var chartTitle='日志采集统计';
		var xAxisArray1 = new Array();
		var recordNumData = new Array();
		var yAxisTitle1='采集数'; //左侧标题 
		var suffix1='条';  //单位
		var request = null;
		var add_queue = null;
$(document).ready(function () { 
		var first = $($(".category_id")[0]).val();	
		if(first!=null){
			var url = "../../category/record_view.do";
   	 		$.get(url, "id="+first, function(result){
	   			 // console.log("result"+result.object);
	   			  var jsonResult = $.parseJSON(result);
	   	 		  var jsonData = jsonResult.object;
	   	 		  console.log(jsonData);
	   
	   	 		  for(var i=0; i < jsonData.length; i++){
	   	 		  	 //status = jsonData[i];
	   	 		  	 xAxisArray1[i] = jsonData[i].timestamp.slice(5,10);
	   	 		  	 recordNumData[i] = jsonData[i].record_num;
	   	 		  }
   	 			 createChart('#showChartBox',legend1,chartTitle,xAxisArray1,recordNumData,yAxisTitle1,suffix1); 	
   			 });
		}
		
		$(".category_view").click(function(){
			var logid= $(this).attr("value");
			clickInstance(logid);
		});
});

function clickInstance(logid){
	var url = "../../category/record_view.do";
	 	$.get(url, "id="+logid, function(result){
			 // console.log("result"+result.object);
			  var jsonResult = $.parseJSON(result);
	 		  var jsonData = jsonResult.object;
	 		  console.log(jsonData);

	 		  for(var i=0; i < jsonData.length; i++){
	 		  	 //status = jsonData[i];
	 		  	 xAxisArray1[i] = jsonData[i].timestamp.slice(5,10);
	 		  	 recordNumData[i] = jsonData[i].record_num;
	 		  }
	 		createChart('#showChartBox',legend1,chartTitle,xAxisArray1,recordNumData,yAxisTitle1,suffix1); 	
	 });	
}
 
  </script>

</head>
<body>
	
	<div>
	<table id="categoryContent">
		<thead>
			<tr class="category_htr">
					<th class="text-center width-15"  style="padding-left:30px;">名称</th>
					<th class="text-center width-15" >监控路径</th>
					<th class="text-center width-15" >服务器</th>
					<th class="text-center width-15">状态</th> 
					<th class="text-center width-15" >配置</th>
					<th class="text-center width-15">操作</th>
				</tr>
		</thead>
		<#if result.success> 
			<#assign lists=result.object>
		<tbody id='tbody1'>
			<#if lists?size &gt; 0> <#list lists as category>
				<tr class="managemen_mon_info server_manager" cid="${category.id?default('0')}">
				<input type="hidden" class="category_id" value="${category.id?default('0')}">
				<td class="text-center"  style="padding-left:30px;">
					<span class="managemen_mon_info_sp">
						<a class="category_view" value="${category.id?default('0')}">${category.name}</a>
					</span>
				</td>
				<td class="text-center" >${category.logpath?default('/')}</td>
				<td class="text-center" > ${category.ip?default('/')}</td>
				<#if category.status==1>
					<td>启动</td>
					<td class="text-center" >
					<span ><a onclick=createCategoryConfigure(${category.id},'${category.name}')>修改配置</a></span>
				</td>
				<#else>
					<td>未启动</td>
					<td class="text-center" >
					<span class="managemen_mon_info_sp"><a onclick=createCategoryConfigure(${category.id},'${category.name}')>创建配置</a></span>
				</td>
				</#if>
				
				<td class="text-center">
					<span class="managemen_mon_info_sp" ><a >启动</a></span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" onclick=deleteCategory(${category.id})><a >删除</a></span> 
				</td>
				<!--	 
					<div id='category_btn_${category.id}' class='category_btn_right' onclick=startCategory(${category.id}) <#if category.status==0> style="background-image:url('../../images/off.png');" <#else> style="background-image:url('../../images/on.png');" </#if> ></div>
			 -->
			</tr>
			</#list>

			<#else>
			<tr>
					<td></td>
					<td class="text-center create">
						你还没有添加Category，立即<a  onclick=createCategory()>创建Category</a>
					</td>
					<td></td>
				</tr>
			</#if>
		</tbody>
		<#else> error:${result.message}</#if>
	</table>
	</div>	

				 <div id="showChartBox" style="margin:10px;margin-top:40px;">
				</div>

	<!-- navigation holder -->
	<div class="holder"></div>
	
</body>
</html>