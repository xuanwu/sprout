<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
</style>

<script>
$(document).ready(function () { 

	var url = "../../weblogs/time.do";	
    $.post(url,"", function(result){
        var jsonResult = $.parseJSON(result);
		
		var legend2='IP流量统计'; 
		var chartTitle2='访问量统计';
		var xAxisArray2=jsonResult.object.name;
		var chartData1=jsonResult.object.data;
		var chartData2=jsonResult.object.ip;
		var max=jsonResult.object.max;
		var yAxisTitle2=''; 
		var suffix2='天'; 
		createChartForCategory('#showBottomChartBox2',legend2,chartTitle2,xAxisArray2,chartData1,chartData2,yAxisTitle2,suffix2);                    
	});
});

function createChartForCategory(boxID,legend2,chartTitle2,xAxisArray2,chartData1,chartData2,yAxisTitle2,suffix2){
	$(function () {                                                                
	    $(boxID).highcharts({  
	        chart: {
	            zoomType: 'x',
	            spacingRight: 20
	        },
	        title: {
	            text: chartTitle2
	        },
	        xAxis: {
	        	categories: xAxisArray2
	        },
	        yAxis: {
	            title: {
	                text: yAxisTitle2
	            }
	        },
	        legend: {
	            enabled: false
	        },
	        tooltip: {
	            enabled: false,
	            formatter: function() {
	                return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+ this.y;
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {
	                    enabled: true
	                },
	                enableMouseTracking: true
	            }
	        },
	        series: [{
	        	name: legend1,
	            data: chartData1
	            
	        },{
	        	name: '真实IP统计',
	            data: chartData2
	        }]
	    	/* chart: {
	            type: 'column'
	        },
	        title: {
	            text: chartTitle2
	        },
	        xAxis: {
	        	categories: xAxisArray2
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: yAxisTitle2
	            }
	        },
	        tooltip: {
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} 天</b></td></tr>',
	            
	
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: legend2,
	            data: chartData2

	        }] */
	    });                                                                  
	}); 
}

</script>
<head>

<body>
	
				<div id="showBottomChartBox2" style="margin:10px;margin-top:20px;min-width:700px;height:400px">

				</div>
</body>
</html>