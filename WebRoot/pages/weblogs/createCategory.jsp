﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/weblog/project.css">


</head>
<style>
.cancel_button {
	float: right;
	display: block;
	background-color: #fff;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	height: 38px;
	width: 80px;
	line-height: 38px;
	color: #666;
	cursor: pointer;
	font-size: 13px;
	margin: 20px auto;
	margin-right: 20px;
}

.addPro_button {
	float: right;
	height: 38px;
	width: 80px;
	line-height: 38px;
}
</style>
<script>
	
	function closeCategory(){
		$("#all").hide();
	}
	
	function addCategory(){
		var form1 = document.getElementById('createCat_form');
		var categoryNameInput = document.getElementById('categoryname');
		if(categoryNameInput.value == '' || categoryNameInput.value == null){
			alert('请输入category名称!');
		}
		else{
			//设置username的隐藏域为当前登录的id
			document.getElementById('appId').value = window.location.search.substring(1, window.location.search.length);
			form1.submit();
		}
	}
</script>
<body>
	<div class="popWindow_title">
		<div style="float: left">创建新日志监控</div>
		<div style="float: right; cursor: pointer" onclick=closeCategory()>close</div>
	</div>
	<div class="popWindow_content">
		<form id="createCat_form" method="post" action="<%=request.getContextPath()%>/category/add.do" >
			<table class="addPro_table" style="margin-top: 10px;">
				<tbody>
					<tr>
						<td><em>*</em> 监控名称:</td>
						<td><input name="categoryname" type="text" id='categoryname'/></td>
					</tr>

					<tr>
						<td><em>*</em> 日志类型:</td>
						<td><select name="categorytype" style="width: 130">
								<option value="0">tomcat访问日志</option>
								<option value="1">nginx访问日志</option>
								<option value="2">进程日志</option>
								<option value="3">系统日志</option>
								<option value="4">hadoop日志</option>
						</select>当前系统支持日志的监控类型</td>
					</tr>
					<td hidden="hidden" name='appId'>
						<input name='appId' id='appId' type='text'>
					</td>
				</tbody>
			</table>

		</form>
		<div class="btn_box">
			<button class="addPro_button" onclick=addCategory()>创建</button>
			<button class="cancel_button" onclick=closeCategory()>取消</button>
		</div>
	</div>
</body>


</html>