﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/logOutline.css">
</head>
	
<body>

	
	<!--顶部app信息-->
        <div class="up" style="margin-top:2px;margin-left:15px;">
			<div class="left">
				<div class="app_name"><a id=" " href="project.jsp" target="_blank">project</a></div>
				<div class="log_title">日志监控与分析</div>
			</div>	
        </div>
		
        <script>  
			$(document).ready(function () { //页面加载完成后事件
				var flag=true;
				$("#mainInfobox1").load("category.jsp");
				
				$(".appnav").click(function(){
					var indexnav=$(".appnav").index(this);
					var indexbef = $("#lognav .appnav").index($(".appnav_selected"));
					console.log("nav-->"+indexnav+"  indexbef-->"+indexbef);
					if(indexnav != indexbef){
						$("#lognav li.appnav_selected").next(".subnav").slideUp();
						$("#lognav li.appnav_selected").removeClass("appnav_selected"); 
						$(this).addClass("appnav_selected");
					}
					$(this).next(".subnav").slideToggle();
				});
				$("#lognav .subnav_li").click(function(){
					var index2=$(".subnav_li").index(this);
					console.log("index2------>"+index2);
					$("li.subnav_selected").removeClass("subnav_selected"); 
						$(this).addClass("subnav_selected"); 
						switch(index2) {  
							case(0):
								$("#mainInfobox1").load("category.jsp"); break;
							case(1):
								$("#mainInfobox1").load("search/log_search.jsp");break;//查看作业
							case(2):  
								$("#mainInfobox1").load("chart_pv.jsp");break;
							case(3):  
						    	$("#mainInfobox1").load("chart_ip.jsp");break;
							case(4):  
								$("#mainInfobox1").load("chart_browser.jsp");break;
							case(5):  
						    	$("#mainInfobox1").load("chart_resources.jsp");break;
						    case(6):
						    	$("#mainInfobox1").load("statistics/statistics.jsp");
						    	break;
						     case(7):
						    	$("#mainInfobox1").load("site/performance.jsp");
								break;
						    case(8):
						    	$("#mainInfobox1").load("error/warninglist.jsp");
								break;
						}
				});
				
			});
		</script>
		
		<div class="maincontent">
			<!--左边菜单栏-->
			<ul class="appMan_nav" id="lognav">
				<li id="lognav1" class="appnav appnav_selected" >日志管理</li>
				<ul id="subnav_1" class="subnav">
					<li class="subnav_li subnav_selected" >任务列表</li>
					<li class="subnav_li">日志查询</li>
				</ul>
				<li id="lognav2" class="appnav" >日志分析</li>
				<ul id="subnav_2" class="subnav">
					<li class="subnav_li">页面浏览量</li>
					<li class="subnav_li">IP流量统计</li>
					<li class="subnav_li">浏览器统计</li>
					<li class="subnav_li">网页资源浏览</li>
					<li class="subnav_li">网页性能统计</li>
					<li class="subnav_li">概括统计</li>
				</ul>
				<li id="lognav3" class="appnav" >异常分析</li>
				<ul id="subnav_3" class="subnav">
					<li class="subnav_li">异常列表</li>
					
				</ul>
			</ul>
			
			<!--右边详细信息-->
			<div class="maininfo">
				<!--应用详情界面-->
				<div id="mainInfobox1" >
				</div>
			</div>
		</div>   
		
</body>
</html>