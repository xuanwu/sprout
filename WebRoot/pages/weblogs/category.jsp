﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/category.css">

	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>

$(document).ready(function () { //页面加载完成后事件
	var url = "../../category/category.fm";	
	//组装请求参数
	$.post(url, null, function(result){
			$("#categoryContent").html(result);
			$(".server_manager").mouseenter(function(){
    		    $(this).css("background-color","#E9E9E4");
	  		});
	  	
		  	$(".server_manager").mouseleave(function(){
		    		$(this).css("background-color","");
		  	});
		  	$(".category_view").click(function(){
    		      var cid = $(this).attr("cid");
    		      console.log("cid-->"+cid);
    		      $("#mainInfobox1").load("logs/loglist.jsp");
    		      $("#cid").val(cid);
    		      
	  		});
	});
});

//创建category
function createCategory(){
	$("#categoryWindow").load("createCategory.jsp");
	$("#all").show();
}
function config(){
	$("#mainInfobox1").load("config.jsp");
}

function fuzzyQuery(){
	var url = "../../category/fuzzyQuery.fm";	
	//组装请求参数
//		var data = $("#id").val();	
	var fuzzyQueryKey = document.getElementById('fuzzyQueryKey').value;
	$.post(url, {
		'fuzzyQueryKey' : fuzzyQueryKey,
		'appId' : window.location.search
		
	}, function(result){
		$("#categoryContent").html(result);
	});
}
</script>
</head>
<div id="search">
	<input id='fuzzyQueryKey' type="text" placeholder="请输入监控名或类型进行模糊查询">
	<button onClick=fuzzyQuery()>搜索</button>
</div>

<button class="create_btn" onclick=createCategory()>创建新日志监控</button>

<div class="container" id="categoryContent"></div>

</html>