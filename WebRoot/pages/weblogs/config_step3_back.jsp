﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/config.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/project.css">
</head>

<script>	
	$(document).ready(function () { 
		
		document.getElementById('textAreaIp').value = '';//初始化IP输入框为空
		$("#lastBtn3").on("click",function(){
			$("#process3").removeClass("processing");
			$("#pro_border2").removeClass("process_blue");
			$("#pro_border2").addClass("process_gray");
			$("#step3_content").hide();
			$("#step2_content").show();
		});
		
		$("#nextBtn3").on("click",function(){
			
			//检测IP地址合法性
			var IPre = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
			
			var IPreInput = document.getElementById('textAreaIp').value;
			
			var IPreInputWithOutBr = IPreInput.split('\n');
			
			var validateIP = true;//判断所有的用空格隔开的IP是否为IP 空行不进行判断
			
			for(var i = 0; i < IPreInputWithOutBr.length;i++){
				if(IPreInputWithOutBr[i] != '')
					if(IPre.test(IPreInputWithOutBr[i])){
						
					}
					else
						validateIP = false;
			}
			//验证成功则post提交
			if(document.getElementById('inputMachineName').value != '' && validateIP){
				//名字输入非空，并且判断IP合法，则提交
				var url_1 = "../../category/updateCategoryConf.do";
				$.post(url_1, 
					{
					'updateId' : $("#category_Conf_Id").html(),
					'IPreInput' : IPreInput,
					'logPath' : logPath,
					'sampleLogs' : sampleLogs,
					're' : re
					}
					, function(result) {
					if(result == 'true'){
						alert('配置成功,点击返回！');
						window.location.href='./loghome.jsp' + window.location.search;
					}
					else
						alert('配置失败');
				});
			}
			else{
				alert('输入有误');
			}
			
		});
	});


</script>	

		<form id="step3_form" method="post" action=" " >
			<table class="addPro_table">	
				<tbody>
				<tr>
					<td >
						<em>*</em>
						机器组名称:
					</td>
					<td>
						<input id='inputMachineName' name="machinename" type="text" />
					</td>
						
				</tr>
				
				<tr>
					<td style="vertical-align:top;">
						<em>*</em>
						IP地址:
					</td>
					<td>
						<textarea id='textAreaIp' rows="15" >
						</textarea>
					</td>
				</tr>
				
				<tr >
					<td></td>
					<td style="margin-top:-10px">
					1、多个IP请用换行分割
					</td>
				</tr>
				</tbody>
			</table>
			
		</form>
		<div class="btnbox2">
			<button class="cancel_button" id="cancelBtn3" onClick=return_back() >取消</button>
			<button class="addPro_button" id="lastBtn3">上一步</button>
			<button class="addPro_button" id="nextBtn3">完成配置</button>
		</div>
</html>