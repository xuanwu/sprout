<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/webLog.css">
   
 	<script type="text/javascript" src="<%=path%>/js/app.js"></script>

</head>
<body>
    <div class="create_content">
    <div id="top">
        <button>创建Category</button>
    </div>
    <div id="search">
        <input type="text" placeholder="请输入Category名进行模糊查询">
        <button>搜索</button>
    </div>
    <table>
        <thead>
            <tr>
                <th class="text-left width-25">Category</th>
                <th class="text-center width-50">Logtal状态</th>
                <th class="text-right width-25">操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td class="text-center create">
                    你还没有添加Category，立即<a href="#">创建Category</a>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <div id="page">
        <ul>
            <li><a href="">&laquo;</a></li>
            <li><a href="">&lsaquo;</a></li>
            <li class="selected"><a href="">1</a></li>
            <li><a href="">&rsaquo;</a></li>
            <li class="right"><a href="">&raquo;</a></li>
        </ul>
        <span>共有0条，每页显示10条</span>
    </div>
    </div>
</body>
</html>