<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/showchart.css">
	<script type="text/javascript" src="<%=path%>/js/showChart.js"></script>
	
	
<style text="text/css">
.create_content_button
a{
	text-decoration:none;
	color:#ffffff;
}
</style>

<script>
$(document).ready(function () { 

	var url = "../../weblogs/browser.do";	
    $.post(url,"", function(result){
        var jsonResult = $.parseJSON(result);
		
		var legend2='浏览器统计';
		var chartTitle2='浏览器统计';
		var xAxisArray2=jsonResult.object.name;
		var chartData2=jsonResult.object.data;
		var max=jsonResult.object.max;
		var yAxisTitle2=''; 
		var suffix2='次'; 
		createChartForCategory('#showBottomChartBox2',legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2);                    
	});
});

function createChartForCategory(boxID,legend2,chartTitle2,xAxisArray2,chartData2,yAxisTitle2,suffix2){
	$(function () {                                                                
	    $(boxID).highcharts({                                           
	    	chart: {
	            type: 'column'
	        },
	        title: {
	            text: chartTitle2
	        },
	        xAxis: {
	        	categories: xAxisArray2
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: yAxisTitle2
	            }
	        },
	        tooltip: {
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} 次</b></td></tr>',
	            
	
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [{
	            name: legend2,
	            data: chartData2

	        }]
	    });                                                                  
	}); 
}

</script>
<head>

<body>
	
				<div id="showBottomChartBox2" style="margin:10px;margin-top:20px;min-width:700px;height:400px">

				</div>
</body>
</html>