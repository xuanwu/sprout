<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
   <link rel="stylesheet" type="text/css" href="<%=path%>/css/webLog.css">
   
 	<script type="text/javascript" src="<%=path%>/js/app.js"></script>

</head>
<body>
    <div class="header">
		<div class="header1">
			<div class="headline">
				<img src="<%=path%>/images/heading.png"/>
			</div>
			<ul id="nav" style="padding:0 40px;">
				<li><a href="<%=path%>/pages/index.jsp">应用列表</a></li>
				<li><a href="<%=path%>/pages/server.jsp">服务列表</a></li>
			</ul>
		
			<ul class="upright">
				<!--登出-->
				<li id="logout"><a href="login-new.jsp">退出登录</a></li>
				
				<!--用户登录信息-->
				<li id="login_info">username</li>
			</ul>
	</div>
    </div>
    <div class="container">
        <div class="up">
        </div>
    </div>
    <div class="create_content">
    <div id="top">
        <h2>Catelory列表</h2>
        <button>创建Category</button>
    </div>
    <div id="search">
        <input type="text" placeholder="请输入Category名进行模糊查询">
        <button>搜索</button>
    </div>
    <table>
        <thead>
            <tr>
                <th class="text-left width-25">Category</th>
                <th class="text-center width-50">Logtal状态</th>
                <th class="text-right width-25">操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td class="text-center create">
                    你还没有添加Category，立即<a href="#">创建Category</a>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <div id="page">
        <ul>
            <li><a href="">&laquo;</a></li>
            <li><a href="">&lsaquo;</a></li>
            <li class="selected"><a href="">1</a></li>
            <li><a href="">&rsaquo;</a></li>
            <li class="right"><a href="">&raquo;</a></li>
        </ul>
        <span>共有0条，每页显示10条</span>
    </div>
    </div>
</body>
</html>