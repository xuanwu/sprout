﻿<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="../../css/weblog/category_list.css">

</head>
<body>
	<div>
	<table id="categoryContent">
		<thead>
			<tr class="category_htr" bgcolor="#E9E9E4">
					<th class="text-center width-50"  style="padding-left:30px;">日志文件</th>
					<th class="text-center width-15" >时间</th>
					<th class="text-center width-15">操作</th> 
				</tr>
		</thead>
		<#if result.success> 
			<#assign lists=result.object>
		<tbody id='tbody1'>
			<#if lists?size &gt; 0> <#list lists as item>
				<tr class="managemen_mon_info server_manager">
				<td class="text-center" >
					<span class="managemen_mon_info_sp">
						<a class="category_view" value="">${item.filename?default('/')}</a>
					</span>
				</td>
				<td class="text-center" >${item.collectedDay?default('/')}</td>
				<td class="text-center">
					<span class="managemen_mon_info_sp" ><a >概况统计</a></span>
				</td>
			</tr>
			</#list>
			</#if>
		</tbody>
		<#else> error:${result.message}</#if>
	</table>
	</div>	
	
	<!-- navigation holder -->
	<div class="holder"></div>
	
</body>
</html>