﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/category.css">

	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<script>

$(document).ready(function () { //页面加载完成后事件
	var url = "../../weblogs/listlogs.fm";	
	var cid = $("#cid").val();
	//组装请求参数
	$.post(url, "id="+cid, function(result){
			$("#categoryContent").html(result);
			$(".server_manager").mouseenter(function(){
    		    $(this).css("background-color","#E9E9E4");
	  		});
	  	
		  	$(".server_manager").mouseleave(function(){
		    		$(this).css("background-color","");
		  	});
	});
});

</script>
</head>

<input type="hidden" id="cid" value="0">
<div id="top_cat">
		<div id="top_title">日志列表</div>	
	</div>
<div id="search">
	<input id='fuzzyQueryKey' type="text" placeholder="请输入监控名或类型进行模糊查询">
	<button >搜索</button>
</div>

<div class="container" id="categoryContent"></div>

</html>