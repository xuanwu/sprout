﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
<style type="text/css">
	#top_cat{
		margin-top: 33px;
	}

	.create_btn{
	float: right;
	display:block;
	margin-right: 2%;
	margin-bottom:2px;
	text-align: center;
	cursor: pointer;
	border: none;
	margin-right: 1px;
	color: #ffffff;
	font-size: 15px;
	background-color:#2c86c2;
	padding:5px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	-khtml-border-radius: 2px;
	border-radius: 2px;
}
.create_btn:hover{
	background:#09f;
}
#categoryContent {
	border-collapse: collapse;
	clear:both;
	width: 100%;
	border-bottom: 1px solid #e9e9e9;
	margin-bottom: 20px;
	padding-top:20px;
	font-weight:normal;
}
#categoryContent tr{
	border-bottom:1px solid #e9e9e9;
}
#categoryContent tr th{
	padding:10px;
}
#categoryContent tr td{
	padding:5px;
}
.category_htr {
	height:30px;
	color:#a9a9a7;
	border-top:1px solid #e9e9e9;
	font-weight:normal;
}
.add_btn  {
	margin: 50px auto 0;
	text-align: center;
	display: block;
	font-size: 20px;
	color: #3AA6E9;
	cursor: pointer;
}
#search{
	float: left;
	margin-bottom:20px;
	margin-top: 60px;
	margin-left: -460px;
}
.search_input{
	padding: 6px 9px;
	min-width: 200px;
	border: 1px solid #e9e9e9;
}
#search button {
	height: 32px;
	padding: 6px 9px;
	text-shadow: 0 1px 1px rgba(0,0,0,.12);
	background-image: -webkit-linear-gradient(top,#f8f8f8,#f4f4f4);
	border: 2px solid #e9e9e9;
	cursor: pointer;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	-khtml-border-radius: 4px;
	border-radius: 4px;
}
#start_time{
	float:left;
	margin-bottom:20px;
	margin-top: 30px;
}
#end_time{
	float:left;
	margin-bottom:20px;
	margin-left: 20px;
	display: block;
	margin-top: 30px;
}

#page {
	height: 35px;
	margin-right:70px;
}
#page span {
	float: right;
	line-height: 35px;
}
#page ul {
	float: right;
	list-style: none;
	padding: 0;
	margin: 0;
	margin-left: 15px;
}
#page ul li {
	float: left;
	border-top: 1px solid #e9e9e9;
	border-bottom: 1px solid #e9e9e9;
	border-left: 1px solid #e9e9e9;
}
#page ul a {
	text-decoration: none;
	display: block;
	width: 33px;
	height: 33px;
	line-height: 30px;
	text-align: center;
}
#page ul a:hover {
	background: #556676;
	color: #fff;
}
#page li.right {
	border-right: 1px solid #e9e9e9;
}
#page li.selected > a {
	background: #556676;
	color: #fff !important;
}
.create {
	height: 150px;
	text-align:center;
}
.create a{
	text-decoration: none;
	color: #208fee;
	cursor:pointer;
}
.text-left {
	text-align: left;
	
}
.text-center {
	text-align: center;
}
.text-right {
	text-align: right;
	width: auto;
}
.width-40 {
	width: 50%;
}
.width-30 {
	width: 35%;
}
#log_search{
	margin-left: 15px;
}
</style>
<script>

/*125.216.243.88 - - [25/Mar/2015:11:23:34 +0800] "GET /index HTTP/1.1" 302 - 272 "http://112.74.125.71:8080/login" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0"
125.216.243.88 - - [25/Mar/2015:11:23:36 +0800] "GET /mainMedia/index/2 HTTP/1.1" 200 30660 1769 "http://112.74.125.71:8080/login" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0"
*/
$(document).ready(function () { //页面加载完成后事件
	$("#log_search").click(function(){
			var url = "../../category/search.fm";
			//组装请求参数
			var sampleLogs = "";	
			$.post(url, {'sampleLogsResult' : sampleLogs, 'type' : 1}, function(result) {
					//console.log("result----->"+result);
					$("#content_list").html(result);
			});
	});
});

</script>
</head>
<div id="top_cat">
		<div id="top_title">搜索</div>	
	</div>
<div id="start_time">
	<input id='start_time_input' class="search_input" type="text" placeholder="请输入开始时间">
</div>
<div id="end_time">
	<input id='end_time_input' class="search_input" type="text" placeholder="请输入结束时间">
</div><br>
<div id="search">
	<input id='fuzzyQueryKey' class="search_input" type="text" placeholder="请输入监控名或类型进行模糊查询">
	<button id ="log_search">搜索</button>
</div>
	
</div>

<div class="container" id="categoryContent">
	<div id='content_list'>
		
	</div>
	
</div>

</html>