<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../css/weblog/log_search.css">

</head>
<body>
	<div class="analyse_result" id="search_result">
		<div class="analyse_result_title" id="search_result_em">
			<em>*</em>搜索结果:
		</div>
		<div class="analyse_result_title" id="biaoshi">标识</div>
		<div class="analyse_result_title">取值</div>

		<div id="analysis_box" class="container">
			<div id='any_left'>
			<#assign lists=result> 
				<#list lists as app>
				<ul>
					<li></li>
					<li class="tr_value">&nbsp;</li>
				</ul>
				<#if app.remote_user?exists >
				<ul>
				<li></li>
				<li class="tr_value">&nbsp;</li>
				</ul>
				</#if>
				<#if app.time_local?exists >
				<ul>
				<li></li>
				<li class="tr_value">${(app.time_local)!}</li>
				</ul>
				</#if>
				<#if app.request?exists >
				<ul>
				<li></li>
				<li class="tr_value">&nbsp;</li>
				</ul>
				</#if>
				<#if app.status?exists >
				<ul>
				<li></li>
				<li>${(app.remote_addr)!}</li>
				</ul>
				</#if>
				<#if app.body_bytes_sent?exists >
				<ul>
				<li></li>
				<li>&nbsp;</li>
				</ul>
				</#if>
				<#if app.http_referer?exists >
				<ul>
				<li></li>
				<li>&nbsp;</li>
				</ul>
				</#if>
				<#if app.http_user_agent?exists >
				<ul>
				<li></li>
				<li>&nbsp;</li>
				</ul>
				</#if>
				<#if app.method?exists >
				<ul>
				<li></li>
				<li>&nbsp;</li>
				</ul>
				</#if>
				<#if app.http_version?exists >
				<ul>
				<li></li>
				<li>&nbsp;</li>
				</ul>
				</#if>
				</#list>
			</div>
			<div id="any_right">
				<table>
					<#assign lists=result> 
					<#list lists as app>
					<#if app.remote_user?exists >
					<tr>
					<td>客户端用户名称：</td>
					<td class="tr_value">${(app.remote_user)!}</td>
					</tr>
					</#if>
					<#if app.time_local?exists >
					</#if>
					<#if app.request?exists >
					<tr>
					<td>请求的URL与HTTP协议：</td>
					<td class="tr_value">${(app.request)!}</td>
					</tr>
					</#if>
					<#if app.status?exists >
					<tr>
					<td>请求状态：</td>
					<td>${(app.status)!}</td>
					</tr>
					</#if>
					<#if app.body_bytes_sent?exists >
					<tr>
					<td>文件内容大小：</td>
					<td>${(app.body_bytes_sent)!}</td>
					</tr>
					</#if>
					<#if app.http_referer?exists >
					<tr>
					<td>页面链接：</td>
					<td>${(app.http_referer)!}</td>
					</tr>
					</#if>
					<#if app.http_user_agent?exists >
					<tr>
					<td>客户浏览器：</td>
					<td>${(app.http_user_agent)!}</td>
					</tr>
					</#if>
					<#if app.method?exists >
					<tr>
					<td>请求方法：</td>
					<td>${(app.method)!}</td>
					</tr>
					</#if>
					<#if app.http_version?exists >
					<tr>
					<td>HTTP版本：</td>
					<td>${(app.http_version)!}</td>
					</tr>
					</#if>
					</#list>
				</table>
			</div>

		</div>
	</div>
</body>
</html>