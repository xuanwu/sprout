<!DOCTYPE html>
<%@ page language="java" import="com.sprout.webapp.model.UserInfo,java.io.*,java.util.*" pageEncoding="UTF-8" %>

<%
String path = request.getContextPath();
UserInfo user = (UserInfo) session.getAttribute("currentUser");
if (user == null) {
	// response.setContentType("application/json; charset=utf-8");
    response.setHeader("cache-control", "no-cache");
	PrintWriter outprint = response.getWriter();
	outprint.write("<script type=\"text/javascript\">(function() {window.location = 'login-new.jsp';})();</script>");
	outprint.flush();
	outprint.close();
	return;
}
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
</head>
 <script>  
	$(document).ready(function () { //页面加载完成后事件
			var url = "../application/list.fm";	
			//组装请求参数
	//		var data = $("#id").val();	
			$.post(url, null, function(result){
			$("#indexDetail").html(result);
			});
	});
		</script>
<body>
    <div class="header">
		<div class="header1">
			<div class="headline">
				<img src="<%=path%>/images/heading.png"/>
			</div>
			<ul id="nav" style="padding:0 40px;">
            <li><a class="selected" href="index.jsp">应用列表</a></li>
            <li><a href="server.jsp">服务列表</a></li>
			</ul>
		
			<ul class="upright">
				<!--登出-->
				<li id="logout"><a href="<%=path%>/logout.do">退出登录</a></li>
				
				<!--用户登录信息-->
				<li id="login_info">username</li>
			</ul>
		</div>
    </div>
    <div class="container" id="indexDetail">
    </div>
    <div class="footer">
		<div class="footer_Info">
			  <a href="<%=path%>/">联系我们</a></li>
		</div >
		<div class="footer_Info">
			  <a href="<%=path%>/">about sprout</a></li>
		</div >
	</div>
</body>
</html>