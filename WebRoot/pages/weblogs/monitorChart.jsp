<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://"
 + request.getServerName() + ":" + request.getServerPort()
 + path + "/";
String hostName =(String)session.getAttribute("hostName");
String[] times = (String[]) session.getAttribute("times");
double[] values = (double[]) session.getAttribute("values");
double[] diskData = (double[]) session.getAttribute("diskData");
double[] memData = (double[]) session.getAttribute("memData");
double[] bytesinData = (double[]) session.getAttribute("bytesinData");
double[] bytesoutData = (double[]) session.getAttribute("bytesoutData");
int count = Integer.parseInt(session.getAttribute("count").toString());

%>

<script>
$(document).ready(function(){


	Highcharts.setOptions({
		global: {
		useUTC: true //关闭UTC
		},
		}); 

	$('#CPU').highcharts({
        chart: { //图表区选项                                                               
			type : 'area',   //曲线图
            zoomType: 'x',
            animation: Highcharts.svg, // don't animate in old IE               
            marginRight: 80,                                                                                                             
        },		
		
		title: {//标题
            text: '<b>主机${hostName}CPU负载走势图</b>',
            x: -20 //center
        },
        subtitle: {//副标题
            text: '',
            x: -20
        },
        xAxis: {
        	type:'datetime',
      	    maxZoom: 60 * 60 * 1000, // 缩放最大1小时
        	tickInterval:2*60*60*1000,//x轴刻度间隔时间2小时       
        },
        yAxis: {
            title: {
                text: '<b>CPU负载百分比</b>'
            },
             labels: {
                //对坐标刻度值进行格式化
                formatter: function() {
                    return this.value+'%';}
            },  
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]            
        },
        tooltip: {//数据点提示框
             	formatter: function() {   //格式化提示框的内容样式                                          
                    return '<b>'+              
                    this.series.name +'</b>'+Highcharts.numberFormat(this.y, 2 )+'%' +'<br/>'+
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);                          
            }               
        },
        legend: {//图例说明
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {//数据点选项
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits:{//版权信息
            enabled:false // 禁用
       },
        series:  [{
            name: '负载率',
            data:(function() {                                                                           
                var data = [],                                                  
                i;                
                var arrayObj = new Array()//数值
                <%for (int i = 0; i < count; i++) {%>       
                arrayObj[<%=i%>]=<%=values[i]%>;
                <%}%>
                var startDate=new Array();
                <%for (int i = 0; i <count; i++) {%>       
                startDate[<%=i%>]='<%=times[i]%>';
                <%}%>
                var date1=new Array();
                var r=new Array();
                for(i=0;i<startDate.length;i++){
                	date1[i]=new Date(Date.parse(startDate[i].replace(/-/g,   "/")));
                	r[i]= Date.UTC(date1[i].getFullYear(),date1[i].getMonth(),date1[i].getDate(),
            				date1[i].getHours(),date1[i].getMinutes(),date1[i].getSeconds() );
                }
//定义赋值数据变量Y                
          
                
//将XY放到图表中                
            	for (i=0;i<arrayObj.length;i++)
            	{
            		data.push({                                                 
                     x: r[i], 
                     y: arrayObj[i]                                       
                    }); 
            	}                
                return data;                                                    
            })()
        }]
    });
	
	$('#DISK').highcharts({
        chart: { //图表区选项                                                               
			type : 'area',   //曲线图
            zoomType: 'x',
            animation: Highcharts.svg, // don't animate in old IE               
            marginRight: 80,                                                                                                             
        },		
		
		title: {//标题
            text: '<b>主机${hostName}Disk负载走势图</b>',
            x: -20 //center
        },
        subtitle: {//副标题
            text: '',
            x: -20
        },
        xAxis: {
        	type:'datetime',
      	    maxZoom: 60 * 60 * 1000, // 缩放最大1小时
       	     tickInterval:2*60*60*1000,//x轴刻度间隔时间2小时       
        },
        yAxis: {
            title: {
                text: '<b>DISK负载百分比</b>'
            },
             labels: {
                //对坐标刻度值进行格式化
                formatter: function() {
                    return this.value+'%';}
            },  
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]            
        },
        tooltip: {//数据点提示框
             	formatter: function() {   //格式化提示框的内容样式                                          
                    return '<b>'+              
                    this.series.name +'</b>'+Highcharts.numberFormat(this.y, 2 )+'%' +'<br/>'+
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);                          
            }               
        },
        legend: {//图例说明
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {//数据点选项
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits:{//版权信息
            enabled:false // 禁用
       },
        series:  [{
            name: '负载率',
            data:(function() {                                                                           
                var data = [],                                                  
                i;                
                var arrayObj = new Array()//数值
                <%for (int i = 0; i < count; i++) {%>       
                arrayObj[<%=i%>]=<%=diskData[i]%>;
                <%}%>
                var startDate=new Array();
                <%for (int i = 0; i <count; i++) {%>       
                startDate[<%=i%>]='<%=times[i]%>';
                <%}%>
                var date1=new Array();
                var r=new Array();
                for(i=0;i<startDate.length;i++){
                	date1[i]=new Date(Date.parse(startDate[i].replace(/-/g,   "/")));
                	r[i]= Date.UTC(date1[i].getFullYear(),date1[i].getMonth(),date1[i].getDate(),
            				date1[i].getHours(),date1[i].getMinutes(),date1[i].getSeconds() );
                }
//定义赋值数据变量Y                
          
                
//将XY放到图表中                
            	for (i=0;i<arrayObj.length;i++)
            	{
            		data.push({                                                 
                     x: r[i], 
                     y: arrayObj[i]                                       
                    }); 
            	}                
                return data;                                                    
            })()
        }]
    });
	
	$('#MEMORY').highcharts({
        chart: { //图表区选项                                                               
			type : 'area',   //曲线图
            zoomType: 'x',
            animation: Highcharts.svg, // don't animate in old IE               
            marginRight: 80,                                                                                                             
        },		
		
		title: {//标题
            text: '<b>主机${hostName}内存负载走势图</b>',
            x: -20 //center
        },
        subtitle: {//副标题
            text: '',
            x: -20
        },
        xAxis: {
        	type:'datetime',
      	    maxZoom: 60 * 60 * 1000, // 缩放最大1小时
//        	tickInterval:2*60*60*1000,//x轴刻度间隔时间2小时       
        },
        yAxis: {
            title: {
                text: '<b>内存负载百分比</b>'
            },

             labels: {
                //对坐标刻度值进行格式化
                formatter: function() {
                    return this.value+'KB';}
            },  
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]            
        },
        tooltip: {//数据点提示框
             	formatter: function() {   //格式化提示框的内容样式                                          
                    return '<b>'+              
                    this.series.name +'</b>'+Highcharts.numberFormat(this.y, 2 )+'KB' +'<br/>'+
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);                          
            }               
        },
        legend: {//图例说明
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {//数据点选项
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits:{//版权信息
            enabled:false // 禁用
       },
        series:  [{
            name: '负载率',
            data:(function() {                                                                           
                var data = [],                                                  
                i;                
                var arrayObj = new Array()//数值
                <%for (int i = 0; i < count; i++) {%>       
                arrayObj[<%=i%>]=<%=memData[i]%>;
                <%}%>
                var startDate=new Array();
                <%for (int i = 0; i < count; i++) {%>       
                startDate[<%=i%>]='<%=times[i]%>';
                <%}%>
                var date1=new Array();
                var r=new Array();
                for(i=0;i<startDate.length;i++){
                	date1[i]=new Date(Date.parse(startDate[i].replace(/-/g,   "/")));
                	r[i]= Date.UTC(date1[i].getFullYear(),date1[i].getMonth(),date1[i].getDate(),
            				date1[i].getHours(),date1[i].getMinutes(),date1[i].getSeconds() );
                }
//定义赋值数据变量Y                
          
                
//将XY放到图表中                
            	for (i=0;i<arrayObj.length;i++)
            	{
            		data.push({                                                 
                     x: r[i], 
                     y: arrayObj[i]                                       
                    }); 
            	}                
                return data;                                                    
            })()
        }]
    });
	
	$('#bytesIn').highcharts({
        chart: { //图表区选项                                                               
			type : 'area',   //曲线图
            zoomType: 'x',
            animation: Highcharts.svg, // don't animate in old IE               
            marginRight: 80,                                                                                                             
        },		
		
		title: {//标题
            text: '<b>主机${hostName}接受流量走势图</b>',
            x: -20 //center
        },
        subtitle: {//副标题
            text: '',
            x: -20
        },
        xAxis: {
        	type:'datetime',
      	    maxZoom: 60 * 60 * 1000, // 缩放最大1小时
//        	tickInterval:2*60*60*1000,//x轴刻度间隔时间2小时       
        },
        yAxis: {
            title: {
                text: '<b>接受流量</b>'
            },

             labels: {
                //对坐标刻度值进行格式化
                formatter: function() {
                    return this.value+'KB';}
            },  
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]            
        },
        tooltip: {//数据点提示框
             	formatter: function() {   //格式化提示框的内容样式                                          
                    return '<b>'+              
                    this.series.name +'</b>'+Highcharts.numberFormat(this.y, 2 )+'KB' +'<br/>'+
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);                          
            }               
        },
        legend: {//图例说明
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {//数据点选项
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits:{//版权信息
            enabled:false // 禁用
       },
        series:  [{
            name: '接受流量',
            data:(function() {                                                                           
                var data = [],                                                  
                i;                
                var arrayObj = new Array()//数值
                <%for (int i = 0; i < count; i++) {%>       
                arrayObj[<%=i%>]=<%=bytesinData[i]%>;
                <%}%>
                var startDate=new Array();
                <%for (int i = 0; i < count; i++) {%>       
                startDate[<%=i%>]='<%=times[i]%>';
                <%}%>
                var date1=new Array();
                var r=new Array();
                for(i=0;i<startDate.length;i++){
                	date1[i]=new Date(Date.parse(startDate[i].replace(/-/g,   "/")));
                	r[i]= Date.UTC(date1[i].getFullYear(),date1[i].getMonth(),date1[i].getDate(),
            				date1[i].getHours(),date1[i].getMinutes(),date1[i].getSeconds() );
                }
//定义赋值数据变量Y                
          
                
//将XY放到图表中                
            	for (i=0;i<arrayObj.length;i++)
            	{
            		data.push({                                                 
                     x: r[i], 
                     y: arrayObj[i]                                       
                    }); 
            	}                
                return data;                                                    
            })()
        }]
    });
	
	$('#bytesOut').highcharts({
        chart: { //图表区选项                                                               
			type : 'area',   //曲线图
            zoomType: 'x',
            animation: Highcharts.svg, // don't animate in old IE               
            marginRight: 80,                                                                                                             
        },		
		
		title: {//标题
            text: '<b>主机${hostName}内存发送流量图</b>',
            x: -20 //center
        },
        subtitle: {//副标题
            text: '',
            x: -20
        },
        xAxis: {
        	type:'datetime',
      	    maxZoom: 60 * 60 * 1000, // 缩放最大1小时
//        	tickInterval:2*60*60*1000,//x轴刻度间隔时间2小时       
        },
        yAxis: {
            title: {
                text: '<b>发送流量</b>'
            },

             labels: {
                //对坐标刻度值进行格式化
                formatter: function() {
                    return this.value+'KB';}
            },  
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]            
        },
        tooltip: {//数据点提示框
             	formatter: function() {   //格式化提示框的内容样式                                          
                    return '<b>'+              
                    this.series.name +'</b>'+Highcharts.numberFormat(this.y, 2 )+'KB' +'<br/>'+
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x);                          
            }               
        },
        legend: {//图例说明
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {//数据点选项
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        credits:{//版权信息
            enabled:false // 禁用
       },
        series:  [{
            name: '发送流量',
            data:(function() {                                                                           
                var data = [],                                                  
                i;                
                var arrayObj = new Array()//数值
                <%for (int i = 0; i < count; i++) {%>       
                arrayObj[<%=i%>]=<%=bytesoutData[i]%>;
                <%}%>
                var startDate=new Array();
                <%for (int i = 0; i < count; i++) {%>       
                startDate[<%=i%>]='<%=times[i]%>';
                <%}%>
                var date1=new Array();
                var r=new Array();
                for(i=0;i<startDate.length;i++){
                	date1[i]=new Date(Date.parse(startDate[i].replace(/-/g,   "/")));
                	r[i]= Date.UTC(date1[i].getFullYear(),date1[i].getMonth(),date1[i].getDate(),
            				date1[i].getHours(),date1[i].getMinutes(),date1[i].getSeconds() );
                }
//定义赋值数据变量Y                
          
                
//将XY放到图表中                
            	for (i=0;i<arrayObj.length;i++)
            	{
            		data.push({                                                 
                     x: r[i], 
                     y: arrayObj[i]                                       
                    }); 
            	}                
                return data;                                                    
            })()
        }]
    });
});

</script>


<ul id="myTab" class="nav nav-tabs">
               <li class="active"><a href="#CPU" data-toggle="tab">CPU</a></li>
              <li ><a href="#DISK" data-toggle="tab">DISK</a></li>
               <li><a href="#MEMORY" data-toggle="tab">MEMORY</a></li>
               <li><a href="#bytesIn" data-toggle="tab">bytesIn</a></li>
               <li><a href="#bytesOut "data-toggle="tab">bytesOut</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="CPU" style="min-width:1200px;height:500px;">
              </div>
              <div class="tab-pane fade " id="DISK" style="min-width:1200px;height:500px;">
              </div>
              <div class="tab-pane fade" id="MEMORY" style="min-width:1200px;height:500px;">
              </div>
              <div class="tab-pane fade" id="bytesIn" style="min-width:1200px;height:500px;">
              </div>
              <div class="tab-pane fade" id="bytesOut" style="min-width:1200px;height:500px;">
              </div>
            </div>
            