<thead>
				<tr class="category_htr">
					<th class="text-center width-15">名称</th>
					<th class="text-center width-15">服务器</th>
					<th class="text-center width-15">程序</th>
					<th class="text-center width-15">时间</th>
					<th class="text-center width-15">错误详情</th>
				</tr>
			</thead>	
<tbody >
<#if result.success>
			<#assign lists=result.object>
			<#if lists?size &gt; 0>	
			<#list lists as item>	
			
			<tr class="managemen_mon_info server_manager">
				<td><a class="server_host monitor_name" >${item.error?default('null')}</a>
					</br>
					${item.host?default('null')}
				</td>
				<td>${item.hot?default('125.216.243.192')}</td>
				<td>${item.hos?default('worker')}</td>
				<td>${item.created_date?default('')}</td>
				<td align="center">
					<a class="server_host monitor_describle" host="${item.host?default('0')}"><span class="managemen_mon_info_sp" style="margin-left:10px;" >查看异常</span></a>
				</td>
				<tr  style="display: none" class="describle">
					<td >${item.describle?default('0')}</td>
				</tr>
			</tr>
			 </#list>
				 </#if>
			<#else>
		<tr>
			<td class="text-center create" colspan="4">
				暂无数据！
			</td>
		
 </#if>
   </tbody>
<body>
