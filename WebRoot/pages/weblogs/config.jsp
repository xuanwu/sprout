﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/weblog/config.css">
</head>

<script>
	$(document).ready(function() {

		$("#category_Conf_Id").html($('#category_Conf_Id_Hidden').html());
		$("#category_Conf_Name").html($('#category_Conf_Name_Hidden').html());

		$("#step1_content").load("config_step1.jsp");
		$("#step2_content").load("config_step2.jsp");
		$("#step3_content").load("config_step3.jsp");
	});
</script>

<body>
	<span id='category_Conf_Id' style="display: none;"></span>
	<span id='category_Conf_Name'style="display: none;"></span>
	<!--style="display: none;"-->
	<div class="config_content">
		<div class="process_bar">
			<div class="process_li processing">1.监控配置</div>
			<div class="process_border process_gray " id="pro_border1">
				<div class="tri_border tri_blue"></div>
			</div>
			<div class="process_li" id="process2">2.日志测试</div>
			<div class="process_border process_gray" id="pro_border2">
				<div class="tri_border tri_gray" id="tri_border2"></div>
			</div>
			<div class="process_li" id="process3">3.完成配置</div>
		</div>

		<div id="step1_content"></div>
		<div id="step2_content" style="display: none;"></div>
		<div id="step3_content" style="display: none;"></div>

	</div>
</body>
</html>