﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/weblog/config.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/weblog/project.css">
</head>
<script>	

/*
var url = "../../category/getAllCategory.fm";	
//组装请求参数
//	var data = $("#id").val();	
$.post(url, null, function(result){
$("#categoryContent").html(result);
});
*/

	var logPath = '';
	var sampleLogs = '';
	var re = '';
	var ip = '';
	var host =null;
	$(document).ready(function () { 
		//document.getElementsByName('sampleLogs')[0].value='';
		$("#sampleLogs").val("");//初始化textfield
		
		$("#log_type").click(function(){
			var index = $("#log_type ").get(0).selectedIndex;
			console.log("index-->"+index);
			switch (index) {
			case 0:
				$("#log_formatlog_format").val("");
				break;
			case 1:
				$("#log_formatlog_format").val("");
				break;
			case 2:
				$("#log_format_tr").show();
				break;
			default:
				break;
			}
		});
		$("#nextBtn1").on("click",function(){
			//先检查表单内容是否都有输入
			host = $("#host").val();
			logPath = $("#logpath").val();
			sampleLogs = $("#sampleLogs").val();
			if(logPath != '' && sampleLogs != '' && host != ''){
					document.getElementsByName('sampleLogs')[1].value = sampleLogs;
					//获取
					/*var url = "../../category/validateSample.fm";
					//组装请求参数
					//	var data = $("#id").val();	
					$.post(url, {'sampleLogsResult' : s[0], 're' : re}, function(result) {
						if(result.indexOf('Exception') > 0)
							alert('发生错误，请检查输入');
						else
							$("#analyseFreemarker").html(result);
					});*/
					$("#process2").addClass("processing");
					$("#pro_border1").removeClass("process_gray");
					$("#pro_border1").addClass("process_blue");
					$("#tri_border2").removeClass("tri_gray");
					$("#tri_border2").addClass("tri_blue");
					$("#step1_content").hide();
					$("#step2_content").show();
			}
			else{
				alert('请输入必填项');
			}
			
			var url = "../../category/validateSample.fm";
			//组装请求参数
			//	var data = $("#id").val();	
			$.post(url, {'sampleLogsResult' : sampleLogs, 'type' : 0}, function(result) {
				if(result.indexOf('Exception') > 0)
					alert('发生错误，请检查输入');
				else
					$("#analyseFreemarker").html(result);
			});
		});
	});

	var first_step = 1;
	
	function return_back(){
		window.location.href='./logHome.jsp' + window.location.search;
	}

</script>

<form id="step1_form" method="post">
	<table class="addPro_table">
		<tbody>
			<tr>
				<td><em>*</em> 监控系统:</td>
				<td>
				<select name="system" id='system'>
					<option value="linux" selected="selected">linux</option>
					<option value="windows">windows</option>
				</select>
				&nbsp;&nbsp;监控的服务器操作系统类型</td>
			</tr>
			<tr>
				<td><em>*</em> 监控主机:</td>
				<td><input name="host" id='host' type="text" />&nbsp;&nbsp;监控的服务器IP地址</td>
			</tr>
			<tr>
				<td><em>*</em> 日志路径:</td>
				<td><input name="logpath" id='logpath' type="text" />&nbsp;&nbsp;代表服务器路径</td>
			</tr>

			<tr>
				<td style="vertical-align: top;"><em>*</em> 日志详例:</td>
				<td><textarea name='sampleLogs' id="sampleLogs" rows="12">
						</textarea></td>
			</tr>
			
			<tr>
				<td><em>*</em> 日志类型:</td>
				<td>
				<select name="log_type" id='log_type'>
					<option value="custom" selected="selected">custom</option>
					<option value="combined">combined</option>
					<option value="other">other</option>
				</select>
				&nbsp;&nbsp;<label id="log_format"></label></td>
			</tr>
			
			<tr style="display: none;" id="log_format_tr">
				<td><em>*</em> 自定义类型:</td>
				<td><input name="log_format_input" id='log_format_input' />&nbsp;&nbsp;日志模式</td>
			</tr>
			
		</tbody>
	</table>
</form>
<div class="btnbox">
	<button class="cancel_button" id="cancelBtn1" onClick=return_back() style="margin: 20px auto;">取消</button>
	<button class="addPro_button" id="nextBtn1">下一步</button>
</div>
</html>