<!DOCTYPE html>
<%@ page language="java" import="com.sprout.webapp.model.UserInfo,java.io.*,java.util.*" pageEncoding="UTF-8" %>

<%
String path = request.getContextPath();
UserInfo user = (UserInfo) session.getAttribute("currentUser");
if (user == null) {
	// response.setContentType("application/json; charset=utf-8");
    response.setHeader("cache-control", "no-cache");
	PrintWriter outprint = response.getWriter();
	outprint.write("<script type=\"text/javascript\">(function() {window.location =location.origin+'/sprouts/pages/login-new.jsp';})();</script>");
	outprint.flush();
	outprint.close();
	return;
}
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/webLog.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/appMan.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>

</head>
<body>
    <div class="header">
		<div class="header1">
			<div class="headline">
				<img src="<%=path%>/images/heading.png"/>
			</div>
			<ul id="nav" style="padding:0 40px;">
				<li><a href="<%=path%>/pages/index.jsp">云服务</a></li>
				 <li><a class="selected" href="<%=path%>/pages/weblogs/weblog.jsp">日志项目</a></li>
			</ul>
		
			<ul class="upright">
				<!--登出-->
				<li id="logout"><a href="<%=path%>/logout.do">退出登录</a></li>
				
				<!--用户登录信息-->
				<li id="login_info"><%="尊敬的 " + user.getUsername() + " , 欢迎您!"%></li>
			</ul>
		</div>
    </div>
    <div class="container" id="weblogDetail">
    </div>
    <div class="footer">
		<div class="footer_Info">
			  <a href="<%=path%>/">联系我们</a>
		</div>
		<div class="footer_Info">
			  <a href="<%=path%>/">about sprout</a></li>
		</div >
	</div>
</body>
<script>  

</script>
<script type="text/javascript" >
	$(document).ready(function () { //页面加载完成后事件
		var url = "../../category/list.fm";	
		$.post(url, null, function(result){
			$("#weblogDetail").html(result);
		});
	});
	
	function deleteApplication(para){
		var url = "../../category/deleteApplication.do";	
		$.post(url, {'applicationId' : para}, function(result){
			if(result == 'true'){
				alert('删除成功,点击后刷新');
				window.location = '/sprouts/pages/weblogs/outline.jsp';
			}
			else
				alert('删除失败,请联系管理员');
		});
	}
</script>
</html>