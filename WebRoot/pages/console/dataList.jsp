﻿<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>


<script>


	$(document).ready(function () { //页面加载完成后事件
		fun_onclick("createList");
		fun_onclick("createList2");
	});
	function fun_onclick(selectorName){
		$("#"+selectorName).on("click",function () {
				if(selectorName==="createList"||selectorName==="createList2")
				$("#mainInfobox1").load("createList.jsp");
		});
	}
	/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>
</head>
	<div id="top_cat">
		<div id="top_title">监控列表</div>	
	</div>
	<button class="create_btn" id="createList">创建新监控</button>
	<div class="c_search">
		<input type="text" placeholder="请输入监控服务器名进行模糊查询">
		<button>搜索</button>
	</div>
	
	
	<table  id="categoryContent"  >
		<thead>
			<tr class="category_htr">
				<th class="text-left width-15">名称</th>
				<th class="text-center width-15">所有者</th>
				<th class="text-center width-25">权限管理</th>
				<th class="text-right width-35" style="padding-right:110px;">操作</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left" id="catName">hadoop_zhou</td>
				<td class="text-center">ALIYUN$landszhou@gmail.com</td>
				<td class="text-center">
					<span class="managemen_mon_info_sp" >查看权限</span>
					<span class="managemen_mon_info_sp"  style="margin-left:10px;" >ACL权限</span>
				</td>
				<td class="text-right">
					<span class="managemen_mon_info_sp" >属性</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" >分区</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" >数据</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" >上传</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" >下载</span>
					<span class="managemen_mon_info_sp" style="margin-left:10px;" >删除</span>
				</td>
			</tr>
			<tr>
				<td class="text-center create" colspan="4">
					你还没有添加数据表，立即<a  id="createList2" >创建数据表</a>
				</td>
			</tr>
		</tbody>
	</table>	
	<div id="page">
		<ul>
			<li><a href="">&laquo;</a></li>
			<li><a href="">&lsaquo;</a></li>
			<li class="selected"><a href="">1</a></li>
			<li><a href="">&rsaquo;</a></li>
			<li class="right"><a href="">&raquo;</a></li>
		</ul>
		<span>共有0条，每页显示10条</span>
	</div>
</html>