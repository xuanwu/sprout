<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/appMan.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/weblog/logOutline.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>
	

</head>
	
<body>

	
	<!--顶部app信息-->
        <div class="up" style="margin-top:2px;margin-left:15px;">
			<div class="left">
				<div class="app_name"><a id=" " href="index.jsp" target="_blank">project</a></div>
				<div class="log_title">开放数据处理服务ODPS</div>
			</div>	
        </div>
		
      <script>  
			$(document).ready(function () { //页面加载完成后事件
				var flag1=false;
				var flag2=true;
				$("#subnav_1").slideDown();
				$("#mainInfobox1").load("dataList.jsp");
				$("#lognav").on("click", "li",function () {  //	
					var index1=$(".appnav").index(this);
					var index2=$(".subnav_li").index(this);
					if(index1==0||index1==1){
						$("li.appnav_selected").removeClass("appnav_selected"); 
						$(this).addClass("appnav_selected"); 
						if(index1==0){
							if(flag1){ 
								$("#subnav_1").slideDown();
							}else{
								$("#subnav_1").slideUp();
							}	
							flag1=!flag1;
						}else if(index1==1){
							if(flag2){ 
								$("#subnav_2").slideDown();
							}else{
								$("#subnav_2").slideUp();
							}	
							flag2=!flag2;
						}
					}else if(index1==-1){
						$("li.subnav_selected").removeClass("subnav_selected"); 
						$(this).addClass("subnav_selected"); 
						switch(index2) {  
							case(0):
								$("#mainInfobox1").load("dataList.jsp"); break;//数据表
							case(1):
								$("#mainInfobox1").load("checkTask.jsp");break;//查看作业
							case(2):
								$("#mainInfobox1").load("doTask.jsp");break;//执行作业
						}
					}
				});
			
			});
		</script>
		
		<div class="maincontent">
			<!--左边菜单栏-->
			<ul class="appMan_nav" id="lognav">
				<li id="lognav1" class="appnav appnav_selected" >数据</li>
				<ul id="subnav_1" class="subnav">
					<li class="subnav_li subnav_selected">数据表</li>
				</ul>
				<li id="lognav2" class="appnav" >作业</li>
				<ul id="subnav_2" class="subnav">
					<li class="subnav_li">查看作业</li>
					<li class="subnav_li">执行作业</li>
				</ul>
			</ul>
			
			<!--右边详细信息-->
			<div class="maininfo">
				<!--应用详情界面-->
				<div id="mainInfobox1" >
				</div>
				
				

			</div>
		</div>   
		
</body>
</html>