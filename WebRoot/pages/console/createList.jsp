<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>

<script>

</script>
<style>
._content .addPro_table{
	margin:20px 0;
}
</style>
</head>
<body>
	<div id="top_cat">
		<div id="top_title">sample</div>
		<a href="projectDetail.jsp">返回数据表列表</a>		
	</div>
	
	<div class="_content">
		<form id="createList_form" method="post" action=" " >
			<table class="addPro_table" >
				<tbody>
				<tr>
					<td><em>*</em>
					数据表名称:
					</td>
					<td colspan="5"><input name="listname" type="text" placeholder="只能包含英文字母、数字、下划线，且只能以字母开头，长度小于128个字符"/></td>
				</tr>
				<tr>
					<td>
					注释:
					</td>
					<td colspan="5"><input name="_comment" type="text" placeholder="不能输入&lt;&gt;&quot;'\字符，最多1024个字符。"/></td>
				</tr>
				
				</tbody>
			</table>
			
		</form>
		
		<button class="addPro_button" >创建</button>
		<button class="cancel_button" >取消</button>
		
	
	</div>
</body>	
</html>