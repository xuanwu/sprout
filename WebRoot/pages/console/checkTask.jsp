<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	<script type="text/javascript"  src="<%=path%>/js/jquery-1.11.0.min.js"></script>


<script>


	$(document).ready(function () { //页面加载完成后事件

	});
	/**function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}**/
</script>
</head>
	<div id="top_cat">
		<div id="top_title">作业列表</div>	
	</div>
	
	<div class="_content" style="border-top:none;top:5px;">
		<table  id="categoryContent"  >
			<thead>
				<tr class="category_htr">
					<th class="text-left width-15">编号(Instance ID)</th>
					<th class="text-center width-15">状态</th>
					<th class="text-center width-15">所有者(Owner)</th>
					<th class="text-center width-15">启动(Start Time)</th>
					<th class="text-center width-15">时长(Latency)</th>
					<th class="text-right width-25" style="padding-right:110px;">操作</th>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td class="text-center create" colspan="4">
						暂无数据！
					</td>
				</tr>
			</tbody>
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</html>