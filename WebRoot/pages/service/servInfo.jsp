﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <link rel="stylesheet" type="text/css" href="<%=path%>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/servInfo.css">
	 
    <script type="text/javascript" src="<%=path%>/js/app.js"></script>
</head>
 <script>  
	$(document).ready(function () { //页面加载完成后事件
			var url = "../service/list.fm";	
			//组装请求参数
			var data={id:location.search.substr(4)};	
			$.post(url, data, function(result){
				$("#serviceDetail").html(result);
			});
	});
		</script>
<body>
 
 <div id="serviceDetail"></div>
        <!--
        <table class="serv_table" id="servTable">
            <thead>
                <tr>
                    <th>服务名</th>
                    <th>编号</th>
                    <th>服务说明</th>
                    <th>创建时间</th>
                    <th width="135px">操作</th>
                </tr>
            </thead>
            <tbody id="servTable_tbody">
                <tr >
                    <td style="text-align:left"><img src="<%=path%>/images/mapreduceIcon.png"/>MapReduce</td>
                    <td>7727</td>
                    <td>Mapreduce service</td>
                    <td>2014-2-27</td>
                    <td >
                        <button class="serv_table_unbound" onclick="toUnbound(this)">解绑</button><button class="serv_table_bound">管理</button>
                    </td>
                </tr>
				
				<tr >
                    <td style="text-align:left"><img src="<%=path%>/images/filesystemIcon.png"/>FileSystem</td>
                    <td>7750</td>
                    <td>Persistent FileSystem service</td>
                    <td>2014-2-27</td>
                    <td >
                        <button class="serv_table_bound" onclick="toBound(this)">绑定</button>
                    </td>
                </tr>
            </tbody>
        </table>
        -->
		
		<div class="create_content">
		  		<div class="create_service_title">创建新服务</div>	
		  		<form action="" method="post" id="service_create_form" >
		  		 
		  		 <input type="hidden" id="service_type" name="servicetype" value="java web" />
                <div id="serviceTypes" class="create_service_box">
				     <div class="create_content_box create_content_selected" id="servIcon1" onclick="servIconOnclick(1)">
					      <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/javawebIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">java_web</div>
					 </div>
					 <div class="create_content_box" id="servIcon2" onclick="servIconOnclick(2)">
					      <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/filesystemIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">FileSystem</div>
					 </div>
					 <div class="create_content_box" id="servIcon3" onclick="servIconOnclick(3)">
					     <div class="create_content_box_ser_bk" >
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/mangodbIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">MangoDB</div>
					 </div>
					 <div class="create_content_box" id="servIcon4" onclick="servIconOnclick(4)">
					     <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/mapreduceIcon.png) center no-repeat" ></div>
						 </div>
						<div class="managemen_ser_word">MapReduce</div>
					 </div>
					<div class="create_content_box" id="servIcon5" onclick="servIconOnclick(5)">
					     <div class="create_content_box_ser_bk">
						     <div class="managemen_ser_pic" style="background:url(<%=path%>/images/mysqlIcon.png) center no-repeat"></div>
						 </div>
						<div class="managemen_ser_word">MySQL</div>
					 </div>
				</div>
				
				 
                <div class="create_content_2">
			         <div class="create_content_url_left">
						 <input type="text" id="servName" name="servicename" style="padding-left:5px;line-height:30px;color:#999;" placeholder="请填入新服务名称" size="40"/>
					 </div>     
			        	<div class="create_content_button" onclick="" id="createService">创建服务</div>
			   </div>
			   </form>
		</div>

 
</body>
</html>