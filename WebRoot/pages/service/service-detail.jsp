<!DOCTYPE html>
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/style.css">
	<link rel="stylesheet" type="text/css" href="${requestatt.contextPath}/css/appMan.css">
	
	<script type="text/javascript" src="${requestatt.contextPath}/js/app.js"></script>
</head>

<script>
$(document).ready(function () { 
	//查看日志
	$('.serv_table_bound').click(function () {
		//eval("document.getElementById('manageService').style.display='block'");
	   // eval("document.getElementById('all').style.display='block'");
		//onclick="$('#all').css('display','block');$('#manageService').css('display','block');"
		
	});

});
</script>
<body>
<#if result.success>
<#assign lists=result.object>
        <table class="serv_table" id="servTable">
            <thead>
                <tr>
                    <th>服务名</th>
                    <th>编号</th>
                    <th>服务说明</th>
                    <th>创建时间</th>
                    <th width="135px">操作</th>
                </tr>
            </thead>
            <tbody id="servTable_tbody">
            <#if lists?size &gt; 0>
        		<#list lists as service>
                <tr >
                    <td style="text-align:left">
                    <#assign servicetype=service.servicetype>
                    <#if servicetype=='MongoDB'>
                    <img src="${requestatt.contextPath}/images/mangodbIcon.png"/>
                    <#elseif servicetype=='FileSystem'>
                    <img src="${requestatt.contextPath}/images/filesystemIcon.png"/>
                    <#elseif servicetype=='java web'>
                    <img src="${requestatt.contextPath}/images/javawebIcon.png"/>
                    <#elseif servicetype=='Mapreduce'>
                    <img src="${requestatt.contextPath}/images/mapreduceIcon.png"/>
                     <#elseif servicetype=='MySQL'>
                    <img src="${requestatt.contextPath}/images/mysqlIcon.png"/>
                    </#if>
                    ${service.servicename}</td>
                    <td>7727</td>
                    <td>${service.servicetype}</td>
                    <td>${service.createdDate}</td>
                    <td >
                        <button class='serv_table_unbound unbound' data-servicetype='${service.servicetype}'>解绑</button>
                        <button class='serv_table_bound' id="serv_manager_button" onclick="$('#all').css('display','block');$('#manageService').css('display','block');">管理</button>
						<!--<button class='serv_table_bound bound' data-servicetype='${service.servicetype}'>绑定</button>";-->
                    </td>
                </tr>
                </#list>
                </#if>
            </tbody>
        </table>
 <#else>
error:${result.message}
</#if>
</body>
</html>