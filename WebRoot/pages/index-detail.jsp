﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
    <link rel="stylesheet" href="../css/jpager/style.css">
	<link rel="stylesheet" href="../css/jpager/jPages.css">
	<link rel="stylesheet" href="../css/jpager/animate.css">
    <link rel="stylesheet" href="../css/jpager/github.css">

	<script src="../js/jpager/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="../js/jpager/highlight.pack.js"></script>
    <script type="text/javascript" src="../js/jpager/tabifier.js"></script>
    <script src="../js/jpager/js.js"></script>
	<script src="../js/jpager/jPages.js"></script>
	<script src="../js/jpager/jquery.lazyload.js"></script>
	 <script>
  /* when document is ready */
  $(function() {
    /* initiate plugin */
        /* initiate the plugin */
        $("div.holder").jPages({
            containerID  : "itemContainer",
			first: '首页',
			last: '尾页',
			previous: '上页',
			next: '下页',
            perPage: 5,
            startPage: 1,
            startRange: 1,
            midRange: 3,
            endRange: 1,
			keyBrowse: true,
			callback    : function( pages, items ){
				/* lazy load current images */
				//items.showing.find("tr").trigger("turnPage");
				/* lazy load next page images */
				//items.oncoming.find("tr").trigger("turnPage");
			}
        });
    
  });
  //服务进度条的百分比
 var temp2='10%';
 document.getElementById("servePercent").style.width=temp2;
 
  </script>
   <style type="text/css">
   ul, ol {
		margin: 0;
	}
	.add_btn a{
		color: #3AA6E9;
	}
	.add_btn a:hover{
		color:#2C86C2;
	}
  .holder {
    margin: 0 100px;
    text-align:right;
  }
  .holder a {
    font-size: 16px;
    cursor: pointer;
    margin: 0 5px;
    color: #333;
  }
  .holder a:hover {
    background-color: #222;
    color: #fff;
  }
  .holder a.jp-previous { margin-right: 15px; }
  .holder a.jp-next { margin-left: 15px; }
  .holder a.jp-current, a.jp-current:hover {
    color: #FF4242;
    font-weight: bold;
  }
  .holder a.jp-disabled, a.jp-disabled:hover {
    color: #bbb;
  }
  .holder a.jp-current, a.jp-current:hover,
  .holder a.jp-disabled, a.jp-disabled:hover {
    cursor: default;
    background: none;
  }
  .holder span { margin: 0 5px; }
  </style>
  
</head>
<body>
<#if result.success>
		<#assign lists=result.object>
		<div class="up">
		<table class="column">
			     <tbody>
				     <tr>
					     <td>
						     <div class="box_1">应用</div>
							 <div class="progress_back">
							     <div class="progress" style="width:220px">
							     	<div  style="width:${lists?size/20*100}%;height:20px;background: #3aa6e9;"></div>
							     	</div>
							     </div>
							 </div>
							 <div id="message" class="box_1_1">(${lists?size}/20个)</div>
						 </td>
						 <td>
						     <div class="box_1">服务</div>
							 <div class="progress_back">
							     <div class="progress" style="width:220px">
							     	<span id="servePercent" class="progress_bar"></span>
							     </div>
							 </div>
							 <div id="ser_message" class="box_1_1">(2/20个)</div>
						 </td>
					 </tr>
				 <tbody>
			 </table>
			 
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>应用名</th>
                    <th>内存</th>
                    <th>实例</th>
                    <th>创建时间</th>
                    <th>应用状态</th>
                </tr>
            </thead>
            <tbody id="itemContainer" >
            <#if lists?size &gt; 0>
            <#list lists as app>
                <tr>
                    <td><a href="apphome.jsp?id=${app.id}">${app.appName}</a></td>
                    <td>128MB</td>
                    <td>1个</td>
                    <td>${app.createdDate}</td>
                    <td>
                        <div class="breakout"></div>
                    </td>
                </tr>
                
                    </#list>
                     <tr>
				</tr>
                    
		    <#else>
		        <tr>
					<td height="200" colspan="5" class="text-center"><div class="add_btn"> <a href="addapplication.jsp">+ 创建新应用</a> </div></td>
				</tr>
		    </#if>
            </tbody>
        </table>
        <div class="add_btn">
            <a href="addapplication.jsp">+ 创建新应用</a>
        </div>
              <!-- navigation holder -->
      <div class="holder"></div>
<#else>
error:${result.message}
</#if>
</body>
</html>