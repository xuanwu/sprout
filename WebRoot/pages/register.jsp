<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/reg_log.css">
<script type="text/javascript" src="../js/register.js"></script>
<body>
	<div class="header">
		<div class="header1">
			<div class="header_title">
				<div class="header_title_action">
					<div style="border-bottom-left-radius: 5px;"
						class="header_title_action_detail"
						onclick="location.href ='register.jsp'">注册</div>
					<div id="login">
						<div style="margin-left: -1px; border-bottom-right-radius: 5px;"
							class="header_title_action_detail"
							onclick="location.href ='login-new.jsp'">登录</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main" style="min-height: 610px;">
		<div class="maincontent">
			<div class="mainlist_reg_log">
				<div class="mainlist_reg_log_left">
					<div class="mainlist_reg_log_title">注册</div>
					<form action="<%=request.getContextPath()%>/user/add.do" onsubmit="return register_judge();" name="form1"
						method="post">
						<div class="mainlist_reg_log_input">
							<input class="mainlist_reg_log_input_item" type="text" id="userNameId" onChange="userNameCheck()"
								name="userName" placeholder="请输入您的用户名">
						</div>
						<div class="mainlist_reg_log_input_error" id ="userNameError" style="display:none;">请输入用户名</div>
						<div class="mainlist_reg_log_input">
							<input class="mainlist_reg_log_input_item" type="text" id="email" onChange="emailCheck()"
								name="email2" placeholder="请输入您的邮箱">
						</div>
						<div class="mainlist_reg_log_input_error" id="emailError" style="display:none;">请输入正确的邮箱</div>
						<div class="mainlist_reg_log_input">
							<div>
								<input class="mainlist_reg_log_input_item" type="password" id="passwordId"
									name="password" onChange="passWordCheck()"
									placeholder="请输入您的密码">
							</div>
						</div>
						<div class="mainlist_reg_log_input_error" id ="passwordError" style="display:none;">X 两次输入的密码不一致</div>
						<div class="mainlist_reg_log_input">
							<input class="mainlist_reg_log_input_item" type="password" id="passwordIdre" onChange="passWordCheck()"
								name="passwordre" placeholder="请再次输入您的密码">
						</div>
						<div class="mainlist_reg_log_input_error"></div>
						<div class="mainlist_reg_log_input">
							<input class="mainlist_reg_log_input_item" type="text" onChange="cellPhoneCheck()" id="cellPhone"
								name="cellphone" placeholder="您的电话号码">
						</div>
						<div class="mainlist_reg_log_input_error" id="cellPhoneError" style="display:none;">请输入正确的电话号码:区号+号码或者手机号码</div>

						<button class="mainlist_reg_log_button" id="register">立刻注册</button>
					</form>
					<div class="go_register_link"
						onclick="location.href ='login-new.jsp'">立即登录</div>
					<div class="no_account">已经有账号</div>
				</div>
				<div class="mainlist_reg_log_right"></div>
			</div>
		</div>
	</div>
	<div class="home_footer"></div>
</body>


</html>