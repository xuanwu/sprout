<!DOCTYPE html >
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/category.css">
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/console/console.css">
	
	<script>

	$(document).ready(function () { //页面加载完成后事件
				var url = "../../userlist.fm";	
				//组装请求参数
            	$.post(url, "", function(result){
					$("#http_content").append(result);
					
					$(".server_manager").mouseenter(function(){
		    		    $(this).css("background-color","#E9E9E4");
				  	});
				  	
				  	$(".server_manager").mouseleave(function(){
				    		$(this).css("background-color","");
				  	});
				  	$(".monitor_status").css("color","#090");
				});
				/*$(".monitor_view").click(function(){
					$("#mainInfobox1").load("httpview.jsp");
					$("#mainInfobox1").children().remove();
					$("#mainInfobox1").html("<div id='container_charts' style='min-width:800px;height:400px;''></div>");
					$('#container_charts').ready(create_charts());
				});*/
	});
	function popWindows(fileName){
		$("#categoryWindow").load(fileName);
		$("#all").show();
	}
	
</script>

</head>
<body>
	<div id="top_cat">
		<div id="top_title">用户列表</div>	
	</div>
	<button class="create_btn" id="createMonitor">创建新</button>
	
	<div class="_content" style="border-top:none;top:5px;">
		<table  id="categoryContent"  >
			<thead>
				<tr class="category_htr">
					<th class="text-left width-15">用户名</th>
					<th class="text-center width-15">电话</th>
					<th class="text-center width-15">email</th>
					<th class="text-center width-15">状态</th>
					<th class="text-right width-25" style="padding-right:150px;">操作</th>
				</tr>
			</thead>
			<tbody id="http_content">
		
			</tbody>
		</table>	
		<div id="page">
			<ul>
				<li><a href="">&laquo;</a></li>
				<li><a href="">&lsaquo;</a></li>
				<li class="selected"><a href="">1</a></li>
				<li><a href="">&rsaquo;</a></li>
				<li class="right"><a href="">&raquo;</a></li>
			</ul>
			<span>共有0条，每页显示10条</span>
		</div>
	</div>
</body>

</html>