function creatPercentChart (boxID, chartTitle, xAxisTitle,yAxisTitil, xAxisArray, yAxisArray,time) {
    $(boxID).highcharts({
        chart: {
            //zoomType: 'x',
            //spacingRight: 20
        },
        title: {
            text: chartTitle
        },
        xAxis: {
        	title: {
                text: xAxisTitle
            },
        	tickPixelInterval: 150,//x轴上的间隔
            type: 'datetime' //定义x轴上日期的显示格式
        },
        yAxis: {
            title: {
                text: yAxisTitil
            }
        },
        tooltip: {
        	enabled: true,
            formatter: function() {
                return '<b>'+ this.series.name +': '+ this.y +' '+suffix3+'</b>'+'<br/>'+Highcharts.dateFormat('%Y-%m-%d %H:%M:%S',this.x) ;
            }
        },
        legend: {
        	enabled: false
        },
        credits: {
          	enabled: false
        },
        exporting: {                                                            
            enabled: false                                                      
        },  
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'area',
            name: '活跃线程数',
            turboThreshold:0, //不限制数据点个数
            data: (function() {                                                                                       
                var data = [],i;
                var rarray=new Array();
                for(i=0;i<xAxisArray.length;i++){
                	var date1 =new Date(Date.parse(xAxisArray[i].replace(/-/g,   "/")));//转化成国际标准格式
                    var r = Date.UTC(date1.getFullYear(),date1.getMonth(),date1.getDate(),
            				date1.getHours(),date1.getMinutes(),date1.getSeconds() );
                    rarray[i]=r-8*60*60*1000;//由于UTC，减去8小时。。。小时*分钟*秒*毫秒
                }
				
                //var time =1*60*60*1000;
                var myDate = new Date(); 
                //var hh= rarray[yAxisArray.length-1]-time;
                var hh= myDate.getTime()-time;//获取当前时间，并前推选择时间（1天，15天，30天）
                for (i = 0; i <= yAxisArray.length-1; i++) { 
                	if(rarray[i]>hh){
                	data.push({                                                 
                        x: rarray[i],
                        y: yAxisArray[i]                                        
                    }); }
                                                                            
                }                                                               
                return data;                                                   
            })()
        }]
    });			
}