
window.onload = function() {
	//判断页面初始化信息，如果有注册失败的话则说明是注册失败返回的url
	var initMessage = window.location.search;
	if(initMessage == '?registeError')
		alert("注册失败,请联系管理员!")
}


var pass1;
var pass2;
var userName;
var cellPhone;
var can_submit = false;

function passWordCheck() {
	pass1 = document.getElementById("passwordId").value;
	pass2 = document.getElementById("passwordIdre").value;
	if (pass1 == pass2) {
		// 不显示提示
		if(pass1!=null&&pass1!=""){
			document.getElementById("passwordError").style.display = "none";
			can_submit = true;
		}
	} else if(pass2!=null&&pass2!=""){
		// 显示提示
		can_submit = false;
		document.getElementById("passwordError").style.display = "block";
	}
}

function userNameCheck() {
	userName = document.form1.userName.value;
	if (userName == '') {
		document.getElementById("userNameError").style.display = "block";
		can_submit = false;
	} else {
		can_submit = true;
		document.getElementById("userNameError").style.display = "none";
	}
}

function cellPhoneCheck() {
	cellPhone = document.form1.cellPhone.value;
	var r = /^1\d{10}$/;// 手机号码正则
	var r2 = /^0\d{2,3}-?\d{7,8}$/;// 固定电话正则
	if (r.test(cellPhone) || r2.test(cellPhone)) {
		can_submit = true;
		document.getElementById("cellPhoneError").style.display = "none";
	} else {
		can_submit = false;
		document.getElementById("cellPhoneError").style.display = "block";

	}
}

function emailCheck() {
	email = document.form1.email2.value;
	var r = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;// 邮箱正则
	if (r.test(email)) {
		can_submit = true;
		document.getElementById("emailError").style.display = "none";
	} else {
		can_submit = false;
		document.getElementById("emailError").style.display = "block";

	}
}

function register_judge() {
	// targetForm=document.forms[0];
	// targetForm.action="${path}/logon.do";
	// targetForm.submit();
	if (can_submit != true)
		alert("输入有误!");
	return can_submit;
}