function createCategoryConfigure(msg_id, msg_name) {
	// 创建Category配置的js函数

	$('#category_Conf_Id_Hidden').html(msg_id);
	$('#category_Conf_Name_Hidden').html(msg_name);
	$("#mainInfobox1").load("config.jsp");

}

function deleteCategory(msg_id) {
	// 删除当前Category的js函数

	var url = '../../category/delete.do';

	$
			.ajax({
				type : "POST",
				url : url,
				data : {
					'deleteCategory_id' : msg_id
				},
				success : function(msg) {
					// alert("Data Saved: " + msg);
					if (msg == 'true') {
						alert('删除成功,点击确定刷新页面');
						window.location = "http://localhost:8080/sprouts/pages/weblogs/loghome.jsp"
								+ window.location.search;

					} else
						alert('删除错误，请联系管理员');
				}
			});
}

function startCategory(msg_id) {
	var categoryImage = document.getElementById('category_btn_' + msg_id);// 获取到开关的图片

	var categoryImageName = categoryImage.style.backgroundImage.split('/');

	var url = '';

	if (categoryImageName[3].substring(0, 2) == 'on') {
		// 当前状态为开
		// alert('now open');
		url = '../../category/start-catogory.do';
	} else {
		// 当前状态为关
		// alert('now close');
		url = '../../category/stop-catogory.do';
	}
	//alert("This is the method");
	$.ajax({
			type : "POST",
			url : url,
			data : {
				'cid' : msg_id
			},
			success : function(msg) {
				alert(msg);
				if (msg == 'open_success') {
					document.getElementById('category_btn_' + msg_id).style.backgroundImage = 'url(../../images/on.png)';
				}
				if (msg == 'close_success') {
					document.getElementById('category_btn_' + msg_id).style.backgroundImage = 'url(../../images/off.png)';
				}
			}
	});

}
