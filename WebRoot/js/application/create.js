//zhou
$(function(){	
	appCreate.run();	
});

var appCreate = {
	userId:0,
	//启动
	run: function(){
		this._init_addorupdate_isssuse();
	},	
	
	_init_addorupdate_isssuse:function(){
		//初始化
		this._init_();
		
		//绑定事件
		this._event_();
	},
	
	//初始化, zf服务信息
	_init_: function(){
		
	},
	//about event
	_event_: function(){
		$("#createApplication").click(function(){
			var url = "../application/add.do";	
			//组装请求参数
			var data = $("#app_create_form").serialize();	
			$.post(url, data, function(result){
				var jsonResult = $.parseJSON(result);
				alert(jsonResult.object);
				window.location = "index.jsp";
			});
		});
		
		$("#user_release_btn").click(function(){
			var url = "../user/addUser.action";	
			//组装请求参数
			var data = $("#addUserInfo").serialize();	
			//update
			var orgCode = $("#orgCode").val();
			
			var orgType = orgCode.substring(0,1);
			if(orgType=='A'){
				data=data+"&userInfo.isPBCUser="+1;
			}else{
				data=data+"&userInfo.isPBCUser="+0;
			}
				
			$.post(url, data, function(result){
				alert(result.message);
				/*if(result.type=="update"){
					window.location = "jumpAddUser.action";
				}*/
				window.location = "jumpAddUser.action";
			});
			return false;
		});
		
		$("#user_update_btn").click(function(){
			var url = "../user/updateUser.action";	
			//组装请求参数
			var data = $("#addUserInfo").serialize();	
			//update
			var userId = $("#userId").val();
			data=data+"&userInfo.id="+userId;
			var orgCode = $("#orgCode").val();
			
			var orgType = orgCode.substring(0,1);
			if(orgType=='A'){
				data=data+"&userInfo.isPBCUser="+1;
			}else{
				data=data+"&userInfo.isPBCUser="+0;
			}
				
			$.post(url, data, function(result){
				alert(result.message);
				window.location = "jumpAddUser.action";
			});
			return false;
		});
		
		
		
		$("#user_change_password").click(function(){
			
			if($('#oldpass').val().length==0){
				alert("请输入旧密码！");
				return false;
			}
			if($('#newpass').val().length==0){
				alert("请输入新密码！");
				return false;
			}
			if($('#repass').val().length==0){
				alert("请输入确定新密码！");
				return false;
			}
			if($('#newpass').val()!=($('#repass').val())){
				alert("两次输入的新密码不同！");
				return false;
			}
			if($('#newpass').val()==($('#oldpass').val())){
				alert("新密码和旧密码不能相同！");
				return false;
			}
			var url = "${base}/user/modifyPwdUser.action";	
			//组装请求参数
			var data = $("#changePWDUser").serialize();	
			
			$.post(url, data, function(result){
				alert(result.message);
				if(result.success){
					window.location = "getUserInfoList.action";
				}
				else{
					$('#oldpass').val("");
					$('#newpass').val("");
					$('#repass').val("");
				}
			});
			return false;
		});
	}
		
};

function checkUsername(){
	var validate = true;
	document.getElementById("errorusername").innerHTML="*";
	if(checkName(document.getElementById("userName").value)){
		validate=false;
		document.getElementById("errorusername").innerHTML="请输入长度为6-16的数字或字母";
		return validate;
	}
	var name = document.getElementById("userName").value;
	$.ajax({
		type : "post",
		url : "../user/checkUser.action",
		data : {
			"userName" : name
		}, //传值到后台
		success : function(result) { //操作成功，可以由后台返回想要的信息
			if(result.success){
				validate=false;
				document.getElementById("errorusername").innerHTML="已经存在同样的用户名，请重新输入";
				return validate;
			}
		},
		error : function() {
			alert("操作失败");
		}
	});
	
	return validate;
}

function checkPassword(){
	var validate = true;
	document.getElementById("errorpw").innerHTML="*";
	if(checkpw(document.getElementById("userPwd").value)){
		validate=false;
		document.getElementById("errorpw").innerHTML="密码应为6-16位的数字或字母";
	}
	return validate;
}

function checkcomfirm(){
	var validate = true;
	document.getElementById("errorcomfirm").innerHTML="*";
	var password=document.getElementById("userPwd").value;
	var repassword=document.getElementById("repassword").value;
	if(!(password==repassword)){
		validate=false;
		document.getElementById("errorcomfirm").innerHTML="请重复输入密码";
	}
	return validate;
}
function checkUserCode(){
	var validate = true;
	var code = document.getElementById("userCode").value;
	document.getElementById("erroruserCode").innerHTML="*";
	var name = /^[0-9a-zA-Z]{4,16}$/;
 	if(code.length==0||!(name.test(code))){
 		validate=false;
		document.getElementById("erroruserCode").innerHTML="请输入长度为6-16的数字或字母";
 	}
	return validate;
}
function checkOrgCode(){
	var validate = true;
	var code = document.getElementById("orgCode").value;
	document.getElementById("errororgCode").innerHTML="*";
	var name = /^[A-Z][0-9a-zA-Z]{4,16}$/;
 	if(code.length==0||!(name.test(code))){
 		validate=false;
		document.getElementById("errororgCode").innerHTML="qing shu ru zheng que ji gou bian ma";
 	}
	return validate;
}

function checkName(text){
	var name = /^[0-9a-zA-Z]{4,16}$/;
 	if(text.length==0||!(name.test(text))){
 		return true;
 	}else{
  		return false; 
	}
}


function checkUserName(name){
	$.ajax({
		type : "post",
		url : "../user/checkUser.action",
		data : {
			"userName" : name
		}, //传值到后台
		success : function(result) { //操作成功，可以由后台返回想要的信息
			alert(result.message);
			if(result.success){
				return true;
			}
		},
		error : function() {
			return false;
		}
	});
}

function checkpw(text){
 	var pwd=/^[0-9a-zA-Z]{4,16}$/;
 	return !(pwd.test(text))
}
function checkLevel(){
	var validate = true;
	var level = document.getElementById("userLevel").value;
	document.getElementById("errorLevel").innerHTML="*";
 	var regex=/^[0-9]{1}$/;
 	if(level.length==0||!(regex.test(level))){
 		validate=false;
		document.getElementById("errorLevel").innerHTML="请输入长度为1的数字";
 	}
 	return validate;
}
