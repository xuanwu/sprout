//xiaocai
$(function(){	
	serviceCreate.run();	
});

var serviceCreate = {
	userId:0,
	//启动
	run: function(){
		this._init_addorupdate_isssuse();
	},	
	
	_init_addorupdate_isssuse:function(){
		//初始化
		this._init_();
		
		//绑定事件
		this._event_();
	},
	
	//初始化, zf服务信息
	_init_: function(){
		
	},
	//about event
	_event_: function(){
			var id=location.search.substr(4);
			
			$('body').on('click', '#createService', function(){
			if (document.getElementById("servName").value=="")
				{
				alert("服务名不能为空");
				}
			else{
				var url = "../service/create.do";	
				//组装请求参数
				var data = $("#service_create_form").serialize();
				data=data+"&id="+id;				
				$.post(url, data, function(result){
					var jsonResult = $.parseJSON(result);
					alert(jsonResult.message);
					
				});
			}
		});
		
		$('body').on('click', '.unbound', function(){
			var url="../service/status/change.do";
			var serviceType = $(this).attr('data-servicetype');
			var data={id:id,serviceType:serviceType,status:"1"};
			var that = this;
			//alert(serviceType);
			$.post(url,data,function(result){
				var jsonResult=$.parseJSON(result);
				if(jsonResult.success==true){
					var text=" <button class='serv_table_bound bound' data-servicetype='"+serviceType+"'>绑定</button>";
					$(that).parent().html(text);
				}
				else{
					alert("操作未能成功");
				}
			});
		});
		
		
		$('body').on('click', '.bound', function(){
		//var that = this;
			var url="../service/status/change.do";
			var serviceType = $(this).attr('data-servicetype');
			var data={id:id,serviceType:serviceType,status:"0"};
			var that = this;
			//alert(serviceType);
			$.post(url,data,function(result){
				var jsonResult=$.parseJSON(result);
				if(jsonResult.success==true){
					var text=" <button class='serv_table_unbound unbound' data-servicetype='"+serviceType+"'>解绑</button><button class='serv_table_bound'>管理</button>";
					$(that).parent().html(text);
				}
				else{
					alert("操作未能成功");
				}
			});
		});
	}
		
};

