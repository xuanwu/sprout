$(function() {
	$('#custom_file_upload')
			.uploadify(
					{
						'uploader' : '../pages/uploadify-v2.1.4/uploadify.swf?var='
								+ (new Date()).getTime(),
						'script' : '/servlet/Upload',
						'cancelImg' : '../pages/uploadify-v2.1.4/cancel.png',
						'folder' : 'uploads',
						'multi' : false,
						'auto' : false,
						'queueID' : 'custom-queue',
						'queueSizeLimit' : 3,
						'simUploadLimit' : 3,
						'sizeLimit' : 1024000,
						'removeCompleted' : false,
						'onSelectOnce' : function(event, data) {
							$('#status-message')
									.text(
											data.filesSelected
													+ ' files have been added to the queue.');
						},
						'onComplete' : function(event, queueID, fileObj,
								response, data) {
							$('#filePath').val(response);
						},
						'onAllComplete' : function(event, data) {
							$('#status-message').text(
									data.filesUploaded + ' files uploaded, '
											+ data.errors + ' errors.');

						}
					});
});