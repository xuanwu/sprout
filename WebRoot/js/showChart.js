﻿function showCPU(){
	eval("document.getElementById('cpuWindow').style.display='block'");
	eval("document.getElementById('all').style.display='block'");
	var a=document.getElementByIdx(sidebar);
    var b=document.getElementByIdx(right);
	//document.getElementById("all").style.height=document.getElementById("cupWindow").height.slice(0,-2)-'0' + 50 + "px"；
}
function closeCpuWindow(){
	eval("document.getElementById('cpuWindow').style.display='none'");
	eval("document.getElementById('all').style.display='none'");	
	
}
function showLog(){
	eval("document.getElementById('logWindow').style.display='block'");
	eval("document.getElementById('all').style.display='block'");
	//"document.getElementById("all").style.height=document.getElementById("logWindow").height+"px";
}

function createChart(boxID,legend,chartTitle,xAxisArray,chartData,yAxisTitle,suffix) {
    $(boxID).highcharts({
		credits:{
			enabled:false
		},
        chart: {
            type: 'line'
        },
        title: {
            text: chartTitle//title
        },
        subtitle: {
            text: ' '
        },
        xAxis: {
            categories: xAxisArray
        },
        yAxis: {
            title: {
                text: yAxisTitle
            }
        },
        tooltip: {
            enabled: false,
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+ this.y +suffix;
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{         //数据
            name: legend,
            data: chartData
        }]
    });
}

function createChartForWeblogs(boxID,legend,chartTitle,xAxisArray,chartData,yAxisTitle,suffix,max) {
    $(boxID).highcharts({
		credits:{
			enabled:false
		},
        chart: {
            type: 'column',
            height: 600
        },
        title: {
            text: chartTitle//title
        },
        subtitle: {
            text: ' '
        },
        xAxis: {
            categories: xAxisArray,
            labels: {
                rotation: -45,
                align: 'right',
                style: {
                    fontSize: '5px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            title: {
                text: yAxisTitle
            },
            max:max
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
            x: 0,
            y: 100
        },
        tooltip: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+ this.y +suffix;
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
        	name:legend,//数据
            data: chartData
        }]
    });
}



