function createChart(boxID,legend,chartTitle,xAxisArray,chartData,yAxisTitle,xAxisTitle,suffix) {
    $(boxID).highcharts({
		credits:{
			enabled:false
		},
        chart: {
            type: 'area'                                            
        },
        title: {
            text: chartTitle//title
        },
        subtitle: {
            text: ''
        },
        xAxis: {
        	title: {
                text: xAxisTitle
            },
            tickPixelInterval: 150,//x轴上的间隔
            type: 'datetime'//定义x轴上日期的显示格式
            //dateTimeLabelFormats:{second:'%H:%M:%S',minute:'%H:%M',hour:'%H:%M',day:'%e. %b',week:'%e. %b',month:'%b \'%y',year:'%Y'}
        },
        yAxis: {
            title: {
                text: yAxisTitle
            }
        },
        tooltip: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.series.name +': '+ this.y +suffix+'</b>'+'<br/>'+Highcharts.dateFormat('%Y-%m-%d %H:%M:%S',this.x) ;
            }
        
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{                                                              
            name: legend,   
            turboThreshold:0, //不限制数据点个数
            data: (function() {                                                                                       
                var data = [],i;
                var rarray=new Array();
                for(i=0;i<xAxisArray.length;i++){
                	var date1 =new Date(Date.parse(xAxisArray[i].replace(/-/g,   "/")));//转化成国际标准格式
                    var r = Date.UTC(date1.getFullYear(),date1.getMonth(),date1.getDate(),
            				date1.getHours(),date1.getMinutes(),date1.getSeconds() );
                    rarray[i]=r;
                }
              //alert(chartData.length);                                                              
                for (i = 0; i <= chartData.length-1; i++) {                                    
                    data.push({                                                 
                        x: rarray[i],
                        y: chartData[i]                                        
                    });                                                         
                }                                                               
                return data;                                                    
            })()                                                                
        }]
    });
}