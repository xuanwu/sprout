CREATE TABLE USERINFO(
	 ID INTEGER NOT NULL NOT NULL AUTO_INCREMENT, 
	 usercode VARCHAR(40),
	 username VARCHAR(40),
	 userpass VARCHAR(40),
	 phone VARCHAR(30),
	 email VARCHAR(30),
	 role VARCHAR(30),
	 status INTEGER,
	 createdDate VARCHAR(30),
	 PRIMARY KEY(ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE SERVICEINFO(
	 ID INTEGER NOT NULL NOT NULL AUTO_INCREMENT, 
	 servicename VARCHAR(40),
	 servicetype VARCHAR(40),
	 serviceenv VARCHAR(40),
	 status INTEGER,
	 createdDate VARCHAR(30),
	 appId INTEGER,
	 userId INTEGER,
	 PRIMARY KEY(ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `taskAttempt`;
CREATE TABLE `taskAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jobId` varchar(80) DEFAULT NULL,
  `jarPath` varchar(255) DEFAULT NULL,
  `memory` varchar(30) DEFAULT NULL,
  `cpu` varchar(30) DEFAULT NULL,
  `distUsed` varchar(30) DEFAULT NULL,
  `network` varchar(80) DEFAULT NULL,
  `architecture` varchar(30) DEFAULT NULL,
  `usedTime` varchar(40) DEFAULT NULL,
  `assumeTime` varchar(40) DEFAULT NULL,
  
  `createdDate` varchar(40) DEFAULT NULL,
  `url` varchar(40) DEFAULT NULL,
  `logsPath` varchar(40) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `appId` int(11) DEFAULT NULL,
  
  `status` int(11) DEFAULT NULL,
  `finalStatus` int(11) DEFAULT NULL,
   `envVariables` varchar(200) DEFAULT NULL,
   `username` varchar(80) DEFAULT NULL,
   `taskname` varchar(200) DEFAULT NULL,
   `applicationType` varchar(40) DEFAULT NULL,
   `queue` varchar(40) DEFAULT NULL,
   `startTime` varchar(40) DEFAULT NULL,
   `mapProgress` float DEFAULT NULL,
   `reduceProgress` float DEFAULT NULL,
   `endTime` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
